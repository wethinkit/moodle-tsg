<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allows you to edit a users profile
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package user
 */

require_once('../config.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/user/editadvanced_form.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$id     = optional_param('id', $USER->id, PARAM_INT);    // user id; -1 if creating new user
$course = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)

$PAGE->set_url('/user/editadvanced.php', array('course'=>$course, 'id'=>$id));

$course = $DB->get_record('course', array('id'=>$course), '*', MUST_EXIST);

if (!empty($USER->newadminuser)) {
    $PAGE->set_course($SITE);
    $PAGE->set_pagelayout('maintenance');
} else {
    require_login($course);
    $PAGE->set_pagelayout('admin');
}

if ($course->id == SITEID) {
    $coursecontext = context_system::instance();   // SYSTEM context
} else {
    $coursecontext = context_course::instance($course->id);   // Course context
}
$systemcontext = context_system::instance();

if ($id == -1) {
    // creating new user
    $user = new stdClass();
    $user->id = -1;
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->deleted = 0;
    require_capability('moodle/user:create', $systemcontext);
    admin_externalpage_setup('addnewuser', '', array('id' => -1));
} else {
    // editing existing user
    require_capability('moodle/user:update', $systemcontext);
    $user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->set_context(context_user::instance($user->id));
    if ($user->id == $USER->id) {
        if ($course->id != SITEID && $node = $PAGE->navigation->find($course->id, navigation_node::TYPE_COURSE)) {
            $node->make_active();
            $PAGE->navbar->includesettingsbase = true;
        }
    } else {
        $PAGE->navigation->extend_for_user($user);
    }
}

// remote users cannot be edited
if ($user->id != -1 and is_mnet_remote_user($user)) {
    redirect($CFG->wwwroot . "/user/view.php?id=$id&course={$course->id}");
}

if ($user->id != $USER->id and is_siteadmin($user) and !is_siteadmin($USER)) {  // Only admins may edit other admins
    print_error('useradmineditadmin');
}

if (isguestuser($user->id)) { // the real guest user can not be edited
    print_error('guestnoeditprofileother');
}

if ($user->deleted) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('userdeleted'));
    echo $OUTPUT->footer();
    die;
}

//load user preferences
useredit_load_preferences($user);

//Load custom profile fields data
profile_load_data($user);

//User interests
if (!empty($CFG->usetags)) {
    require_once($CFG->dirroot.'/tag/lib.php');
    $user->interests = tag_get_tags_array('user', $id);
}

if ($user->id !== -1) {
    $usercontext = context_user::instance($user->id);
    $editoroptions = array(
        'maxfiles'   => EDITOR_UNLIMITED_FILES,
        'maxbytes'   => $CFG->maxbytes,
        'trusttext'  => false,
        'forcehttps' => false,
        'context'    => $usercontext
    );

    $user = file_prepare_standard_editor($user, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
} else {
    $usercontext = null;
    // This is a new user, we don't want to add files here
    $editoroptions = array(
        'maxfiles'=>0,
        'maxbytes'=>0,
        'trusttext'=>false,
        'forcehttps'=>false,
        'context' => $coursecontext
    );
}

// Prepare filemanager draft area.
$draftitemid = 0;
$filemanagercontext = $editoroptions['context'];
$filemanageroptions = array('maxbytes'       => $CFG->maxbytes,
                             'subdirs'        => 0,
                             'maxfiles'       => 1,
                             'accepted_types' => 'web_image');
file_prepare_draft_area($draftitemid, $filemanagercontext->id, 'user', 'newicon', 0, $filemanageroptions);
$user->imagefile = $draftitemid;

// TSG Mod Begin

// -----------------------------------------------------------------------------
// This section (through the "TSG Mod End" marker) retrieves two sets of data
// from the db:
//
//   1. Lookup data (e.g. values for dropdowns)
//   2. Data from the User entity to be dislayed on the form
// -----------------------------------------------------------------------------


/*
 * Setup Symfony & get required Doctrine repositories
 */
require_once __DIR__.'/../local/tsg/s/app/bootstrap.php.cache';
require_once __DIR__.'/../local/tsg/s/app/AppKernel.php';
$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$kernel->boot();
$GLOBALS['kernel'] = $kernel;

$logger = $kernel->getContainer()->get('logger');
$em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

$userRepo = $em->getRepository('TSGMoodleLMSBundle:User');
$userGroupRepo = $em->getRepository('TSGMoodleLMSBundle:UserGroup');
$userInfoRepo = $em->getRepository('TSGMoodleLMSBundle:UserInfo');
$tagRepo = $em->getRepository('TSGMoodleLMSBundle:Tag');

/*
 * Lookup user through Doctrine unless we're creating a new one.  We need
 * the Doctrine entity instead of the Moodle provided $user/$usernew so that we
 * can use our ORM.
 */
$userEntity = $user->id == -1 ? false : $userRepo->find($user->id);

/*
 * Set status group fields on $user from User#info:
 *
 *   - $user->local_tsg_hire_date        = User#info#hireDate
 *   - $user->local_tsg_termination_date = User#info#terminationDate
 *   - $user->local_tsg_is_active        = User#info#isActive
 *
 * The form will display this information in the relevant fields.
 */
$userEntityInfo = $userEntity === false ? false : $userEntity->getInfo();
if (!$userEntityInfo) {
    $userEntityInfo = new \TSG\MoodleLMSBundle\Entity\UserInfo();
    $userEntityInfo->setIsActive(1);
}


$hireDate = $userEntityInfo->getHireDate();
if (!empty($hireDate)) {
    $hireDate = getdate($hireDate);
    $user->local_tsg_hire_date = array(
        'month' => $hireDate['mon'],
        'day' => $hireDate['mday'],
        'year' => $hireDate['year']
    );
}
else {
    $user->local_tsg_hire_date = array(
        'month' => '',
        'day' => '',
        'year' => ''
    );
}

$terminationDate = $userEntityInfo->getTerminationDate();
if (!empty($terminationDate)) {
    $terminationDate = getdate($terminationDate);
    $user->local_tsg_termination_date = array(
        'month' => $terminationDate['mon'],
        'day' => $terminationDate['mday'],
        'year' => $terminationDate['year']
    );
}
else {
    $user->local_tsg_termination_date = array(
        'month' => '',
        'day' => '',
        'year' => ''
    );
}

$user->local_tsg_inactive = !$userEntityInfo->isActive();

/*
 * Lookup all profile groups so form can display dropdowns for each.  Passed to
 * form below through moodleform#_customdata.
 */
$profileGroups = $userGroupRepo->getProfileGroups();

/*
 * Set profile group fields on $user.  Each profile group property is identified
 * as local_tsg_GroupNameId (e.g. local_tsg_DepartmentId).  The value of the
 * property is the child group id of the profile gruop to which the user belongs.
 */
$profileGroupMembership = $userRepo->getProfileGroupMembershipForUser($user->id);
if ($profileGroupMembership) {
    foreach ($profileGroupMembership as $group) {
        // local_tsg_GroupNameId (e.g. local_tsg_DepartmentId)
        $property = str_replace(' ', '',
                'local_tsg_'.$group->getParent()->first()->getName()).'Id';

        // e.g. $user->local_tsg_DepartmentId = accounting.id
        $user->{$property} = $group->getId();
    }
}

/*
 * Lookup all tags so form can display checkbox group.  Passed to form below
 * through moodleform#_customdata.
 */
$tags = $tagRepo->findBy(array(), array('name' => 'asc'));

/*
 * Set $user->local_tsg_tags from User#tags.  Form will use this information
 * to check the relevant checkboxes created from $tags (see above).
 */
if ($userEntity !== false) {
    $userEntityTags = $userEntity->getTags();
    if ($userEntityTags) {
        // reformat records for checkbox selection (e.g. array(1=>true, 3=>true))
        // where 1 & 3 are tagIds and true indicates checkbox should be checked
        // and the absence of a tagId means that checkbox should be unchecked.
        $selectedTags = array();
        foreach ($userEntityTags as $tag) {
            $selectedTags[$tag->getId()] = true;
        }
        $user->local_tsg_tags = $selectedTags;
    }
}

// TSG Mod End

//create form
$userform = new user_editadvanced_form(null, array(
    'editoroptions' => $editoroptions,
    'filemanageroptions' => $filemanageroptions,
    'userid' => $user->id,
// TSG Mod Begin
    'profileGroups' => $profileGroups,
    'tags' => $tags));
// TSG Mod End
$userform->set_data($user);

if ($usernew = $userform->get_data()) {

    if (empty($usernew->auth)) {
        //user editing self
        $authplugin = get_auth_plugin($user->auth);
        unset($usernew->auth); //can not change/remove
    } else {
        $authplugin = get_auth_plugin($usernew->auth);
    }

    $usernew->timemodified = time();

    if ($usernew->id == -1) {
        //TODO check out if it makes sense to create account with this auth plugin and what to do with the password
        unset($usernew->id);
        $usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, null, 'user', 'profile', null);
        $usernew->mnethostid = $CFG->mnet_localhost_id; // always local user
        $usernew->confirmed  = 1;
        $usernew->timecreated = time();
        $usernew->password = hash_internal_user_password($usernew->newpassword);
        $usernew->id = $DB->insert_record('user', $usernew);
        $usercreated = true;
        add_to_log($course->id, 'user', 'add', "view.php?id=$usernew->id&course=$course->id", '');

    } else {
        $usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
        $DB->update_record('user', $usernew);
        // pass a true $userold here
        if (! $authplugin->user_update($user, $userform->get_data())) {
            // auth update failed, rollback for moodle
            $DB->update_record('user', $user);
            print_error('cannotupdateuseronexauth', '', '', $user->auth);
        }
        add_to_log($course->id, 'user', 'update', "view.php?id=$user->id&course=$course->id", '');

        //set new password if specified
        if (!empty($usernew->newpassword)) {
            if ($authplugin->can_change_password()) {
                if (!$authplugin->user_update_password($usernew, $usernew->newpassword)){
                    print_error('cannotupdatepasswordonextauth', '', '', $usernew->auth);
                }
                unset_user_preference('create_password', $usernew); // prevent cron from generating the password
            }
        }

        // force logout if user just suspended
        if (isset($usernew->suspended) and $usernew->suspended and !$user->suspended) {
            session_kill_user($user->id);
        }

        $usercreated = false;
    }

// TSG Mod Begin

    // -------------------------------------------------------------------------
    // This section (through the "TSG Mod End" marker) processes submitted data
    // from the form and stores it using the ORM system (Doctrine).
    // -------------------------------------------------------------------------

    /*
     * Refresh previously retrieved $userEntity with values just
     * updated by core moodle code above.
     */
    $userEntity = $userRepo->find($usernew->id);
    $userEntityInfo = $userEntity->getInfo();
    if (empty($userEntityInfo)) {
        // for a new user, User::UserInfo has not yet been created
        $userEntityInfo = new \TSG\MoodleLMSBundle\Entity\UserInfo();
        $userEntityInfo->setUser($userEntity);
        $userEntity->setInfo($userEntityInfo);
    }

    /*
     * Update User#info fields (hireDate, terminationDate, isActive)
     */

    $month = $usernew->local_tsg_hire_date['month'];
    $day = $usernew->local_tsg_hire_date['day'];
    $year = $usernew->local_tsg_hire_date['year'];
    $userEntityInfo->setHireDate(
        ($month == '' && $day == '' && $year == '') ?
            null : mktime(0,0,0,$month, $day, $year)
    );

    $month = $usernew->local_tsg_termination_date['month'];
    $day = $usernew->local_tsg_termination_date['day'];
    $year = $usernew->local_tsg_termination_date['year'];
    $userEntityInfo->setTerminationDate(
        ($month == '' && $day == '' && $year == '') ?
            null : mktime(0,0,0,$month, $day, $year)
    );

    $userEntityInfo->setIsActive(
        (is_null($userEntityInfo->getTerminationDate())) ?
            !$usernew->local_tsg_inactive : 0
    );


    /*
     * Update group membership for profile groups
     */

    $userGroupRepo = $em->getRepository('TSGMoodleLMSBundle:UserGroup');
    $profileGroups = $userGroupRepo->getProfileGroups();
    if ($profileGroups) {
        foreach ($profileGroups as $pg) {
            // profile group's select element name (e.g. local_tsg_DepartmentId)
            $property = 'local_tsg_'.str_replace(' ', '', $pg->getName()).'Id';

            if ($usernew->{$property} > 0) {
                // A selection was made in the profile group dropdown.  Lookup
                // the Group entity corresonding to the selected id.  This is
                // the child of $pg (e.g. department) to which the user should
                // be assigned.
                $g = $userGroupRepo->find($usernew->{$property});

                // Assign the selected child group to this user.  This will also
                // remove the user from any sibling groups as users may only be
                // members of one child group of a profile group (e.g. a user
                // may only be in one department).
                $userRepo->assignUserToProfileGroup($userEntity, $g);
            }
            else {
                // User selected the empty option in the profile group
                // dropdown.  Remove them from any children of $pg.
                $userRepo->removeUserFromProfileGroup($userEntity, $pg);
            }
        }
    }


    /*
     * Update tags
     */

    if ($usernew->local_tsg_tags) {
        $newTags = array();
        foreach ($usernew->local_tsg_tags as $id => $isSelected) {
            $t = $tagRepo->find($id);
            $newTags[] = $t;
        }
        $userEntity->setTags($newTags);
    }
    else {
        $userEntity->setTags(array());
    }

    $em->persist($userEntity);
    $em->flush();

// TSG Mod End

    $usercontext = context_user::instance($usernew->id);

    //update preferences
    useredit_update_user_preference($usernew);

    // update tags
    if (!empty($CFG->usetags) and empty($USER->newadminuser)) {
        useredit_update_interests($usernew, $usernew->interests);
    }

    //update user picture
    if (empty($USER->newadminuser)) {
        useredit_update_picture($usernew, $userform, $filemanageroptions);
    }

    // update mail bounces
    useredit_update_bounces($user, $usernew);

    // update forum track preference
    useredit_update_trackforums($user, $usernew);

    // save custom profile fields data
    profile_save_data($usernew);

    // reload from db
    $usernew = $DB->get_record('user', array('id'=>$usernew->id));

    // trigger events
    if ($usercreated) {
        events_trigger('user_created', $usernew);
    } else {
        events_trigger('user_updated', $usernew);
    }

    if ($user->id == $USER->id) {
        // Override old $USER session variable
        foreach ((array)$usernew as $variable => $value) {
            $USER->$variable = $value;
        }
        // preload custom fields
        profile_load_custom_fields($USER);

        if (!empty($USER->newadminuser)) {
            unset($USER->newadminuser);
            // apply defaults again - some of them might depend on admin user info, backup, roles, etc.
            admin_apply_default_settings(NULL , false);
            // redirect to admin/ to continue with installation
            redirect("$CFG->wwwroot/$CFG->admin/");
        } else {
            redirect("$CFG->wwwroot/user/view.php?id=$USER->id&course=$course->id");
        }
    } else {
        session_gc(); // remove stale sessions
        redirect("$CFG->wwwroot/$CFG->admin/user.php");
    }
    //never reached
}

// make sure we really are on the https page when https login required
$PAGE->verify_https_required();


/// Display page header
if ($user->id == -1 or ($user->id != $USER->id)) {
    if ($user->id == -1) {
        echo $OUTPUT->header();
    } else {
        $PAGE->set_heading($SITE->fullname);
        echo $OUTPUT->header();
        $userfullname = fullname($user, true);
        echo $OUTPUT->heading($userfullname);
    }
} else if (!empty($USER->newadminuser)) {
    $strinstallation = get_string('installation', 'install');
    $strprimaryadminsetup = get_string('primaryadminsetup');

    $PAGE->navbar->add($strprimaryadminsetup);
    $PAGE->set_title($strinstallation);
    $PAGE->set_heading($strinstallation);
    $PAGE->set_cacheable(false);

    echo $OUTPUT->header();
    echo $OUTPUT->box(get_string('configintroadmin', 'admin'), 'generalbox boxwidthnormal boxaligncenter');
    echo '<br />';
} else {
    $streditmyprofile = get_string('editmyprofile');
    $strparticipants  = get_string('participants');
    $strnewuser       = get_string('newuser');
    $userfullname     = fullname($user, true);

    $PAGE->set_title("$course->shortname: $streditmyprofile");
    $PAGE->set_heading($course->fullname);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($userfullname);
}

/// Finally display THE form
$userform->display();

/// and proper footer
echo $OUTPUT->footer();

