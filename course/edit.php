<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit course settings
 *
 * @package    moodlecore
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../config.php');
require_once('lib.php');
require_once('edit_form.php');

$id         = optional_param('id', 0, PARAM_INT);       // course id
$categoryid = optional_param('category', 0, PARAM_INT); // course category - can be changed in edit form
$returnto = optional_param('returnto', 0, PARAM_ALPHANUM); // generic navigation return page switch

$PAGE->set_pagelayout('admin');
$pageparams = array('id'=>$id);
if (empty($id)) {
    $pageparams = array('category'=>$categoryid);
}
$PAGE->set_url('/course/edit.php', $pageparams);

// basic access control checks
if ($id) { // editing course
    if ($id == SITEID){
        // don't allow editing of  'site course' using this from
        print_error('cannoteditsiteform');
    }

    // Login to the course and retrieve also all fields defined by course format.
    $course = get_course($id);
    require_login($course);
    $course = course_get_format($course)->get_course();

    $category = $DB->get_record('course_categories', array('id'=>$course->category), '*', MUST_EXIST);
    $coursecontext = context_course::instance($course->id);
    require_capability('moodle/course:update', $coursecontext);

} else if ($categoryid) { // creating new course in this category
    $course = null;
    require_login();
    $category = $DB->get_record('course_categories', array('id'=>$categoryid), '*', MUST_EXIST);
    $catcontext = context_coursecat::instance($category->id);
    require_capability('moodle/course:create', $catcontext);
    $PAGE->set_context($catcontext);

} else {
    require_login();
    print_error('needcoursecategroyid');
}

// Prepare course and the editor
$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
$overviewfilesoptions = course_overviewfiles_options($course);
if (!empty($course)) {
    //add context for editor
    $editoroptions['context'] = $coursecontext;
    $course = file_prepare_standard_editor($course, 'summary', $editoroptions, $coursecontext, 'course', 'summary', 0);
    if ($overviewfilesoptions) {
        file_prepare_standard_filemanager($course, 'overviewfiles', $overviewfilesoptions, $coursecontext, 'course', 'overviewfiles', 0);
    }

    // Inject current aliases
    $aliases = $DB->get_records('role_names', array('contextid'=>$coursecontext->id));
    foreach($aliases as $alias) {
        $course->{'role_'.$alias->roleid} = $alias->name;
    }

} else {
    //editor should respect category context if course context is not set.
    $editoroptions['context'] = $catcontext;
    $course = file_prepare_standard_editor($course, 'summary', $editoroptions, null, 'course', 'summary', null);
    if ($overviewfilesoptions) {
        file_prepare_standard_filemanager($course, 'overviewfiles', $overviewfilesoptions, null, 'course', 'overviewfiles', 0);
    }
}

// TSG Mod Begin

// -----------------------------------------------------------------------------
// This section (through the "TSG Mod End" marker) retrieves two sets of data
// from the db:
//
//   1. Lookup data (e.g. values for dropdowns)
//   2. Data from the Course entity to be dislayed on the form
// -----------------------------------------------------------------------------

/*
 * Setup Symfony & get required Doctrine repositories
 */
require_once __DIR__.'/../local/tsg/s/app/bootstrap.php.cache';
require_once __DIR__.'/../local/tsg/s/app/AppKernel.php';
$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$kernel->boot();
$GLOBALS['kernel'] = $kernel;

$logger = $kernel->getContainer()->get('logger');
$em     = $kernel->getContainer()->get('doctrine.orm.entity_manager');

$courseRepo = $em->getRepository('TSGMoodleLMSBundle:Course');
$tagRepo    = $em->getRepository('TSGMoodleLMSBundle:Tag');

/*
 * Lookup course through Doctrine unless we're creating a new one.  We need
 * the Doctrine entity instead of the Moodle provided $course so that we can
 * use our ORM.
 */
$courseEntity = empty($course) ? false : $courseRepo->find($course->id);


/*
 * Set course info fields on $course from Course#info.  The form will display
 * this information in the relevant fields.
 */
$courseEntityInfo = $courseEntity === false ? false : $courseEntity->getInfo();
if ($courseEntityInfo !== false) {
    $course->local_tsg_hours          = $courseEntityInfo->getHours();
    $course->local_tsg_credits        = $courseEntityInfo->getCredits();
    $course->local_tsg_costperstudent = $courseEntityInfo->getCostPerStudent();
}

/*
 * Lookup all tags so form can display checkbox group.  Passed to form below
 * through moodleform#_customdata.
 */
$tags = $tagRepo->findBy(array(), array('name' => 'asc'));

/*
 * Set $course->local_tsg_tags from Course#tags.  Form will use this information
 * to check the relevant checkboxes created from $tags (see above).
 */
if ($courseEntity !== false) {
    $courseEntityTags = $courseEntity->getTags();
    if ($courseEntityTags) {
        // reformat records for checkbox selection (e.g. array(1=>true, 3=>true))
        // where 1 & 3 are tagIds and true indicates checkbox should be checked
        // and the absence of a tagId means that checkbox should be unchecked.
        $selectedTags = array();
        foreach ($courseEntityTags as $tag) {
            $selectedTags[$tag->getId()] = true;
        }
        $course->local_tsg_tags = $selectedTags;
    }
}

// TSG Mod End

// first create the form
$editform = new course_edit_form(NULL, array(
    'course'=>$course,
    'category'=>$category,
    'editoroptions'=>$editoroptions,
    'returnto'=>$returnto,
// TSG Mod Begin
    'tags' => $tags)
// TSG Mod End
);
if ($editform->is_cancelled()) {
        switch ($returnto) {
            case 'category':
                $url = new moodle_url($CFG->wwwroot.'/course/index.php', array('categoryid' => $categoryid));
                break;
            case 'catmanage':
                $url = new moodle_url($CFG->wwwroot.'/course/manage.php', array('categoryid' => $categoryid));
                break;
            case 'topcatmanage':
                $url = new moodle_url($CFG->wwwroot.'/course/manage.php');
                break;
            case 'topcat':
                $url = new moodle_url($CFG->wwwroot.'/course/');
                break;
            default:
                if (!empty($course->id)) {
                    $url = new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$course->id));
                } else {
                    $url = new moodle_url($CFG->wwwroot.'/course/');
                }
                break;
        }
        redirect($url);

} else if ($data = $editform->get_data()) {
    // process data if submitted

    if (empty($course->id)) {
        // In creating the course
        $course = create_course($data, $editoroptions);

        // Get the context of the newly created course
        $context = context_course::instance($course->id, MUST_EXIST);

        if (!empty($CFG->creatornewroleid) and !is_viewing($context, NULL, 'moodle/role:assign') and !is_enrolled($context, NULL, 'moodle/role:assign')) {
            // deal with course creators - enrol them internally with default role
            enrol_try_internal_enrol($course->id, $USER->id, $CFG->creatornewroleid);

        }
        if (!is_enrolled($context)) {
            // Redirect to manual enrolment page if possible
            $instances = enrol_get_instances($course->id, true);
            foreach($instances as $instance) {
                if ($plugin = enrol_get_plugin($instance->enrol)) {
                    if ($plugin->get_manual_enrol_link($instance)) {
                        // we know that the ajax enrol UI will have an option to enrol
                        redirect(new moodle_url('/enrol/users.php', array('id'=>$course->id)));
                    }
                }
            }
        }
    } else {
        // Save any changes to the files used in the editor
        update_course($data, $editoroptions);
    }

// TSG Mod Begin

    // -------------------------------------------------------------------------
    // This section (through the "TSG Mod End" marker) processes submitted data
    // from the form and stores it using the ORM system (Doctrine).
    // -------------------------------------------------------------------------

    /*
     * Refresh previously retrieved $courseEntity with values just
     * updated by core moodle code above.
     */
    $courseEntity = $courseRepo->find($course->id);
    $courseEntityInfo = $courseEntity->getInfo();
    if (empty($courseEntityInfo)) {
        // for a new user, Course#info has not yet been created
        $courseEntityInfo = new \TSG\MoodleLMSBundle\Entity\CourseInfo();
        $courseEntityInfo->setCourse($courseEntity);
        $courseEntity->setInfo($courseEntityInfo);
    }

    /*
     * Update Course#info fields (hours, credits, costPerStudent)
     */
    $courseEntityInfo->setHours(empty($data->local_tsg_hours) ?
        null : $data->local_tsg_hours);
    $courseEntityInfo->setCredits(empty($data->local_tsg_credits) ?
        null : $data->local_tsg_credits);
    $courseEntityInfo->setCostPerStudent(empty($data->local_tsg_costperstudent) ?
        null : $data->local_tsg_costperstudent);

    /*
     * Update tags
     */
    if ($data->local_tsg_tags) {
        $newTags = array();
        foreach ($data->local_tsg_tags as $id => $isSelected) {
            $t = $tagRepo->find($id);
            $newTags[] = $t;
        }
        $courseEntity->setTags($newTags);
    }
    else {
        $courseEntity->setTags(array());
    }

    $em->persist($courseEntity);
    $em->flush();

// TSG Mod End

    // Redirect user to newly created/updated course.
    redirect(new moodle_url('/course/view.php', array('id' => $course->id)));
}


// Print the form

$site = get_site();

$streditcoursesettings = get_string("editcoursesettings");
$straddnewcourse = get_string("addnewcourse");
$stradministration = get_string("administration");
$strcategories = get_string("categories");

if (!empty($course->id)) {
    $PAGE->navbar->add($streditcoursesettings);
    $title = $streditcoursesettings;
    $fullname = $course->fullname;
} else {
    $PAGE->navbar->add($stradministration, new moodle_url('/admin/index.php'));
    $PAGE->navbar->add($strcategories, new moodle_url('/course/index.php'));
    $PAGE->navbar->add($straddnewcourse);
    $title = "$site->shortname: $straddnewcourse";
    $fullname = $site->fullname;
}

$PAGE->set_title($title);
$PAGE->set_heading($fullname);

echo $OUTPUT->header();
echo $OUTPUT->heading($streditcoursesettings);

$editform->display();

echo $OUTPUT->footer();

