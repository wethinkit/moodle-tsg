describe('lms home page', function() {

    beforeEach(function() {
        browser.ignoreSynchronization = true;
        browser.get('http://moodle-tsg.local');
    });

    it('should display the title', function() {
        var header = element(by.css('#page-header .headermain'));
        expect(header.getText()).toEqual('DCFS Learning Management System');

        /*
        element(by.model('todoText')).sendKeys('write a protractor test');
        element(by.css('[value="add"]')).click();

        var todoList = element.all(by.repeater('todo in todos'));
        expect(todoList.count()).toEqual(3);
        expect(todoList.get(2).getText()).toEqual('write a protractor test');
        */
    });

    it("should log in", function() {
        var username = element(by.css('input#login_username'));
        var password = element(by.css('input#login_password'));
        var submit = element(by.css('form#login input[type="submit"]'));

        username.sendKeys('admin');
        password.sendKeys('Lsc@2011');
        submit.click().then(function() {
            expect(element(by.css('div.logininfo a[title="View profile"]')).isPresent()).toBe(true);
            console.log('logged in');
        });
    });
});