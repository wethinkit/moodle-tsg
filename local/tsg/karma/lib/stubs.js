M = {};
M.local_tsg = {};

Y = {};

Y.JSON =
{
    parse: function(json) {
        return window.JSON.parse(json);
    }
}


Y.io = function(url, options)
{
    if (options && options.on && options.on.success) {
        options.on.success(1, Y.Mock.io.response);
    }
};

Y.io.responseText = function(data)
{
    Y.Mock.io.response.responseText = data;
}

Y.Mock = {
    io: {
        response: {
            responseText: "{}"
        }
    }
}