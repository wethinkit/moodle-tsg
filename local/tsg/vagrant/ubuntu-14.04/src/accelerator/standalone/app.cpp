#include <crow.h>
#include <logging.h>
#include <chrono>
#include <thread>
#include <string>
#include "app.h"
#include "user_service.h"

int main(int argc, const char ** argv)
{
	UserService service { "tcp://127.0.0.1:3306", "moodle_dcfs_dev", "moodle_user", "learning1" };
	crow::SimpleApp app;

    CROW_ROUTE(app, "/sleep")
    ([]{
        CROW_LOG_INFO << std::this_thread::get_id() << ":  sleeping for 20 seconds";
        std::this_thread::sleep_for(std::chrono::seconds(20));
        CROW_LOG_INFO << std::this_thread::get_id() << ":  awake";
        return "<html><body><h2>TSG Moodle Accelerator</h2></body></html>";
    });

    CROW_ROUTE(app, "/users/test")
    ([&service]{
    	return service.test();
    });

    CROW_ROUTE(app, "/users/findByPattern")
    ([&service]{
        CROW_LOG_INFO << std::this_thread::get_id() << ":  retrieving users like 'j%'";
    	string pattern {"j"};
    	return service.findByPattern(pattern);
    });

    app.port(18080)
        .concurrency(2)
        .run();
}