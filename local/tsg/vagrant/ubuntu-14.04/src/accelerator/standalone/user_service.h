#ifndef __USER_SERVICE_H__
#define __USER_SERVICE_H__

#include <logging.h>
#include <iostream>
#include <memory>
#include <string>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

using namespace std;

class UserService {

private:
	unique_ptr<sql::Connection> conn;
	unique_ptr<sql::PreparedStatement> pstmt;
	
public:
	UserService(const string& url, const string& schema, const string& username, const string& password);

	crow::json::wvalue test();
	crow::json::wvalue findByPattern(const string & pattern);

private:
	const string SQL_FIND_USER_BY_PATTERN = "select id,username,lastname,firstname,email \
			from user \
			where lastname like ? \
			order by lastname,firstname \
			limit 10";
};

#endif /* __USER_SERVICE_H__ */
