	.file	"scratch.cpp"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	_ZStL13allocator_arg, @object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.type	_ZStL6ignore, @object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
.LC0:
	.string	"Object constructor"
	.section	.text._ZN6ObjectC2Ev,"axG",@progbits,_ZN6ObjectC5Ev,comdat
	.align 2
	.weak	_ZN6ObjectC2Ev
	.type	_ZN6ObjectC2Ev, @function
_ZN6ObjectC2Ev:
.LFB1997:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	$.LC0, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1997:
	.size	_ZN6ObjectC2Ev, .-_ZN6ObjectC2Ev
	.weak	_ZN6ObjectC1Ev
	.set	_ZN6ObjectC1Ev,_ZN6ObjectC2Ev
	.section	.rodata
.LC1:
	.string	"Object destructor"
	.section	.text._ZN6ObjectD2Ev,"axG",@progbits,_ZN6ObjectD5Ev,comdat
	.align 2
	.weak	_ZN6ObjectD2Ev
	.type	_ZN6ObjectD2Ev, @function
_ZN6ObjectD2Ev:
.LFB2000:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2000
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	$.LC1, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2000:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN6ObjectD2Ev,"aG",@progbits,_ZN6ObjectD5Ev,comdat
.LLSDA2000:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2000-.LLSDACSB2000
.LLSDACSB2000:
.LLSDACSE2000:
	.section	.text._ZN6ObjectD2Ev,"axG",@progbits,_ZN6ObjectD5Ev,comdat
	.size	_ZN6ObjectD2Ev, .-_ZN6ObjectD2Ev
	.weak	_ZN6ObjectD1Ev
	.set	_ZN6ObjectD1Ev,_ZN6ObjectD2Ev
	.section	.rodata
.LC2:
	.string	"Container constructor"
	.section	.text._ZN9ContainerC2Ev,"axG",@progbits,_ZN9ContainerC5Ev,comdat
	.align 2
	.weak	_ZN9ContainerC2Ev
	.type	_ZN9ContainerC2Ev, @function
_ZN9ContainerC2Ev:
.LFB2003:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2003
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
.LEHB0:
	call	_Z10get_objectv
.LEHE0:
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC1EPS0_
	movl	$.LC2, %esi
	movl	$_ZSt4cout, %edi
.LEHB1:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE1:
	jmp	.L7
.L6:
	movq	%rax, %rbx
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB2:
	call	_Unwind_Resume
.LEHE2:
.L7:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2003:
	.section	.gcc_except_table._ZN9ContainerC2Ev,"aG",@progbits,_ZN9ContainerC5Ev,comdat
.LLSDA2003:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2003-.LLSDACSB2003
.LLSDACSB2003:
	.uleb128 .LEHB0-.LFB2003
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB2003
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L6-.LFB2003
	.uleb128 0
	.uleb128 .LEHB2-.LFB2003
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE2003:
	.section	.text._ZN9ContainerC2Ev,"axG",@progbits,_ZN9ContainerC5Ev,comdat
	.size	_ZN9ContainerC2Ev, .-_ZN9ContainerC2Ev
	.weak	_ZN9ContainerC1Ev
	.set	_ZN9ContainerC1Ev,_ZN9ContainerC2Ev
	.section	.rodata
.LC3:
	.string	"Container destructor"
	.section	.text._ZN9ContainerD2Ev,"axG",@progbits,_ZN9ContainerD5Ev,comdat
	.align 2
	.weak	_ZN9ContainerD2Ev
	.type	_ZN9ContainerD2Ev, @function
_ZN9ContainerD2Ev:
.LFB2006:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2006
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	$.LC3, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED1Ev
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2006:
	.section	.gcc_except_table._ZN9ContainerD2Ev,"aG",@progbits,_ZN9ContainerD5Ev,comdat
.LLSDA2006:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2006-.LLSDACSB2006
.LLSDACSB2006:
.LLSDACSE2006:
	.section	.text._ZN9ContainerD2Ev,"axG",@progbits,_ZN9ContainerD5Ev,comdat
	.size	_ZN9ContainerD2Ev, .-_ZN9ContainerD2Ev
	.weak	_ZN9ContainerD1Ev
	.set	_ZN9ContainerD1Ev,_ZN9ContainerD2Ev
	.section	.rodata
.LC4:
	.string	"allocated new Container"
.LC5:
	.string	"exiting program"
	.text
	.globl	_Z5mainxv
	.type	_Z5mainxv, @function
_Z5mainxv:
.LFB2008:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2008
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
.LEHB3:
	call	_ZN9ContainerC1Ev
.LEHE3:
	movl	$.LC4, %esi
	movl	$_ZSt4cout, %edi
.LEHB4:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movl	$.LC5, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
.LEHE4:
	movl	$0, %ebx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9ContainerD1Ev
	movl	%ebx, %eax
	jmp	.L14
.L13:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN9ContainerD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB5:
	call	_Unwind_Resume
.LEHE5:
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2008:
	.section	.gcc_except_table,"a",@progbits
.LLSDA2008:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2008-.LLSDACSB2008
.LLSDACSB2008:
	.uleb128 .LEHB3-.LFB2008
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB2008
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L13-.LFB2008
	.uleb128 0
	.uleb128 .LEHB5-.LFB2008
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE2008:
	.text
	.size	_Z5mainxv, .-_Z5mainxv
	.globl	_Z10get_objectv
	.type	_Z10get_objectv, @function
_Z10get_objectv:
.LFB2009:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA2009
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	$1, %edi
.LEHB6:
	call	_Znwm
.LEHE6:
	movq	%rax, %rbx
	movq	%rbx, %rdi
.LEHB7:
	call	_ZN6ObjectC1Ev
.LEHE7:
	movq	%rbx, -24(%rbp)
	movq	-24(%rbp), %rax
	jmp	.L19
.L18:
	movq	%rax, %r12
	movq	%rbx, %rdi
	call	_ZdlPv
	movq	%r12, %rax
	movq	%rax, %rdi
.LEHB8:
	call	_Unwind_Resume
.LEHE8:
.L19:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2009:
	.section	.gcc_except_table
.LLSDA2009:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2009-.LLSDACSB2009
.LLSDACSB2009:
	.uleb128 .LEHB6-.LFB2009
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB2009
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L18-.LFB2009
	.uleb128 0
	.uleb128 .LEHB8-.LFB2009
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0
	.uleb128 0
.LLSDACSE2009:
	.text
	.size	_Z10get_objectv, .-_Z10get_objectv
	.section	.text._ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE,"axG",@progbits,_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE,comdat
	.weak	_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE
	.type	_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE, @function
_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE:
.LFB2070:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2070:
	.size	_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE, .-_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE
	.section	.text._ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE,"axG",@progbits,_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE,comdat
	.weak	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE
	.type	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE, @function
_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE:
.LFB2071:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2071:
	.size	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE, .-_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE
	.section	.text._ZNSt11_Tuple_implILm2EIEEC2Ev,"axG",@progbits,_ZNSt11_Tuple_implILm2EIEEC5Ev,comdat
	.align 2
	.weak	_ZNSt11_Tuple_implILm2EIEEC2Ev
	.type	_ZNSt11_Tuple_implILm2EIEEC2Ev, @function
_ZNSt11_Tuple_implILm2EIEEC2Ev:
.LFB2075:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2075:
	.size	_ZNSt11_Tuple_implILm2EIEEC2Ev, .-_ZNSt11_Tuple_implILm2EIEEC2Ev
	.weak	_ZNSt11_Tuple_implILm2EJEEC2Ev
	.set	_ZNSt11_Tuple_implILm2EJEEC2Ev,_ZNSt11_Tuple_implILm2EIEEC2Ev
	.weak	_ZNSt11_Tuple_implILm2EIEEC1Ev
	.set	_ZNSt11_Tuple_implILm2EIEEC1Ev,_ZNSt11_Tuple_implILm2EIEEC2Ev
	.weak	_ZNSt11_Tuple_implILm2EJEEC1Ev
	.set	_ZNSt11_Tuple_implILm2EJEEC1Ev,_ZNSt11_Tuple_implILm2EIEEC1Ev
	.section	.text._ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_,"axG",@progbits,_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC5IS2_vEEOT_,comdat
	.align 2
	.weak	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_
	.type	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_, @function
_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_:
.LFB2078:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2078:
	.size	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_, .-_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_
	.weak	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC1IS2_vEEOT_
	.set	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC1IS2_vEEOT_,_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_
	.section	.text._ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_,"axG",@progbits,_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC5IS2_IEvEEOT_DpOT0_,comdat
	.align 2
	.weak	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_
	.type	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_, @function
_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_:
.LFB2080:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt11_Tuple_implILm2EIEEC2Ev
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EEC2IS2_vEEOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2080:
	.size	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_, .-_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_
	.weak	_ZNSt11_Tuple_implILm1EJSt14default_deleteI6ObjectEEEC2IS2_JEvEEOT_DpOT0_
	.set	_ZNSt11_Tuple_implILm1EJSt14default_deleteI6ObjectEEEC2IS2_JEvEEOT_DpOT0_,_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_
	.weak	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC1IS2_IEvEEOT_DpOT0_
	.set	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC1IS2_IEvEEOT_DpOT0_,_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_
	.weak	_ZNSt11_Tuple_implILm1EJSt14default_deleteI6ObjectEEEC1IS2_JEvEEOT_DpOT0_
	.set	_ZNSt11_Tuple_implILm1EJSt14default_deleteI6ObjectEEEC1IS2_JEvEEOT_DpOT0_,_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC1IS2_IEvEEOT_DpOT0_
	.section	.text._ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_,"axG",@progbits,_ZNSt10_Head_baseILm0EP6ObjectLb0EEC5IRS1_vEEOT_,comdat
	.align 2
	.weak	_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_
	.type	_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_, @function
_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_:
.LFB2083:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, (%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2083:
	.size	_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_, .-_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_
	.weak	_ZNSt10_Head_baseILm0EP6ObjectLb0EEC1IRS1_vEEOT_
	.set	_ZNSt10_Head_baseILm0EP6ObjectLb0EEC1IRS1_vEEOT_,_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_
	.section	.text._ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_,"axG",@progbits,_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC5IRS1_IS3_EvEEOT_DpOT0_,comdat
	.align 2
	.weak	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_
	.type	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_, @function
_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_:
.LFB2085:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEEC2IS2_IEvEEOT_DpOT0_
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSt10_Head_baseILm0EP6ObjectLb0EEC2IRS1_vEEOT_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2085:
	.size	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_, .-_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_
	.weak	_ZNSt11_Tuple_implILm0EJP6ObjectSt14default_deleteIS0_EEEC2IRS1_JS3_EvEEOT_DpOT0_
	.set	_ZNSt11_Tuple_implILm0EJP6ObjectSt14default_deleteIS0_EEEC2IRS1_JS3_EvEEOT_DpOT0_,_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_
	.weak	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC1IRS1_IS3_EvEEOT_DpOT0_
	.set	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC1IRS1_IS3_EvEEOT_DpOT0_,_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_
	.weak	_ZNSt11_Tuple_implILm0EJP6ObjectSt14default_deleteIS0_EEEC1IRS1_JS3_EvEEOT_DpOT0_
	.set	_ZNSt11_Tuple_implILm0EJP6ObjectSt14default_deleteIS0_EEEC1IRS1_JS3_EvEEOT_DpOT0_,_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC1IRS1_IS3_EvEEOT_DpOT0_
	.section	.text._ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_,"axG",@progbits,_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC5IRS1_S3_vEEOT_OT0_,comdat
	.align 2
	.weak	_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_
	.type	_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_, @function
_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_:
.LFB2087:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardISt14default_deleteI6ObjectEEOT_RNSt16remove_referenceIS3_E4typeE
	movq	%rax, %rbx
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt7forwardIRP6ObjectEOT_RNSt16remove_referenceIS3_E4typeE
	movq	%rax, %rcx
	movq	-24(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEEC2IRS1_IS3_EvEEOT_DpOT0_
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2087:
	.size	_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_, .-_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_
	.weak	_ZNSt5tupleIJP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_
	.set	_ZNSt5tupleIJP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_,_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_
	.weak	_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC1IRS1_S3_vEEOT_OT0_
	.set	_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC1IRS1_S3_vEEOT_OT0_,_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC2IRS1_S3_vEEOT_OT0_
	.weak	_ZNSt5tupleIJP6ObjectSt14default_deleteIS0_EEEC1IRS1_S3_vEEOT_OT0_
	.set	_ZNSt5tupleIJP6ObjectSt14default_deleteIS0_EEEC1IRS1_S3_vEEOT_OT0_,_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC1IRS1_S3_vEEOT_OT0_
	.section	.text._ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_,"axG",@progbits,_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC5EPS0_,comdat
	.align 2
	.weak	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_
	.type	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_, @function
_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_:
.LFB2089:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	leaq	-1(%rbp), %rdx
	leaq	-32(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_ZNSt5tupleIIP6ObjectSt14default_deleteIS0_EEEC1IRS1_S3_vEEOT_OT0_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2089:
	.size	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_, .-_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_
	.weak	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC1EPS0_
	.set	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC1EPS0_,_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EEC2EPS0_
	.section	.text._ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_,"axG",@progbits,_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_,comdat
	.weak	_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_
	.type	_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_, @function
_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_:
.LFB2095:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2095:
	.size	_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_, .-_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_
	.section	.text._ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_,"axG",@progbits,_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_,comdat
	.weak	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_
	.type	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_, @function
_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_:
.LFB2094:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_Head_baseILm0EP6ObjectLb0EE7_M_headERS2_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2094:
	.size	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_, .-_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_
	.weak	_ZNSt11_Tuple_implILm0EJP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_
	.set	_ZNSt11_Tuple_implILm0EJP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_,_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_
	.section	.text._ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE,"axG",@progbits,_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE,comdat
	.weak	_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE
	.type	_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE, @function
_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE:
.LFB2093:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt11_Tuple_implILm0EIP6ObjectSt14default_deleteIS0_EEE7_M_headERS4_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2093:
	.size	_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE, .-_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE
	.weak	_ZSt12__get_helperILm0EP6ObjectJSt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EJS5_DpT1_EE
	.set	_ZSt12__get_helperILm0EP6ObjectJSt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EJS5_DpT1_EE,_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE
	.section	.text._ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_,"axG",@progbits,_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_,comdat
	.weak	_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	.type	_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_, @function
_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_:
.LFB2092:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt12__get_helperILm0EP6ObjectISt14default_deleteIS0_EEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS5_DpT1_EE
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2092:
	.size	_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_, .-_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	.weak	_ZSt3getILm0EJP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEE4typeERS9_
	.set	_ZSt3getILm0EJP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEE4typeERS9_,_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	.section	.text._ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED5Ev,comdat
	.align 2
	.weak	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev
	.type	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev, @function
_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev:
.LFB2096:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt3getILm0EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L40
	movq	-24(%rbp), %rax
	movq	(%rax), %rbx
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNKSt14default_deleteI6ObjectEclEPS0_
.L40:
	movq	-24(%rbp), %rax
	movq	$0, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2096:
	.size	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev, .-_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev
	.weak	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED1Ev
	.set	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED1Ev,_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EED2Ev
	.section	.text._ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_,"axG",@progbits,_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_,comdat
	.weak	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_
	.type	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_, @function
_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_:
.LFB2147:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2147:
	.size	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_, .-_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_
	.section	.text._ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_,"axG",@progbits,_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_,comdat
	.weak	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_
	.type	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_, @function
_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_:
.LFB2146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt10_Head_baseILm1ESt14default_deleteI6ObjectELb1EE7_M_headERS3_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2146:
	.size	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_, .-_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_
	.weak	_ZNSt11_Tuple_implILm1EJSt14default_deleteI6ObjectEEE7_M_headERS3_
	.set	_ZNSt11_Tuple_implILm1EJSt14default_deleteI6ObjectEEE7_M_headERS3_,_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_
	.section	.text._ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE,"axG",@progbits,_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE,comdat
	.weak	_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE
	.type	_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE, @function
_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE:
.LFB2145:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSt11_Tuple_implILm1EISt14default_deleteI6ObjectEEE7_M_headERS3_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2145:
	.size	_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE, .-_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE
	.weak	_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEJEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EJS4_DpT1_EE
	.set	_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEJEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EJS4_DpT1_EE,_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE
	.section	.text._ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_,"axG",@progbits,_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_,comdat
	.weak	_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	.type	_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_, @function
_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_:
.LFB2144:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt12__get_helperILm1ESt14default_deleteI6ObjectEIEENSt9__add_refIT0_E4typeERSt11_Tuple_implIXT_EIS4_DpT1_EE
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2144:
	.size	_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_, .-_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	.weak	_ZSt3getILm1EJP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEE4typeERS9_
	.set	_ZSt3getILm1EJP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIJDpT0_EEE4typeEE4typeERS9_,_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	.section	.text._ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv,"axG",@progbits,_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv,comdat
	.align 2
	.weak	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv
	.type	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv, @function
_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv:
.LFB2143:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZSt3getILm1EIP6ObjectSt14default_deleteIS0_EEENSt9__add_refINSt13tuple_elementIXT_ESt5tupleIIDpT0_EEE4typeEE4typeERS9_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2143:
	.size	_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv, .-_ZNSt10unique_ptrI6ObjectSt14default_deleteIS0_EE11get_deleterEv
	.section	.text._ZNKSt14default_deleteI6ObjectEclEPS0_,"axG",@progbits,_ZNKSt14default_deleteI6ObjectEclEPS0_,comdat
	.align 2
	.weak	_ZNKSt14default_deleteI6ObjectEclEPS0_
	.type	_ZNKSt14default_deleteI6ObjectEclEPS0_, @function
_ZNKSt14default_deleteI6ObjectEclEPS0_:
.LFB2148:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L52
	movq	%rbx, %rdi
	call	_ZN6ObjectD1Ev
	movq	%rbx, %rdi
	call	_ZdlPv
.L52:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2148:
	.size	_ZNKSt14default_deleteI6ObjectEclEPS0_, .-_ZNKSt14default_deleteI6ObjectEclEPS0_
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB2217:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L54
	cmpl	$65535, -8(%rbp)
	jne	.L54
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	call	__cxa_atexit
.L54:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2217:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.section	.rodata
	.align 4
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, @object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.long	2
	.text
	.type	_GLOBAL__sub_I__Z5mainxv, @function
_GLOBAL__sub_I__Z5mainxv:
.LFB2218:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2218:
	.size	_GLOBAL__sub_I__Z5mainxv, .-_GLOBAL__sub_I__Z5mainxv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z5mainxv
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
	.section	.note.GNU-stack,"",@progbits
