#include <iostream>
#include <memory>

using namespace std;


class Object
{
public:
	Object()
	{
		cout << "Object constructor" << endl;
	}
	~Object()
	{
		cout << "Object destructor" << endl;
	}
};

Object * get_object();

class Container
{
private:
	unique_ptr<Object> o;
public:
	Container() : o { get_object() }
	{
		cout << "Container constructor" << endl;
	}
	~Container() 
	{ 
		cout << "Container destructor" << endl;
	}
};




int mainx()
{
	Container c;
	cout << "allocated new Container" << endl;

	cout << "exiting program" << endl;
	return 0;
}

Object * get_object()
{
	Object * p = new Object();
	return p;
}