#include <json.h>
#include <logging.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include "user_service.h"
#include "db/resultset_resource.h"

using namespace std;

UserService::UserService(const string& url, const string& schema, const string& username, const string& password) : 
	conn { get_driver_instance()->connect(url, username, password) }
{
	CROW_LOG_INFO << "setting schema and preparing statement";

	try {
		conn->setSchema(schema);
		pstmt = unique_ptr<sql::PreparedStatement> { conn->prepareStatement(SQL_FIND_USER_BY_PATTERN) };
	}
	catch (sql::SQLException& e) {
		CROW_LOG_ERROR << "# ERR: SQLException in " << __FILE__;
	  	CROW_LOG_ERROR << "(" << __func__ << ") on line " << __LINE__;
	  	CROW_LOG_ERROR << "# ERR: " << e.what();
	  	CROW_LOG_ERROR << " (MySQL error code: " << e.getErrorCode();
	  	CROW_LOG_ERROR << ", SQLState: " << e.getSQLState() << " )";

		throw;
	}
}


crow::json::wvalue UserService::test()
{
	crow::json::wvalue result;
	crow::json::wvalue embedded;
	crow::json::wvalue users;

	vector<string> data {"a", "b"};
	size_t i = 0;
	for (auto & s : data) {
		crow::json::wvalue u;
		u["id"] = s;
		u["last_name"] = s;
		u["first_name"] = s;
		
		users[i++] = move(u);
	}

	embedded["user"] = move(users);
	result["_embedded"] = move(embedded);
	return result;
}


crow::json::wvalue UserService::findByPattern(const string& pattern)
{
	crow::json::wvalue result;
	crow::json::wvalue embedded;
	crow::json::wvalue users;

	unique_ptr<sql::ResultSet> res;
	try {
		// pstmt->setString(1, pattern + "%");
		// Php::out << "prepared select statement" << endl;

		// Php::out << (res ? "res is initialized" : "res is nullptr") << endl;
		// res = pstmt->executeQuery();
		// Php::out << (res ? "got a result" : "no result") << endl;
		// Php::out << "column count: " << res->getMetaData()->getColumnCount() << endl;

		pstmt->setString(1, pattern + "%");
		res = unique_ptr<sql::ResultSet> { pstmt->executeQuery() };
		int i = 0;

		while (res->next()) {
			// Php::out << "adding user " << i;
			crow::json::wvalue u;

			u["id"] = res->getInt("id");
			u["user_name"] = res->getString("username").asStdString();
			u["last_name"] = res->getString("lastname").asStdString();
			u["first_name"] = res->getString("firstname").asStdString();
			u["email"] = res->getString("email").asStdString();

			users[i++] = move(u);
			// Php::out << ", username = " << res->getString("username") << endl;
		}
	}
	catch(sql::SQLException& e) {
		cout << "# ERR: SQLException in " << __FILE__;
	  	cout << "(" << __func__ << ") on line " << __LINE__ << endl;
	  	cout << "# ERR: " << e.what();
	  	cout << " (MySQL error code: " << e.getErrorCode();
	  	cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	embedded["user"] = move(users);
	result["_embedded"] = move(embedded);	
	
	return result;
}