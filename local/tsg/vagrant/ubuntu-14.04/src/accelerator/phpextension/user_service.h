#ifndef __USER_SERVICE_H__
#define __USER_SERVICE_H__

#include <memory>
#include <string>
#include <phpcpp.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

using namespace std;

class UserService : public Php::Base {

private:
	unique_ptr<sql::Connection> conn;
	unique_ptr<sql::PreparedStatement> pstmt;
	
public:
	UserService();

	Php::Value test();
	void testResponse();
	Php::Value findByPattern(Php::Parameters& params);
	void outputByPattern(Php::Parameters& params);

private:
	unique_ptr<sql::ResultSet> matches(const string& pattern);

private:
	static constexpr int LIMIT = 10;
	const string SQL_FIND_USER_BY_PATTERN = "select id,username,lastname,firstname,email \
			from user \
			where lastname like ? \
			order by lastname,firstname \
			limit 10";
};

#endif /* __USER_SERVICE_H__ */
