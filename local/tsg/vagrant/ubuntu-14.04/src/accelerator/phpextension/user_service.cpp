#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <array>
#include <iostream>
#include <memory>
#include <string>
#include "user_service.h"

using namespace std;

UserService::UserService() : 
	conn { get_driver_instance()->connect("tcp://127.0.0.1:3306", "moodle_user", "learning1") }
{
	try {
		conn->setAutoCommit(false);
		cout << "set autocommit to false" << endl;
		conn->setSchema("moodle_dcfs_dev");
		pstmt = unique_ptr<sql::PreparedStatement> { conn->prepareStatement(SQL_FIND_USER_BY_PATTERN) };
	}
	catch (sql::SQLException& e) {
		cout << "# ERR: SQLException in " << __FILE__ << endl;
	  	cout << "(" << __func__ << ") on line " << __LINE__ << endl;
	  	cout << "# ERR: " << e.what() << endl;
	  	cout << " (MySQL error code: " << e.getErrorCode() << endl;
	  	cout << ", SQLState: " << e.getSQLState() << " )";

		throw;
	}
}

Php::Value UserService::test()
{
	Php::Object result;
	Php::Object embedded;
	Php::Array users;

	Php::Object u;
	u["name"] = "Steve";
	users[0] = u;

	embedded["user"] = users;
	result["embedded"] = embedded;

	return result;
}

/*
 * Public interface
 */

void UserService::testResponse()
{
	Php::out << "{ \"message\" : \"testResponse\" }" << endl;
}

Php::Value UserService::findByPattern(Php::Parameters& params)
{
	string pattern = params[0];

	Php::Object result;
	Php::Object embedded;
	Php::Array users;

	try {
		unique_ptr<sql::ResultSet> res = matches(pattern);

		int i = 0;

		while (res->next()) {
			Php::Object u;

			u["id"] = res->getInt("id");
			u["user_name"] = res->getString("username").asStdString();
			u["last_name"] = res->getString("lastname").asStdString();
			u["first_name"] = res->getString("firstname").asStdString();
			u["email"] = res->getString("email").asStdString();

			users[i++] = u;
		}
	}
	catch(sql::SQLException& e) {
		Php::out << "# ERR: SQLException in " << __FILE__;
	  	Php::out << "(" << __func__ << ") on line " << __LINE__ << endl;
	  	Php::out << "# ERR: " << e.what();
	  	Php::out << " (MySQL error code: " << e.getErrorCode();
	  	Php::out << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	embedded["user"] = users;
	result["_embedded"] = embedded;	
	
	return result;
}

void UserService::outputByPattern(Php::Parameters& params)
{
	unique_ptr<sql::ResultSet> rs = matches(params[0]);

	string newline { "\r\n" };
	array<string, LIMIT> a;
	for (int i = 0; i < LIMIT; ++i) {
		if (!rs->next())
			break;

		string s { "      { " };
		s.append(newline);

		s.append("        \"id\": \"");
		s.append(to_string(rs->getInt("id")));
		s.append("\",");
		s.append(newline);

		s.append("        \"user_name\": \"");
		s.append(rs->getString("username"));
		s.append("\",");
		s.append(newline);

		s.append("        \"last_name\": \"");
		s.append(rs->getString("lastname"));
		s.append("\",");
		s.append(newline);

		s.append("        \"first_name\": \"");
		s.append(rs->getString("firstname"));
		s.append("\"");
		s.append(newline);

		s.append("      }");
		if (i < LIMIT - 1)
			s.append(",");

		a[i] = s;
	}

	Php::out << "{" << endl;
	Php::out << " \"_embedded\":" << endl;
	Php::out << "  {" << endl;
	Php::out << "    \"user\":" << endl;
	Php::out << "    [" << endl;
	for (auto& s : a) {
		Php::out << s << endl;
	}
	Php::out << "    ]" << endl;
	Php::out << "  }" << endl;
	Php::out << "}" << endl;
}


/*
 * Private methods
 */

unique_ptr<sql::ResultSet> UserService::matches(const string& pattern)
{
	this->pstmt->setString(1, pattern + "%");
	return unique_ptr<sql::ResultSet> { pstmt->executeQuery() };
}