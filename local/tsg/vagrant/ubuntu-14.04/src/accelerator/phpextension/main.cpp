#include <phpcpp.h>
#include <iostream>
#include "user_service.h"
#include "yo.h"

void hello_from_accelerator();
void hello_from_accelerator()
{
    std::cout << "Hello again from the TSG Moodle Accelerator PHP extension." << std::endl;
}

Php::Value local_tsg_find_users_by_pattern(Php::Parameters& params);
Php::Value local_tsg_find_users_by_pattern(Php::Parameters& params)
{
    Yo yo;
    Php::Value v;
    v["message"] = yo.yo();
    return v;
}

/**
 *  tell the compiler that the get_module is a pure C function
 */
extern "C" {
    
    /**
     *  Function that is called by PHP right after the PHP process
     *  has started, and that returns an address of an internal PHP
     *  strucure with all the details and features of your extension
     *
     *  @return void*   a pointer to an address that is understood by PHP
     */
    PHPCPP_EXPORT void *get_module() 
    {
        // static(!) Php::Extension object that should stay in memory
        // for the entire duration of the process
        static Php::Extension extension("tsgmoodleaccelerator", "1.0");
        
        extension.add("hello_from_accelerator", hello_from_accelerator);
        extension.add("local_tsg_find_users_by_pattern", local_tsg_find_users_by_pattern);

        Php::Class<UserService> userService("TSG\\Accelerator\\UserService");
        userService.method("findByPattern", &UserService::findByPattern);
        userService.method("outputByPattern", &UserService::outputByPattern);
        userService.method("test", &UserService::test);
        userService.method("testResponse", &UserService::testResponse);

        extension.add(std::move(userService));

        extension.onRequest([]() {            
            std::cout << "tsgmoodleaccelerator" << std::endl;
        });
        
        // return the extension
        return extension;
    }
}
