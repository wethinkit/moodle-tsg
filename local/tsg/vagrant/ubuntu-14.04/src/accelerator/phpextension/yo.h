#ifndef __YO_H__
#define __YO_H__

#include <string>

using namespace std;

class Yo
{
public:
	string yo();
};

#endif /* __YO_H__ */