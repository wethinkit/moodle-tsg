#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <iostream>
#include <string>

using namespace std;


int main(void) {
	sql::Driver * driver;
	sql::Connection * conn;
	sql::PreparedStatement * pstmt;
	sql::ResultSet * res;

	try {
		driver = get_driver_instance();
		conn = driver->connect("tcp://127.0.0.1:3306", "moodle_user", "learning1");
		cout << "successful connection" << endl;

		conn->setSchema("moodle_dcfs_dev");
		cout << "using moodle_dcfs_dev db" << endl;

		pstmt = conn->prepareStatement("select id,username,lastname,firstname,email from user limit 5");
		cout << "prepared select statement" << endl;

		res = pstmt->executeQuery();
		while (res->next()) {
			cout << "id = " << to_string(res->getInt("id"));
			cout << ", username = " << res->getString("username");
			cout << endl;
		}

		delete res;
		delete pstmt;
		delete conn;
	}
	catch(sql::SQLException& e) {
		cout << "# ERR: SQLException in " << __FILE__;
	  	cout << "(" << __func__ << ") on line " << __LINE__ << endl;
	  	cout << "# ERR: " << e.what();
	  	cout << " (MySQL error code: " << e.getErrorCode();
	  	cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

