#ifndef __USER_SERVICE_H__
#define __USER_SERVICE_H__

#include <phpcpp.h>
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>


class UserService : public Php::Base {
private:
	sql::Driver * driver;
	sql::Connection * conn;
	sql::PreparedStatement * pstmt;
public:
	UserService() 
	{
		driver = get_driver_instance();
		conn = driver->connect("tcp://127.0.0.1:3306", "moodle_user", "learning1");		
		conn->setSchema("moodle_dcfs_dev");
		pstmt = conn->prepareStatement("select id,username,lastname,firstname,email \
			from user \
			where lastname like ? \
			order by lastname,firstname \
			limit 10");
	}
	~UserService()
	{
		delete pstmt;
		delete conn;
	}

	Php::Value test();
	Php::Value findByPattern(Php::Parameters& params);
};

#endif /* __USER_SERVICE_H__ */
