#include <phpcpp.h>
#include <iostream>
#include "howdy.h"
#include "yo.h"
#include "user_service.h"

void hello_world();
void hello_world()
{
    Php::out << "Hello World" << std::endl;
}

/**
 *  tell the compiler that the get_module is a pure C function
 */
extern "C" {
    
    /**
     *  Function that is called by PHP right after the PHP process
     *  has started, and that returns an address of an internal PHP
     *  strucure with all the details and features of your extension
     *
     *  @return void*   a pointer to an address that is understood by PHP
     */
    PHPCPP_EXPORT void *get_module() 
    {
        // static(!) Php::Extension object that should stay in memory
        // for the entire duration of the process (that's why it's static)
        static Php::Extension extension("firstextension", "1.0");
        
        // @todo    add your own functions, classes, namespaces to the extension
        extension.add("hello_world", hello_world);
        extension.add("howdy", howdy);
        extension.add("hey", hey);

        Php::Class<Yo> yo("Yo");
        yo.method("speak", &Yo::speak);
        yo.method("speakThis", &Yo::speakThis);
        yo.method("message", &Yo::message);
        extension.add(std::move(yo));

        Php::Class<UserService> userService("UserService");
        userService.method("findByPattern", &UserService::findByPattern);
        userService.method("test", &UserService::test);
        extension.add(std::move(userService));
        
        // return the extension
        return extension;
    }
}
