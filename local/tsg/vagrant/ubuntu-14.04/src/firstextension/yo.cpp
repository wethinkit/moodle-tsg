#include <phpcpp.h>
#include <iostream>
#include <string>
#include "yo.h"

int Yo::count = 0;
const char * Yo::ids = "abcde";

Yo::Yo()
{
	id = 1;
}

void Yo::speak()
{
	std::cout << "Yo" << std::endl;
}

void Yo::speakThis(Php::Parameters& params)
{
	std::string s = params[0];
	std::cout << s << std::endl;
}

Php::Value Yo::message()
{
	
	std::string s {"Hello from "};
	s += id;
	s += ": ";
	return s + std::to_string(++Yo::count);
}