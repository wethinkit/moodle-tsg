#include <phpcpp.h>
#include <iostream>
#include "howdy.h"

void howdy()
{
	Php::out << "Howdy" << std::endl;
}

void hey(Php::Parameters& params)
{
	Php::Object o = params[0];
	Php::out << o.call("getMessage") << std::endl;
}