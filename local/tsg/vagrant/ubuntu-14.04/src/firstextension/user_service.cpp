#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <iostream>
#include <string>
#include "user_service.h"

using namespace std;

Php::Value UserService::test()
{
	Php::Object result;
	Php::Object embedded;
	Php::Array users;

	Php::Object u;
	u["name"] = "Steve";
	users[0] = u;

	embedded["user"] = users;
	result["embedded"] = embedded;

	return result;
}

Php::Value UserService::findByPattern(Php::Parameters& params)
{
	string pattern = params[0];

	Php::Object result;
	Php::Object embedded;
	Php::Array users;

	sql::ResultSet * res = nullptr;

	try {
		pstmt->setString(1, pattern + "%");
		// Php::out << "prepared select statement" << endl;

		// Php::out << (res ? "res is initialized" : "res is nullptr") << endl;
		res = pstmt->executeQuery();
		// Php::out << (res ? "got a result" : "no result") << endl;
		// Php::out << "column count: " << res->getMetaData()->getColumnCount() << endl;

		int i = 0;

		while (res->next()) {
			// Php::out << "adding user " << i;
			Php::Object u;

			u["id"] = res->getInt("id");
			u["user_name"] = res->getString("username").asStdString();
			u["last_name"] = res->getString("lastname").asStdString();
			u["first_name"] = res->getString("firstname").asStdString();
			u["email"] = res->getString("email").asStdString();

			users[i++] = u;
			// Php::out << ", username = " << res->getString("username") << endl;
		}

		delete res;
	}
	catch(sql::SQLException& e) {
		Php::out << "# ERR: SQLException in " << __FILE__;
	  	Php::out << "(" << __func__ << ") on line " << __LINE__ << endl;
	  	Php::out << "# ERR: " << e.what();
	  	Php::out << " (MySQL error code: " << e.getErrorCode();
	  	Php::out << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	embedded["user"] = users;
	result["_embedded"] = embedded;	
	
	return result;
}