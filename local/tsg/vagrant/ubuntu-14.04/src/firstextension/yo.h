#ifndef __yo_h__
#define __yo_h__

#include <string>
#include <phpcpp.h>

class Yo : public Php::Base {	
private:
	static int count;
	static const char * ids;
	int id;

public:
	Yo();
	void speak();
	void speakThis(Php::Parameters& params);
	Php::Value message();
};

#endif /* __yo_h__ */