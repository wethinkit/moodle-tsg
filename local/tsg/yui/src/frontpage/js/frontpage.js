
var api = new M.local_tsg.api();

function fetchAssignments(callback, errback)
{
    var url = M.local_tsg.defaults.contextRoot +
        M.local_tsg.frontpage.params.urls.frontpage.incompletesForCurrentUser;

    Y.io(url, {
        method   : 'get',
        headers  : { 'Accept': 'application/hal+json' },
        on       : {
            success: function (transactionId, response)
            {
                api.parseResponse(response.response, callback, errback);
            },

            failure: function (transactionId, response)
            {
                var problem = api.parseFailure(response.response);
                errback(problem);
            }
        }
    });
}

function fetchCompletions(callback, errback)
{
    var url = M.local_tsg.defaults.contextRoot +
        M.local_tsg.frontpage.params.urls.frontpage.completionsForCurrentUser;

    Y.io(url, {
        method  : 'get',
        headers : { 'Accept': 'application/hal+json' },
        on      : {
            success: function (transactionId, response)
            {
                api.parseResponse(response.response, callback, errback);
            },

            failure: function (transactionId, response)
            {
                var problem = api.parseFailure(response.response);
                errback(problem);
            }
        }
    });
}


M.local_tsg = M.local_tsg || {};

M.local_tsg.frontpage = {

    init: function(params)
    {
        window.console = window.console || function () {};
        M.local_tsg.frontpage.params = params;
        M.local_tsg.widgets.tree('#tsg-course-catalog-tree', M.local_tsg.frontpage.fetchCategory);

        var viewModel = M.local_tsg.frontpage.viewModel = new M.local_tsg.frontpage.ViewModel(params);
        M.local_tsg.ko.applyBindings(viewModel);
    },

    ViewModel: function(params)
    {
        var ko = M.local_tsg.ko;
        var self = this;
        self.params = params;

        self.courseCatalogState = ko.observable('fetching'); // fetching|loaded
        self.courseCatalogError = ko.observable('');
        self.myCoursesState     = ko.observable('fetching'); // fetching|loaded|error
        self.myCoursesError     = ko.observable('');
        self.myTranscriptState  = ko.observable('fetching'); // fetching|loaded|error
        self.myTranscriptError  = ko.observable('');


        self.assignments = ko.observableArray([]);
        self.completions = ko.observableArray([]);


        self.gotoCoursePage = function (completion)
        {
            var url       = '/course/view.php?id=' + completion.item.course.id;
            location.href = url;
        };

        self.certificate = function (completion)
        {
            var url = completion._links.certificate.href;
            url = url.replace('__gradeid__', completion.id);
            window.open(url);
        };


        self.assignmentSessionField = function (assignment, fieldName)
        {
            var session = assignment.session_enrollment ?
                assignment.session_enrollment.session : false;

            return session ? session[fieldName] : '';
        };

        self.completionSessionField = function (completion, fieldName)
        {
            var session = completion.item.session;
            return session ? session[fieldName] : '';
        };

        // fetch the assignments for 'my courses'
        fetchAssignments(
            function (data)
            {
                self.myCoursesState('loaded');

                if (data) {
                    self.assignments.push.apply(self.assignments, data._embedded.courseAssignment);
                }
            },
            function (err)
            {
                self.myCoursesState('error');

                var errType = api.problemTypeFromURI(err.type);
                if ('notloggedin' === errType) {
                    // self.myCoursesError(
                    //    M.util.get_string('mycourses.problem.notloggedin.detail', 'local_tsg'));
                    // TODO: get string manager above working
                    self.myCoursesError('You must be logged in to see My Courses');
                }
                else {
                    self.myCoursesError(err.title + ': ' + err.detail);
                }
            }
        );

        // fetch the completions for 'my transcript'
        fetchCompletions(
            function (data)
            {
                self.myTranscriptState('loaded');

                if (data) {
                    self.completions.push.apply(self.completions, data._embedded.grade);
                }
            },
            function (err)
            {
                self.myTranscriptState('error');

                var errType = api.problemTypeFromURI(err.type);
                if ('notloggedin' === errType) {
                    // self.myTranscriptError(
                    //    M.util.get_string('mytranscript.problem.notloggedin.detail', 'local_tsg'));
                    // TODO: get string manager above working
                    self.myTranscriptError('You must be logged in to see My Transcript');
                }
                else {
                    self.myTranscriptError(err.title + ': ' + err.detail);
                }
            }
        );
    },

    fetchCategory: function (id, callback)
    {
        var url = M.local_tsg.frontpage.params.urls.course_category.children;
        url = url.replace('__categoryid__', id);
        url = M.local_tsg.defaults.contextRoot + url;

        Y.io(url, {
            method   : "GET",
            headers  : { 'Accept': 'application/hal+json' },
            on       : {
                success: function (transactionId, response) {

                    M.local_tsg.frontpage.viewModel.courseCatalogState('loaded');

                    try {
                        var r = Y.JSON.parse(response.response);

                        if (!('courseCategory' in r._embedded)) {
                            callback([]);
                            return;
                        }

                        var categories = r._embedded.courseCategory.map(function (cat) {

                            var leaves = cat.courses.map(function (course) {

                                return {
                                    id    : course.id,
                                    name  : course.full_name,

                                    click : function (course) {
                                        var url = M.local_tsg.frontpage.params.urls.course.view;
                                        url = url.replace('__courseid__', course.id);
                                        window.location.href = url;
                                    }
                                };
                            });

                            return {
                                id      : cat.id,
                                name    : cat.name,
                                leaves  : leaves
                            };
                        });

                        console.log(categories);
                        callback(categories);
                    }
                    catch (ex) {
                        alert('Could not understand response from server');
                    }
                }
            }
        });
    }
};
