M.local_tsg = M.local_tsg || {};

M.local_tsg.assignmentmanagement =
{
    init: function (params) {

        var viewModel = M.local_tsg.assignmentmanagement.setupViewModel(params);
        M.local_tsg.ko.applyBindings(viewModel);

        viewModel.fetchSessions();
    },

    ViewModel: function (params)
    {
        var self = this;
        self.params = params;
        self.courseId = params.courseId;
        var ko = M.local_tsg.ko;

        self.selectedUser = ko.observable(false);

        /*
         * Assignment Form
         */

        self.assignment =
        {
            deadlineMonth: ko.observable(false),
            deadlineDay: ko.observable(false),
            deadlineYear: ko.observable(false),
            reason: ko.observable(),
            repeatInterval: ko.observable(),
            repeatPeriod: ko.observable(false)
        };


        /*
         * Enrollment Form
         */

        self.enrollment =
        {
            selectedSession: ko.observable(false)
        };


        /*
         * Search Form
         */

        self.search =
        {
            deadlineStartMonth: ko.observable(false),
            deadlineStartDay: ko.observable(false),
            deadlineStartYear: ko.observable(false),

            deadlineEndMonth: ko.observable(false),
            deadlineEndDay: ko.observable(false),
            deadlineEndYear: ko.observable(false),
            
            filterForCompletes: ko.observable(false),
            filterForIncompletes: ko.observable(false),

            results: ko.observableArray()
        };


        /*
         * Messages
         */

        self.validationErrors = ko.observableArray(false);
        self.serverError = ko.observable(false);
        self.showAssignSuccess = ko.observable(false);

        self.clearValidationErrors = function()
        {
            self.validationErrors.removeAll();
        };

        self.clearServerError = function()
        {
            self.serverError(false);
        };

        self.hideAssignSuccess = function()
        {
            self.showAssignSuccess(false);
        };


        /*
         * Actions
         */

        self.fetchSessions = function()
        {
            var url = self.params.urls.course.sessions;
            url = url.replace('__courseid__', self.courseId);
            Y.io(url, {
                method: "GET",
                headers: { "Accept": "application/hal+json" },
                on: {
                    success: function(transactionId, response) {
                        var r = Y.JSON.parse(response.response);
                        var sessions = r._embedded.courseSession;
                        self.choices.sessions(sessions);
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.pickSession = function(session)
        {
            self.enrollment.selectedSession(session);
            $("#tsg-pick-session-dialog").modal("hide");
        };

        self.unPickSession = function()
        {
            self.enrollment.selectedSession(false);
        };

        self.isWorking = false;

        self.isAssigning = false;
        self.assignUser = function(data, event)
        {
            if (self.isWorking) {
                return;
            }

            self.showAssignSuccess(false);
            self.clearValidationErrors();
            self.clearServerError();

            if (!self.validate()) {
                return;
            }

            self.isWorking = true;
            self.isAssigning = true;
            $(event.target).button("loading");

            var url = self.params.urls.assignment.assignAndEnrollUser;

            Y.io(url, {
                method: "POST",
                headers: { "Accept": "application/hal+json" },
                data: Y.JSON.stringify({
                    userId: self.selectedUser().id,
                    assignment: {
                        courseId: self.courseId,
                        deadline: M.local_tsg.moment.utc({
                            year: self.assignment.deadlineYear(),
                            month: self.assignment.deadlineMonth().num,
                            day: self.assignment.deadlineDay()
                        }).format("YYYY-MM-DDTHH:mm:ssZ"),
                        reason: self.assignment.reason(),
                        repeatInterval: self.assignment.repeatInterval(),
                        repeatPeriod: self.assignment.repeatPeriod()
                    },
                    enrollment: {
                        sessionId: self.enrollment.selectedSession().id
                    }
                }),
                "arguments": event.target,
                on: {
                    success: function(/* transactionId, response */)
                    {
                        self.showAssignSuccess(true);
                    },

                    failure: function(transactionId, response)
                    {
                        try {
                            var r = Y.JSON.parse(response.response);
                            self.serverError(r);
                        }
                        catch(ex) {
                            self.serverError('Could not understand response from server');
                        }
                    },

                    end: function(transactionId, buttonElement)
                    {
                        self.isWorking = false;
                        self.isAssigning = false;
                        $(buttonElement).button('reset');
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete
                }
            });
        };

        self.isSearching = ko.observable(false);
        self.searchAction = function(data, event)
        {
            if (self.isWorking) {
                return;
            }

            self.isWorking = true;
            self.isSearching(true);
            $(event.target).button("loading");

            var url = self.params.urls.assignment.search;
            
            var form = self.search;
            form.results([]);

            var searchParams = {
                courseId             : self.courseId,
                filterForCompletes   : form.filterForCompletes(),
                filterForIncompletes : form.filterForIncompletes()
            };

            if (form.deadlineStartYear() && form.deadlineStartMonth() && form.deadlineStartDay()) {
                searchParams.deadlineStart = M.local_tsg.moment.utc({
                    year  : form.deadlineStartYear(),
                    month : form.deadlineStartMonth().num,
                    day   : form.deadlineStartDay()
                }).format("YYYY-MM-DDTHH:mm:ssZ");
            }
            
            if (form.deadlineEndYear() && form.deadlineEndMonth() && form.deadlineEndDay()) {
                searchParams.deadlineEnd = M.local_tsg.moment.utc({
                    year  : form.deadlineEndYear(),
                    month : form.deadlineEndMonth().num,
                    day   : form.deadlineEndDay()
                }).format("YYYY-MM-DDTHH:mm:ssZ");
            }

            Y.io(url, {
                method: "GET",
                headers: { "Accept": "application/hal+json" },
                data: searchParams,
                "arguments": event.target,
                on: {
                    success: function(transactionId, response)
                    {
                        var parsed = Y.JSON.parse(response.response);
                        if (!parsed._embedded || parsed._embedded.empty) {
                            return;
                        }
                        var assignments = parsed._embedded.courseAssignment;
                        for (var i=0; i < assignments.length; ++i) {
                            var r = assignments[i];
                            r.isSelected = ko.observable(true);
                            self.search.results.push(r);
                        }
                    },

                    failure: function(transactionId, response)
                    {
                        try {
                            var r = Y.JSON.parse(response.response);
                            self.serverError(r);
                        }
                        catch(ex) {
                            self.serverError('Could not understand response from server');
                        }
                    },

                    end: function(transactionId, buttonElement)
                    {
                        self.isWorking = false;
                        self.isSearching(false);
                        $(buttonElement).button('reset');
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete
                }
            });
        };

        self.validate = function()
        {
            if (!self.assignment.deadlineYear() ||
                !self.assignment.deadlineMonth() ||
                !self.assignment.deadlineDay()) {

                self.validationErrors.push("Deadline is required.");
            }

            if (!self.selectedUser()) {
                self.validationErrors.push("You must select a user.");
            }

            return self.validationErrors().length === 0;
        };


        /*
         * Choices
         */

        self.choices =
        {
            months: [
                {num: 0, name: "Jan"},
                {num: 1, name: "Feb"},
                {num: 2, name: "Mar"},
                {num: 3, name: "Apr"},
                {num: 4, name: "May"},
                {num: 5, name: "Jun"},
                {num: 6, name: "Jul"},
                {num: 7, name: "Aug"},
                {num: 8, name: "Sep"},
                {num: 9, name: "Oct"},
                {num: 10, name: "Nov"},
                {num: 11, name: "Dec"}
            ],

            days: function (month, year) {
                if (!month) {
                    return [];
                }

                switch (month.num) {
                    case 0:
                    case 2:
                    case 4:
                    case 6:
                    case 7:
                    case 9:
                    case 11:
                        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
                    case 1:
                        return M.local_tsg.moment([year]).isLeapYear() ?
                            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29] :
                            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];
                    case 3:
                    case 5:
                    case 8:
                    case 10:
                        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
                }
            },

            years: ko.computed(function () {
                var year = new Date().getFullYear();
                return [year - 3, year - 2, year - 1, year,
                    year + 1, year + 2, year + 3, year + 4, year + 5, year + 6
                ];
            }),

            repeatPeriods: ["months", "years"],

            sessions: ko.observableArray()
        };

        self.choices.assignment =
        {
            deadlineDays: ko.computed(function () {
                return self.choices.days(self.assignment.deadlineMonth(),
                    self.assignment.deadlineYear());
            })
        };

        self.choices.search =
        {
            startDays: ko.computed(function () {
                return self.choices.days(self.search.deadlineStartMonth(),
                    self.search.deadlineStartYear());
            }),

            endDays: ko.computed(function () {
                return self.choices.days(self.search.deadlineEndMonth(),
                    self.search.deadlineEndYear());
            })
        };
    },

    setupViewModel: function(params)
    {
        var viewModel = new M.local_tsg.assignmentmanagement.ViewModel(params);
        M.local_tsg.assignmentmanagement.viewModel = viewModel;

        M.local_tsg.autocomplete.user('#tsg-select-user', params.urls.user.findByPattern,
            function (user) {
                M.local_tsg.assignmentmanagement.viewModel.selectedUser(user);
            },
            function () {
                M.local_tsg.assignmentmanagement.viewModel.selectedUser(false);
            }
        );

        $("#tsg-sessions-list").on("click", "[data-tsg-action='select']", function(e) {
            var session = M.local_tsg.ko.dataFor(e.target);
            viewModel.pickSession(session);
        });

        return viewModel;
    }
};
