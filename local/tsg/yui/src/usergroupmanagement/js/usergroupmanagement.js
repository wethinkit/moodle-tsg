M.local_tsg = M.local_tsg || {};


M.local_tsg.usergroupmanagement = {
    init: function(params)
    {
        M.local_tsg.usergroupmanagement.setupViewModel(params);
        M.local_tsg.ko.applyBindings(M.local_tsg.usergroupmanagement.viewModel);

        M.local_tsg.autocomplete.user('#select-user', params.urls.user.findByPattern,
        function(user) {
            M.local_tsg.usergroupmanagement.viewModel.selectedUser = user;
        },
        function() {
            M.local_tsg.usergroupmanagement.viewModel.selectedUser = false;
        });
    },

    ViewModel: function(params) {
        var self = this;
        self.params = params;
        var ko = M.local_tsg.ko;

        self.newGroupName = ko.observable();
        // for drop-down.  TODO:  consolidate with self.groups
        self.groupHiearchy = params.groupHiearchy;
        self.newGroupParent = ko.observable(false);

        // displayable data
        self.groups = ko.observableArray();
        self.currentGroup = false;
        self.members = ko.observableArray();
        self.managers = ko.observableArray();

        // for autocomplete
        self.selectedUser = false;

        self.isFetchingGroups = ko.observable(false);
        self.isFetchingMembers = ko.observable(false);
        self.isFetchingManagers = ko.observable(false);

        self.fetchGroups = function()
        {
            self.groups.removeAll();

            Y.io(self.params.urls.usergroup.fetch_all, {
                headers: {"Accept": "application/hal+json"},
                on: {
                    success: function(transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        self.displayGroups(data._embedded.userGroup);
                        console.log(data);
                    },

                    start: function(transactionId)
                    {
                        M.local_tsg.defaults.io.on.start(transactionId);
                        self.isFetchingGroups(true);
                    },

                    complete: function(transactionId, response)
                    {
                        M.local_tsg.defaults.io.on.complete(transactionId, response);
                        self.isFetchingGroups(false);
                    },

                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        },

        self.displayGroups = function (groups)
        {
            if (!groups) {
                return;
            }

            for (var i = 0; i < groups.length; ++i) {
                var g = groups[i];
                self.groups.push({
                    id: g.id,
                    name: g.name,
                    children: g.children,
                    isActive: ko.observable(false)
                });
            }
        };

        self.toggleActiveGroup = function (group)
        {
            var groups = self.groups();
            for (var i = 0; i < groups.length; ++i) {
                var g = groups[i];
                if (g.id !== group.id) {
                    g.isActive(false);
                }
            }
            group.isActive(!group.isActive());
        };

        self.addNewGroup = function()
        {
            Y.io(self.params.urls.usergroup.add, {
                method: "POST",
                headers: { "Accept": "application/hal+json" },
                data: Y.JSON.stringify({
                    name: self.newGroupName(),
                    parent: {
                        id: self.newGroupParent() ? self.newGroupParent().id : 'root'
                    }
                }),
                on: {
                    success: function()
                    {
                        self.fetchGroups();
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.deleteGroup = function(group)
        {
            var url = self.params.urls.usergroup.remove;
            url = url.replace("__groupid__", group.id);

            Y.io(url, {
                method: "DELETE",
                headers: { "Accept": "application/hal+json" },
                on: {
                    success: function()
                    {
                        self.fetchGroups();
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.fetchMembersAndManagers = function(group)
        {
            self.currentGroup = group;
            self.fetchMembers(group);
            self.fetchManagers(group);
        };

        self.fetchMembers = function (group)
        {
            self.members.removeAll();

            Y.io(group._links.member.href, {
                headers: {"Accept": "application/hal+json"},

                on: {
                    success: function (transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        console.log(data);

                        var members = data._embedded.user;
                        for (var i = 0; i < members.length; ++i) {
                            members[i].isSelected = ko.observable(false);
                            self.members.push(members[i]);
                        }
                    },

                    start: function(transactionId)
                    {
                        M.local_tsg.defaults.io.on.start(transactionId);
                        self.isFetchingMembers(true);
                    },

                    complete: function(transactionId, response)
                    {
                        M.local_tsg.defaults.io.on.complete(transactionId, response);
                        self.isFetchingMembers(false);
                    },

                    failure: M.local_tsg.defaults.io.on.failure
                }
            });

            /*
             var data = M.local_tsg.usergroupmanagement.sampleGroupMembers;
             var members = group.id in data ? data[group.id] : [];
             for (var i = 0; i < members.length; ++i) {
             self.members.push(members[i]);
             }
             */
        };

        self.fetchManagers = function(group)
        {
            self.managers.removeAll();

            Y.io(group._links.manager.href, {
                headers: {"Accept": "application/hal+json"},

                on: {
                    success: function (transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        console.log(data);

                        var managers = data._embedded.user;
                        for (var i = 0; i < managers.length; ++i) {
                            managers[i].isSelected = ko.observable(false);
                            self.managers.push(managers[i]);
                        }
                    },

                    start: function(transactionId)
                    {
                        M.local_tsg.defaults.io.on.start(transactionId);
                        self.isFetchingManagers(true);
                    },

                    complete: function(transactionId, response)
                    {
                        M.local_tsg.defaults.io.on.complete(transactionId, response);
                        self.isFetchingManagers(false);
                    },

                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.toggleUser = function (user)
        {
            user.isSelected(!user.isSelected());
        };

        self.addMember = function()
        {
            var url = self.params.urls.usergroup.members.add;
            url = url.replace("__groupid__", self.currentGroup.id);
            Y.io(url, {
                method: "POST",
                headers: { "Accept": "application/hal+json" },
                data: Y.JSON.stringify(self.selectedUser),
                on: {
                    success: function(transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        console.log("response: ");
                        console.log(data);
                        self.fetchMembers(self.currentGroup);
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.removeMembers = function ()
        {
            var selected = [];
            var members = self.members();
            for (var i = 0; i < members.length; i++) {
                if (members[i].isSelected()) {
                    selected.push(members[i]);
                }
            }

            var url = self.params.urls.usergroup.members.remove;
            url = url.replace("__groupid__", self.currentGroup.id);
            Y.io(url, {
                method: "PUT",
                headers: { "Accept": "application/hal+json" },
                data: Y.JSON.stringify(selected),
                on: {
                    success: function(transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        console.log("response: ");
                        console.log(data);
                        self.fetchMembers(self.currentGroup);
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.addManager = function()
        {
            var url = self.params.urls.usergroup.managers.add;
            url = url.replace("__groupid__", self.currentGroup.id);
            Y.io(url, {
                method: "POST",
                headers: { "Accept": "application/hal+json" },
                data: Y.JSON.stringify(self.selectedUser),
                on: {
                    success: function(transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        console.log("response: ");
                        console.log(data);
                        self.fetchManagers(self.currentGroup);
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };

        self.removeManagers = function ()
        {
            var selected = [];
            var managers = self.managers();
            for (var i = 0; i < managers.length; i++) {
                if (managers[i].isSelected()) {
                    selected.push(managers[i]);
                }
            }

            var url = self.params.urls.usergroup.managers.remove;
            url = url.replace("__groupid__", self.currentGroup.id);
            Y.io(url, {
                method: "PUT",
                headers: { "Accept": "application/hal+json" },
                data: Y.JSON.stringify(selected),
                on: {
                    success: function(transactionId, response)
                    {
                        console.log("io:success transactionId: " + transactionId);
                        var data = Y.JSON.parse(response.responseText);
                        console.log("response: ");
                        console.log(data);
                        self.fetchManagers(self.currentGroup);
                    },

                    start: M.local_tsg.defaults.io.on.start,
                    complete: M.local_tsg.defaults.io.on.complete,
                    failure: M.local_tsg.defaults.io.on.failure
                }
            });
        };
    },

    setupViewModel: function(params)
    {
        var viewModel = new M.local_tsg.usergroupmanagement.ViewModel(params);
        M.local_tsg.usergroupmanagement.viewModel = viewModel;

        // viewModel.displayGroups(M.local_tsg.usergroupmanagement.sampleGroups);
        viewModel.fetchGroups();
    },

    sampleGroups:
    [
        {
            id: 1,
            name: 'Top A',
            children:
                [
                    { id: "1-1", name: 'Top 1 Sub 1' },
                    { id: "1-2", name: 'Top 1 Sub 2' }
                ]
        },

        {
            id: 2,
            name: 'Top B',
            children:
                [
                    { id: "2-1", name: 'Top 2 Sub 1' },
                    { id: "2-2", name: 'Top 2 Sub 2' },
                    { id: "2-3", name: 'Top 3 Sub 3' }
                ]
        },

        {
            id: 3,
            name: 'Top C',
            children:
                [
                    { id: "3-1", name: 'Top 3 Sub 1' },
                    { id: "3-2", name: 'Top 3 Sub 2' }
                ]
        }
    ],

    sampleGroupMembers:
    {
        "1-1":
        [
            { id: 2, last_name: "Allen", first_name: "Sheila" },
            { id: 1, last_name: "Jones", first_name: "Steve" }
        ],

        "1-2":
        [
            { id: 3, last_name: "Duke", first_name: "Keith" }
        ]
    }
};
