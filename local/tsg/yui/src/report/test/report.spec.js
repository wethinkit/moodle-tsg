describe("Reports", function() {
    var module;

    beforeEach(function() {
        module = M.local_tsg.report;
        module.setupViewModel({
            report: {
                run: "/run-report"
            }
        });

        Y.io.responseText(window.JSON.stringify(buildTestHalResponse()));
    });

    it("should call setupViewModel when init is called", function() {
        spyOn(module, "setupViewModel");
        spyOn(module, "setupAutoCompletes"); // only here to prevent a real call to setupAutoCompletes
        spyOn(M.local_tsg.ko, "applyBindings"); // only here to prevent a real call to applyBindings
        module.init({});
        expect(module.setupViewModel).toHaveBeenCalledWith(jasmine.any(Object));
    });

    it("should calculate startDate from year, month, and day fields", function() {
        module.viewModel.startYear('2014');
        module.viewModel.startMonth('1');
        module.viewModel.startDay('1');
        expect(module.viewModel.startDate()).toBe(1388556000*1000);
    });

    it("should calculate endDate from year, month, and day fields", function() {
        module.viewModel.endYear('2014');
        module.viewModel.endMonth('1');
        module.viewModel.endDay('1');
        expect(module.viewModel.endDate()).toBe(1388556000*1000);
    });

    it("should use the passed in url to run reports", function() {
        spyOn(Y, "io");
        module.viewModel.run();
        expect(Y.io).toHaveBeenCalledWith("/run-report", jasmine.any(Object));
    });

    it("should post data when running reports", function() {
        spyOn(Y, "io");
        module.viewModel.run();
        expect(Y.io.calls[0].args[1].method).toBe("POST");
    })

    it("should fetch records when run", function() {
        module.viewModel.run();
        expect(module.viewModel.records().length).toBe(3);
        expect(module.viewModel.records()[0].Course.fullname).toBe("test 0");
    });

    it("should send criteria in request body", function() {
        spyOn(Y, "io");

        module.viewModel.startYear('2014');
        module.viewModel.startMonth('1');
        module.viewModel.startDay('1');

        module.viewModel.endYear('2014');
        module.viewModel.endMonth('1');
        module.viewModel.endDay('1');

        module.viewModel.run();

        expect(Y.io.calls[0].args[1].data).toEqual({
            startDate: 1388556000*1000,
            endDate: 1388556000*1000
        });
        // expect(Y.io.calls[0].args[1].data.startDate).toBe(1388556000*1000);
    });
});

function buildTestHalResponse()
{
    var testRecords = [];
    for (var i=0; i < 3; i++) {
        testRecords.push(buildTestRecord(i));
    }

    return {
        _embedded: {
            record: testRecords
        }
    }
}

function buildTestRecord(num)
{
    return {
        "Course": {
            "fullname": "test " + num
        },

        CourseCategory: {
            pathName: "category " + num
        },

        User: {
            lastname: "last" + num,
            firstname: "first" + num
        },

        objectType: "completion"
    };
}