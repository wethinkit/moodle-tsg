M.local_tsg = M.local_tsg || {};


M.local_tsg.report = {
    init: function(params)
    {
        M.local_tsg.report.setupViewModel(params);
        M.local_tsg.report.setupAutoCompletes(params.urls);
        M.local_tsg.ko.applyBindings(M.local_tsg.report.viewModel);
    },

    ViewModel: function(params) {
        var self = this;
        self.urls = params.urls;
        var ko = M.local_tsg.ko;

        /*
         * Utility functions
         */

        self.toDate = function(year, month, day)
        {
            if (year && month && day) {
                var d = new Date(year, month - 1, day);
                return d.valueOf();
            }
            return false;
        };

        /*
         * Criteria fields
         */

        self.courseId = ko.observable(false);
        self.tagId = ko.observable(false);
        self.userId = ko.observable(false);
        self.userGroups = ko.observableArray(params.userGroups);
        self.selectedUserGroup = ko.observable();
        self.includeUserSubGroups = ko.observable(false);

        self.startDay = ko.observable();
        self.startMonth = ko.observable();
        self.startYear = ko.observable();
        self.startDate = ko.computed(function() {
            return self.toDate(self.startYear(), self.startMonth(), self.startDay());
        });

        self.endDay = ko.observable();
        self.endMonth = ko.observable();
        self.endYear = ko.observable();
        self.endDate = ko.computed(function() {
            return self.toDate(self.endYear(), self.endMonth(), self.endDay());
        });

        self.includeTerminated = ko.observable(false);
        self.includeInactives = ko.observable(false);
        self.hiredWithin = ko.observable(false);
        self.hiredWithinDays = ko.observable();


        /*
         * Record type and columns fields
         */
        self.optionalColumns = params.profileGroups;
        self.selectedOptionalColumns = ko.observableArray();
        self.reportType = ko.observable('completions');


        /*
         * Miscellaneous properties
         */

        self.title = "Reports";
        self.records = ko.observableArray();
        self.columnCount = ko.computed(function() {
            return 15 + self.selectedOptionalColumns().length;
        });
        self.links = {};
        self.isLoading = ko.observable(false);
        self.isEmptyResults = ko.observable(false);
        self.errorMessage = ko.observable("");


        /*
         * report running functions
         */

        self.run = function()
        {
            self.fetch(params.urls.report.run, self.runEventHandlers);
        };

        self.download = function()
        {
            document.getElementById('criteria-form').submit();
        };

        self.fetch = function(url, eventHandlers)
        {
            self.records.removeAll();

            console.log("calling " + params.urls.report.run);
            Y.io(url, {
                headers: { "Accept": "application/hal+json" },
                method: "POST",
                data: {
                    courseId: self.courseId(),
                    tagId: self.tagId(),
                    userId: self.userId(),
                    userGroupId: self.selectedUserGroup() ? self.selectedUserGroup().id : 0,
                    includeUserSubGroups: self.includeUserSubGroups(),
                    startDate: self.startDate(),
                    endDate: self.endDate(),
                    includeTerminated: self.includeTerminated(),
                    includeInactives: self.includeInactives(),
                    hiredWithin: self.hiredWithin(),
                    hiredWithinDays: self.hiredWithinDays(),
                    reportType: self.reportType()
                },
                on: eventHandlers
            });
        };

        self.previous = function()
        {
            if (self.links.prev) {
                self.fetch(self.links.prev.href, self.runEventHandlers);
            }
        };

        self.next = function()
        {
            if (self.links.next.href) {
                self.fetch(self.links.next.href, self.runEventHandlers);
            }
        };

        self.runEventHandlers = {
            start: function(transactionId) {
                console.log("io:start run transactionId: " + transactionId);
                self.records.removeAll();
                self.isLoading(true);
                self.isEmptyResults(false);
                self.errorMessage("");
            },

            complete: function(transactionId, response) {
                console.log("io:complete transactionId: " + transactionId +
                    ", status: " + response.statusText);
                self.isLoading(false);
            },

            success: function(transactionId, response) {
                console.log("io:success transactionId: " + transactionId);
                var data = Y.JSON.parse(response.responseText);

                self.links = data._links;
                var records = data._embedded.record;

                if (!records) {
                    self.isEmptyResults(true);
                    return;
                }

                for (var i in records) {
                    if (!records.hasOwnProperty(i)) {
                        continue;
                    }
                    self.records.push(records[i]);
                }
            },

            failure: function(transactionId, response) {
                console.log("io:failure transactionId: " + transactionId);
                self.errorMessage(response.statusText);
            }
        };
    },

    setupViewModel: function(params)
    {
        M.local_tsg.report.viewModel = new M.local_tsg.report.ViewModel(params);
    },

    setupAutoCompletes: function(urls)
    {
        var courseAC = new Y.AutoComplete({
            inputNode: '#course',
            source: urls.course.findByPattern + '?pattern={query}',
            queryDelay: 300,
            resultListLocator: '_embedded.course',
            resultTextLocator: 'full_name',
            render: true,
            width: 'auto'
        });
        courseAC.on(['clear', 'results', 'query'], function() {
            M.local_tsg.report.viewModel.courseId(false);
        });
        courseAC.on('select', function(e) {
            M.local_tsg.report.viewModel.courseId(e.result.raw.id);
        });

        var tagAC = new Y.AutoComplete({
            inputNode: '#tag',
            source: urls.tag.findByPattern + '?pattern={query}',
            queryDelay: 300,
            resultListLocator: '_embedded.tag',
            resultTextLocator:  'name',
            render: true,
            width: 'auto'
        });
        tagAC.on(['clear','results','query'], function() {
            M.local_tsg.report.viewModel.tagId(false);
        });
        tagAC.on('select', function(e) {
            M.local_tsg.report.viewModel.tagId(e.result.raw.id);
        });

        var userAC = new Y.AutoComplete({
            inputNode: '#user',
            source: urls.user.findByPattern + '?pattern={query}',
            queryDelay: 300,
            resultListLocator: '_embedded.user',
            resultTextLocator:  function(user) { return user.last_name + ', ' + user.first_name
                + ' (' + user.user_name + ')'; },
            render: true,
            width: 'auto'
        });
        userAC.on(['clear','results','query'], function() {
            M.local_tsg.report.viewModel.userId(false);
        });
        userAC.on('select', function(e) {
            M.local_tsg.report.viewModel.userId(e.result.raw.id);
        });
    }
};
