M.local_tsg = M.local_tsg || {};

M.local_tsg.api = function ()
{
    var self = this;

    self.parseResponse = function (response, callback, errback)
    {
        var r = {};
        try {
            r = Y.JSON.parse(response);
        }
        catch (ex) {
            errback(self.problems.badresponse);
            return false;
        }

        callback(r);
        return true;
    };

    self.parseFailure = function (response)
    {
        var problem = false;
        try {
            problem = Y.JSON.parse(response);
        }
        catch (ex) {
            problem = self.problems.badresponse;
        }

        return problem;
    };

    self.problemTypeFromURI = function (uri)
    {
        for (var key in self.problems) {
            if (!self.problems.hasOwnProperty(key)) {
                continue;
            }
            if (M.local_tsg.defaults.contextRoot + self.problems[key].type === uri) {
                return key;
            }
        }

        return false;
    };

    self.problems =
    {
        notloggedin:
        {
            type   : '/api/problem/notloggedin',
            titile : 'Not logged in',
            detail : ''
        },

        badresponse:
        {
            type   : '/api/problem/badresponse',
            title  : 'Unexpected response from server',
            detail : ''
        }
    };
};

