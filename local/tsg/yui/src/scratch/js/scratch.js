M.local_tsg = M.local_tsg || {};


M.local_tsg.scratch = {
    init: function(urls)
    {
        M.local_tsg.setupViewModel(urls);
        M.local_tsg.ko.applyBindings(self.viewModel);
    },

    setupViewModel: function(urls)
    {
        var ko = M.local_tsg.ko;
        var self = M.local_tsg.scratch;

        self.viewModel = new self.ViewModel();

        Y.io(urls.user.list, {
            headers: { "Accept": "application/hal+json" },
            on: {
                success: function(transactionId, response) {
                    var r = Y.JSON.parse(response.responseText);

                    for (var i=0; i < r._embedded.user.length; i++) {
                        self.viewModel.users.push(r._embedded.user[i]);
                    }
                }
            }
        });
    },

    ViewModel: function() {
        var self = this;
        var ko = M.local_tsg.ko;

        self.title = "DBAL 2";
        self.users = ko.observableArray();
    }
};
