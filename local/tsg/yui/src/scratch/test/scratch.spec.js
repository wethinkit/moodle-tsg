

describe("A suite", function() {

    beforeEach(function() {
        Y.io.responseText ('{ \
            "_embedded": { \
                "user": [ \
                    {}, \
                    {} \
                ] \
            } \
        }');
    });

    afterEach(function() {

    });

    it("test X", function() {
        M.local_tsg.scratch.setupViewModel({
            "user": { "list": "list-url" }
        });

        expect(M.local_tsg.scratch.viewModel.users().length).toBe(2);
    });
});