M.local_tsg = M.local_tsg || {};

M.local_tsg.sessions =
{
    init: function (params)
    {
        var viewModel = M.local_tsg.sessions.setupViewModel(params);
        M.local_tsg.ko.applyBindings(viewModel);
    },

    ViewModel: function (params) {
        var self = this;
        self.params = params;
        self.courseId = params.courseId;
        var ko = M.local_tsg.ko;
        var moment = M.local_tsg.moment;


        /*
         * Sessions
         */

        self.sessions = ko.observableArray([]);
        /*
         self.sessions = ko.observableArray([
         {
         start_date: '1/1/2015',
         end_date: '1/1/2015',
         schedule: '8:00 am',
         location: 'office',
         capacity: '15',
         instructor: 'Sheila Allen'
         },
         {
         start_date: '1/1/2015',
         end_date: '1/1/2015',
         schedule: '8:00 am',
         location: 'office',
         capacity: '15',
         instructor: 'Sheila Allen'
         },
         {
         start_date: '1/1/2015',
         end_date: '1/1/2015',
         schedule: '8:00 am',
         location: 'office',
         capacity: '15',
         instructor: 'Sheila Allen'
         }
         ]);
         */


        /*
         * Create/Edit Session Dialog
         */

        self.editSessionForm =
        {
            mode        : 'insert',

            start       :
            {
                month  : ko.observable(''),
                day    : ko.observable(''),
                year   : ko.observable('')
            },

            end         :
            {
                month  : ko.observable(''),
                day    : ko.observable(''),
                year   : ko.observable('')
            },

            schedule    : ko.observable(),
            location    : ko.observable(),
            capacity    : ko.observable(),
            instructor  : ko.observable(),

            session     : {},

            showProgressBar: ko.observable(false),

            set: function (session) {
                var form = self.editSessionForm;

                var start = moment.utc(session.start_date * 1000);
                form.start.month(self.choices.months[start.month()]);
                form.start.day(start.date());
                form.start.year(start.year());

                var end = moment.utc(session.end_date * 1000);
                form.end.month(self.choices.months[end.month()]);
                form.end.day(end.date());
                form.end.year(end.year());

                form.schedule(session.schedule);
                form.location(session.location);
                form.capacity(session.capacity);
                form.instructor(session.instructor);
            },

            clear: function () {
                var s = self.editSessionForm;

                s.start.month('');
                s.start.year('');
                s.start.day('');

                s.end.month('');
                s.end.year('');
                s.end.day('');

                s.schedule('');
                s.location('');
                s.capacity('');
                s.instructor('');
            }
        };


        /*
         * Roster Dialog
         */

        self.rosterDialog =
        {
            _links   : {},
            entries  : ko.observableArray([]),

            showProgressBar: ko.observable(false),

            open: function(session)
            {
                self.rosterDialog.showProgressBar(true);
                $('#tsg-roster-dialog').modal('show');
                self.fetchRoster(session);
            },

            close: function()
            {
                $('#tsg-roster-dialog').modal('hide');
            },

            save: function()
            {
                self.updateRoster();
            }
        };


        /*
         * Messages
         */

        self.confirmDialog =
        {
            prompt           : ko.observable(''),
            body             : ko.observable(''),
            showProgressBar  : ko.observable(false),

            clear: function()
            {
                self.confirmDialog.prompt('');
                self.confirmDialog.body('');
                self.confirmDialog.showProgressBar(false);
            }
        };

        self.validationErrors = ko.observableArray(false);
        self.serverError = ko.observable(false);

        self.clearValidationErrors = function () {
            self.validationErrors.removeAll();
        };

        self.clearServerError = function () {
            self.serverError(false);
        };

        self.ioFailure = function (transactionId, response) {
            try {
                var r = Y.JSON.parse(response.response);
                self.serverError(r);
            }
            catch (ex) {
                self.serverError('Could not understand response from server');
            }
        };


        /*
         * Actions
         */

        self.newSession = function () {
            self.editSessionForm.clear();
            self.editSessionForm.mode = 'insert';
            $('#tsg-edit-session-dialog').modal();
        };

        self.editSession = function (session) {
            self.editSessionForm.set(session);
            self.editSessionForm.mode = 'update';
            self.editSessionForm.url = session._links.self.href;
            $('#tsg-edit-session-dialog').modal();
        };

        self.saveSession = function ()
        {
            self.editSessionForm.showProgressBar(true);
            return self.editSessionForm.mode === 'insert' ?
                self.insertSession() : self.updateSession();
        };

        self.confirmDeleteSession = function (session)
        {
            self.confirmDialog.clear();
            self.confirmDialog.prompt('Are you sure you want to delete this session?');

            var t = '<table class="table" style="font-size: smaller">';
            t    += '  <thead><tr>';
            t    += '    <th>Start</th>';
            t    += '    <th>End</th>';
            t    += '    <th>Schedule</th>';
            t    += '    <th>Location</th>';
            t    += '    <th>Capacity</th>';
            t    += '    <th>Instructor</th>';
            t    += '  </tr></thead>';
            t    += '  <tbody><tr>';
            t    += '    <td class="tsg-date">' + M.local_tsg.ko.fromUnixTime(session.start_date) + '</td>';
            t    += '    <td class="tsg-date">' + M.local_tsg.ko.fromUnixTime(session.end_date) + '</td>';
            t    += '    <td>' + session.schedule + '</td>';
            t    += '    <td>' + session.location + '</td>';
            t    += '    <td>' + session.capacity + '</td>';
            t    += '    <td>' + session.instructor + '</td>';
            t    += '  </tr></tbody>';
            t    += '</table>';

            self.confirmDialog.body(t);

            $('#tsg-confirm-yes').one('click', null, session, function(e) {
                self.confirmDialog.showProgressBar(true);
                self.deleteSession(e.data);
                return false;
            });

            $('#tsg-confirm-no').one('click', function() {
                self.confirmDialog.clear();
                $('#tsg-confirm-dialog').modal('hide');
                return false;
            });

            $('#tsg-confirm-dialog').modal('show');
        };


        /*
         * API Communication
         */

        self.insertSession = function ()
        {
            Y.io(self.params.urls.session.insert, {
                method: "POST",
                headers: {"Accept": "application/hal+json"},
                data: Y.JSON.stringify(self.serializeSessionForm(self.editSessionForm)),
                on: {
                    success: function (transactionId, response) {
                        self.editSessionForm.showProgressBar(false);
                        $('#tsg-edit-session-dialog').modal('hide');

                        try {
                            Y.JSON.parse(response.response);
                        }
                        catch (ex) {
                            self.serverError('Could not understand response from server');
                            return;
                        }

                        self.fetchSessions();
                    },

                    failure   : self.ioFailure,
                    start     : M.local_tsg.defaults.io.on.start,
                    complete  : M.local_tsg.defaults.io.on.complete,
                    end       : M.local_tsg.defaults.io.on.end
                }
            });
        };

        self.updateSession = function ()
        {
            var form = self.editSessionForm;

            Y.io(form.url, {
                method   : "PUT",
                headers  : {"Accept": "application/hal+json"},
                data     : Y.JSON.stringify(self.serializeSessionForm(form)),
                on       : {
                    success: function (transactionId, response) {
                        self.editSessionForm.showProgressBar(false);
                        $('#tsg-edit-session-dialog').modal('hide');

                        var updated;
                        try {
                            updated = Y.JSON.parse(response.response);
                        }
                        catch (ex) {
                            self.serverError('Could not understand response from server');
                            return;
                        }

                        var currentIndex = -1;
                        for (var i = 0; i < self.sessions().length; ++i) {
                            if (self.sessions()[i].id === updated.id) {
                                currentIndex = i;
                                break;
                            }
                        }

                        if (currentIndex > -1) {
                            self.sessions.splice(currentIndex, 1, updated);
                        }
                    },

                    failure   : self.ioFailure,
                    start     : M.local_tsg.defaults.io.on.start,
                    complete  : M.local_tsg.defaults.io.on.complete,
                    end       : M.local_tsg.defaults.io.on.end
                }
            });
        };

        self.deleteSession = function (session) {
            Y.io(session._links.self.href, {
                method   : 'DELETE',
                headers  : { 'Accept': 'application/hal+json' },
                on       : {
                    success: function (transactionId, response) {
                        $('#tsg-confirm-dialog').modal('hide');
                        self.confirmDialog.clear();

                        var deleted;
                        try {
                            deleted = Y.JSON.parse(response.response);
                        }
                        catch (ex) {
                            self.serverError('Could not understand response from server');
                            return;
                        }

                        self.sessions.remove(function(s) {
                            return s.id === parseInt(deleted.id, 10);
                        });
                    }
                }
            });
        };

        self.fetchSessions = function () {
            self.sessions([]);

            Y.io(self.params.urls.course.sessions, {
                method   : "GET",
                headers  : { 'Accept': 'application/hal+json' },
                on       : {
                    success: function (transactionId, response) {

                        try {
                            var r = Y.JSON.parse(response.response);
                            self.sessions(r._embedded.courseSession);
                        }
                        catch (ex) {
                            self.serverError('Could not understand response from server');
                        }
                    },

                    start     : M.local_tsg.defaults.io.on.start,
                    complete  : M.local_tsg.defaults.io.on.complete,
                    failure   : self.ioFailure,
                    end       : M.local_tsg.defaults.io.on.end
                }
            });
        };

        self.fetchRoster = function (session)
        {
            self.rosterDialog.entries([]);

            Y.io(session._links.roster.href, {
                method   : 'GET',
                headers  : { 'Accept': 'application/hal+json' },

                on       : {
                    success: function (transactionId, response) {
                        var parsed;
                        try {
                            parsed = Y.JSON.parse(response.response);
                        }
                        catch (ex) {
                            self.serverError('Could not understand response from server');
                            return;
                        }

                        self.rosterDialog._links = parsed._links;
                        self.rosterDialog.entries(parsed._embedded.sessionRoster);
                    },
                    end: function() { self.rosterDialog.showProgressBar(false); }
                }
            });
        };

        self.updateRoster = function ()
        {
            Y.io(self.rosterDialog._links.update.href, {

                method   : 'PUT',
                headers  : { 'Accept': 'application/hal+json' },
                data     : Y.JSON.stringify(self.rosterDialog.entries()),

                on       : {

                    success: function (transactionId, response) {

                        var parsed;
                        try {
                            parsed = Y.JSON.parse(response.response);
                        }
                        catch (ex) {
                            self.serverError('Could not understand response from server');
                            return;
                        }

                        self.rosterDialog.close();
                    }
                }
            });
        };


        /*
         * Utilities
         */

        self.serializeSessionForm = function (form) {
            return {
                courseId    : self.courseId,
                startDate   : M.local_tsg.moment.utc(
                    {
                        year   : form.start.year(),
                        month  : form.start.month().num,
                        day    : form.start.day()
                    }
                ).format("YYYY-MM-DDTHH:mm:ssZ"),

                endDate     : M.local_tsg.moment.utc(
                    {
                        year   : form.end.year(),
                        month  : form.end.month().num,
                        day    : form.end.day()
                    }
                ).format("YYYY-MM-DDTHH:mm:ssZ"),

                schedule    : form.schedule(),
                location    : form.location(),
                capacity    : form.capacity(),
                instructor  : form.instructor()
            };
        };


        /*
         * Choices
         */

        self.choices =
        {
            months: [
                {num: 0, name: "Jan"},
                {num: 1, name: "Feb"},
                {num: 2, name: "Mar"},
                {num: 3, name: "Apr"},
                {num: 4, name: "May"},
                {num: 5, name: "Jun"},
                {num: 6, name: "Jul"},
                {num: 7, name: "Aug"},
                {num: 8, name: "Sep"},
                {num: 9, name: "Oct"},
                {num: 10, name: "Nov"},
                {num: 11, name: "Dec"}
            ],

            days: function (month, year) {
                if (!month) {
                    return [];
                }

                switch (month.num) {
                    case 0:
                    case 2:
                    case 4:
                    case 6:
                    case 7:
                    case 9:
                    case 11:
                        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
                    case 1:
                        return M.local_tsg.moment([year]).isLeapYear() ?
                            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29] :
                            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28];
                    case 3:
                    case 5:
                    case 8:
                    case 10:
                        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
                }
            },

            years: ko.computed(function () {
                var year = new Date().getFullYear();
                return [year - 3, year - 2, year - 1, year,
                    year + 1, year + 2, year + 3, year + 4, year + 5, year + 6
                ];
            })
        };

        self.choices.edit =
        {
            start:
            {
                days: ko.computed(function () {
                    return self.choices.days(self.editSessionForm.start.month(),
                        self.editSessionForm.start.year());
                })
            },

            end:
            {
                days: ko.computed(function () {
                    return self.choices.days(self.editSessionForm.end.month(),
                        self.editSessionForm.end.year());
                })
            }
        };
    },

    setupViewModel: function (params)
    {
        var viewModel = new M.local_tsg.sessions.ViewModel(params);
        viewModel.fetchSessions();
        M.local_tsg.sessions.viewModel = viewModel;
        return viewModel;
    }
};
