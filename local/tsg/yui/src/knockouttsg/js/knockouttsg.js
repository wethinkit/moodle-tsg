M.local_tsg = M.local_tsg || {};

M.local_tsg.ko.fromUnixTime = function(timestamp, format)
{
    var DEFAULT_FORMAT = "MM-DD-YYYY";

    format = format ? format : DEFAULT_FORMAT;
    return timestamp ? M.local_tsg.moment.utc(timestamp).format(format) : "";
};

M.local_tsg.ko.bindingHandlers.fromUnixTime = {

    update: function(element, valueAccessor, allBindings) {
        var timestamp = valueAccessor();
        var format = allBindings.get("format");

        element.innerHTML = M.local_tsg.ko.fromUnixTime(timestamp * 1000, format);
    }
};