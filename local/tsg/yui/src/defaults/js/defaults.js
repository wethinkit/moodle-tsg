M.local_tsg = M.local_tsg || {};

M.local_tsg.defaults =
{
    contextRoot: '/local/tsg/s/web/app_dev.php',

    io: {
        on: {
            start: function (transactionId) {
                console.log("io:start run transactionId: " + transactionId);
            },

            complete: function (transactionId, response) {
                console.log("io:complete transactionId: " + transactionId +
                ", status: " + response.statusText);
            },

            failure: function (transactionId /*, response */) {
                console.log("io:failure transactionId: " + transactionId);
            }
        }
    }
};
