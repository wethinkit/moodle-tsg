M.local_tsg = M.local_tsg || {};


M.local_tsg.widgets = {

    init: function()
    {
        window.console = window.console || function () {};
    },


    tree: function(top, fetchFn)
    {
        function toggleElement(element)
        {
            var display = element.style.display;
            if (display === '' || display === 'block') {
                element.style.display = 'none';
            }
            else {
                element.style.display = 'block';
            }
        }

        function toggleBranch(branch, icon)
        {
            var subLists = branch.querySelectorAll('ul');

            Array.prototype.forEach.call(subLists, function (list) {

                var items = list.querySelectorAll('li');
                Array.prototype.forEach.call(items, toggleElement);
            });

            icon.classList.toggle(openedClass);
            icon.classList.toggle(closedClass);
        }

        function DomData()
        {
            var self = this;

            self.buckets = [];
            self.nextBucketIndex = 0;

            self.getForElement = function (element, key)
            {
                var bucketIndex = element.getAttribute('data-tsg-bucket-index');
                if (!bucketIndex) {
                    return false;
                }

                var bucket = self.buckets[bucketIndex];
                return bucket[key];
            };

            self.setForElement = function (element, key, value)
            {
                var bucketIndex = element.getAttribute('data-tsg-bucket-index');
                if (!bucketIndex) {
                    bucketIndex = self.nextBucketIndex++;
                    element.setAttribute('data-tsg-bucket-index', bucketIndex);
                    self.buckets[bucketIndex] = {};
                }

                var bucket = self.buckets[bucketIndex];
                bucket[key] = value;
            };
        }

        function fetchDev(id, callback)
        {
            console.log('fetching categories for category.id = ' + id);
            setTimeout(function () {

                var data = false;
                switch (id) {
                    case 0:
                        data = {
                            _embedded: {
                                categories: [
                                    { id: 1, name: 'Top A' },
                                    { id: 2, name: 'Top B' }
                                ]
                            }
                        };
                        break;

                    case 1:
                        data = {
                            _embedded: {
                                categories: [
                                    { id: 3, name: 'A sub A' },
                                    { id: 4, name: 'A sub B' }
                                ]
                            }
                        };
                        break;

                    case 2:
                        data = {
                            _embedded: {
                                categories: [
                                    { id: 5, name: 'B sub A' }
                                ]
                            }
                        };
                        break;
                }

                callback(data);

            }, 1000);
        }


        /*
         * "member" variables for buildNodeForCategory, captured via closure
         */

        var domData = new DomData();
        var openedClass    = 'icon-chevron-down';
        var closedClass    = 'icon-chevron-right';
        var hourglassClass = 'icon-time';

        function buildBranchNode(branchData) {

            var item = document.createElement('li');
            domData.setForElement(item, 'branch-data', branchData);

            item.classList.add('tsg-branch');

            var icon = document.createElement('i');
            icon.classList.add('tsg-indicator');
            // icon.classList.add('glyphicon');
            icon.classList.add(closedClass);
            item.appendChild(icon);

            var text = document.createTextNode(branchData.name);
            item.appendChild(text);

            icon.addEventListener('click', function () {

                var childrenFetched = domData.getForElement(item, 'children-fetched');
                if (!childrenFetched) {
                    var branchData = domData.getForElement(this.parentElement, 'branch-data');
                    console.log('fetching sub-branches of ' + branchData.name);

                    icon.classList.remove(openedClass);
                    icon.classList.remove(closedClass);
                    icon.classList.add(hourglassClass);

                    fetchFn(branchData.id, function (subBranches) {
                        console.log('received:');
                        console.log(subBranches);

                        var children = document.createElement('ul');

                        // build sub branch nodes
                        subBranches.forEach(function (sb) {
                            children.appendChild(buildBranchNode(sb));
                        });

                        // build leaf nodes
                        branchData.leaves.forEach(function (leafData) {
                            var leaf = document.createElement('li');
                            domData.setForElement(leaf, 'leaf-data', leafData);
                            leaf.classList.add('tsg-branch');

                            if ('click' in leafData) {

                                // if a click handler was passed in, leaf is an anchor
                                var a = document.createElement('a');
                                a.href="";
                                a.addEventListener('click', function (e) {
                                    e.preventDefault();
                                    leafData.click(leafData);
                                });
                                a.appendChild(document.createTextNode(leafData.name));
                                leaf.appendChild(a);
                            }
                            else {

                                // otherwise, leaf is just a text node
                                leaf.appendChild(document.createTextNode(leafData.name));
                            }

                            children.appendChild(leaf);
                        });
                        
                        item.appendChild(children);
                        domData.setForElement(item, 'children-fetched', true);

                        icon.classList.add(openedClass);
                        icon.classList.remove(hourglassClass);
                    });
                }
                else {
                    toggleBranch(item, icon);
                }
            });

            return item;
        }


        top = document.querySelector(top);
        top.classList.add('tsg-tree');

        fetchFn(0, function (branches) {
            branches.forEach(function (b) {

                var node = buildBranchNode(b);
                top.appendChild(node);
            });
        });
    }
};
