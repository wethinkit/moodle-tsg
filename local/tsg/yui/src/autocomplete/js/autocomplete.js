M.local_tsg = M.local_tsg || {};

M.local_tsg.autocomplete = {

    init: function(params)
    {
        alert('TSG Autocomplete');
    },

    user: function(inputNodeSelector, url, selectCallback, clearCallback)
    {
        var userAC = new Y.AutoComplete({
            inputNode: inputNodeSelector,
            source: url + '?pattern={query}',
            queryDelay: 300,
            resultListLocator: '_embedded.user',
            resultTextLocator:  function(user) { return user.last_name + ', ' + user.first_name
                + ' (' + user.user_name + ')'; },
            render: true,
            align: {
                node: inputNodeSelector,
                points: [Y.WidgetPositionAlign.BL, Y.WidgetPositionAlign.TL]
            }
        });
        userAC.on(['clear','results','query'], clearCallback);
        userAC.on('select', function(e) {
            selectCallback(e.result.raw);
        });
    }
};