<?php

require_once(__DIR__ . '/config.php');
require_once(__DIR__ . '/locallib.php');

function local_tsg_extends_navigation(global_navigation $nav)
{
    global $PAGE;

    // Only add to navigation on site home page
    if (!$PAGE->course || $PAGE->course->id != 1) {
        return;
    }

    $nav->add_node(local_tsg_build_navigation_node('users', '/admin/user.php', 't/user'));
    $nav->add_node(local_tsg_build_navigation_node('groups',
        LOCAL_TSG_CONTEXTROOT . '/usergroupmanagement', 't/groupv'));
    $nav->add_node(local_tsg_build_navigation_node('reports',
        LOCAL_TSG_CONTEXTROOT . '/report', 't/grades'));
}

function local_tsg_extends_settings_navigation(settings_navigation $nav, context $context)
{
    global $PAGE;

    // Only add this settings item on non-site course pages.
    if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
    }

    $settingnode = $nav->find('courseadmin', navigation_node::TYPE_COURSE);
    if (!$settingnode) {
        return;
    }


    $label = get_string('assignments', 'local_tsg');
    $url = new moodle_url(LOCAL_TSG_CONTEXTROOT . '/course/' .
        $PAGE->course->id . '/assignmentmanagement');

    $node = navigation_node::create(
        $label,
        $url,
        navigation_node::NODETYPE_LEAF,
        'tsg',
        'tsg',
        new pix_icon('t/assignroles', $label)
    );

    if ($PAGE->url->compare($url, URL_MATCH_BASE)) {
        $node->make_active();
    }
    $settingnode->add_node($node);


    $label = get_string('sessions', 'local_tsg');
    $url =  new moodle_url(LOCAL_TSG_CONTEXTROOT . '/course/' .
        $PAGE->course->id . '/sessions');
    
    $node = navigation_node::create(
        $label,
        $url,
        navigation_node::NODETYPE_LEAF,
        'tsg',
        'tsg',
        new pix_icon('c/event', $label)
    );

    if ($PAGE->url->compare($url, URL_MATCH_BASE)) {
        $node->make_active();
    }
    $settingnode->add_node($node);
}