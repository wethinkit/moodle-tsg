<?php

namespace TSG\MoodleGlobalsBundle\Facade;


class MoodleGlobals {

    private $cfg;
    private $db;
    private $output;
    private $page;
    private $user;

    public function __construct($moodleconfigfile)
    {
        require_once($moodleconfigfile);
        global $CFG, $DB, $OUTPUT, $PAGE, $USER;

        $this->cfg = $CFG;
        $this->db = $DB;
        $this->output = $OUTPUT;
        $this->page = $PAGE;
        $this->user = $USER;
    }


    public function getCfg()
    {
        return $this->cfg;
    }
    public function getDb()
    {
        return $this->db;
    }
    public function getOutput()
    {
        return $this->output;
    }

    public function getPage()
    {
        return $this->page;
    }
    public function getUser()
    {
        return $this->user;
    }
} 