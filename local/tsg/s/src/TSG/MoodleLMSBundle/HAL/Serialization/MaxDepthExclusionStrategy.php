<?php

namespace TSG\MoodleLMSBundle\HAL\Serialization;

use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;


class MaxDepthExclusionStrategy extends BaseExclusionStrategy
{
    private $depth;


    public function __construct($depth, $logger=null)
    {
        parent::__construct($logger);
        $this->depth = $depth;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $navigatorContext)
    {
        // $this->logger->debug('classMetaData.name = ' . $metadata->name);
    }

    /**
     * {@inheritDoc}
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $navigatorContext)
    {
        // $this->logger->debug($property->name . ' (' . $property->class . '): depth = ' . $navigatorContext->getDepth());
        return $navigatorContext->getDepth() > $this->depth;
    }
}