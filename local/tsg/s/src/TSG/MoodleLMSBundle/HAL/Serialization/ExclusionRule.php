<?php
namespace TSG\MoodleLMSBundle\HAL\Serialization;


class ExclusionRule
{
    /**
     * @var string
     */
    private $className;

    /**
     * @var string
     */
    private $propertyName;

    /**
     * @var int
     */
    private $depth;


    public function __construct($className, $propertyName, $depth)
    {
        $this->className    = $className;
        $this->propertyName = $propertyName;
        $this->depth        = $depth;
    }


    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getPropertyName()
    {
        return $this->propertyName;
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }
}