<?php

namespace TSG\MoodleLMSBundle\HAL\Serialization;

use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;
use Psr\Log\LoggerInterface;


class RulesExclusionStrategy implements ExclusionStrategyInterface
{
    /**
     * @var int
     */
    private $maxDepth;

    /**
     * @var ExclusionRule[]
     */
    private $exclusions;

    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(array $exclusions, $maxDepth = 0, $logger = null)
    {
        $this->exclusions = $exclusions;
        $this->maxDepth   = $maxDepth;
        $this->logger     = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $navigatorContext)
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $navigatorContext)
    {
        $className = substr($property->class, strrpos($property->class, '\\') + 1);
        $propertyName = $property->name;
        $depth = $navigatorContext->getDepth();

        /*
        static $depths = 0;
        if ($navigatorContext->getDepth() > $depths) {
            $this->debug('shouldSkipProperty: ' . $className . '.'
                . $property->name . ': ' . $navigatorContext->getDepth());

            // $depths++;
        }
        */

        foreach ($this->exclusions as $e) {
            if ($propertyName === $e->getPropertyName() &&
                $depth        === $e->getDepth()) {

                return true;
            }
        }

        return $this->maxDepth === 0 ? false : $depth > $this->maxDepth;
    }


    private function debug($message)
    {
        if ($this->logger == null) {
            return;
        }
        $this->logger->debug($message);
    }
}