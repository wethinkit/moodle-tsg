<?php
namespace TSG\MoodleLMSBundle\HAL\Serialization;


use JMS\Serializer\Exclusion\ExclusionStrategyInterface;

abstract class BaseExclusionStrategy implements ExclusionStrategyInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct($logger)
    {
        $this->logger = $logger;
    }


    protected function debug($message)
    {
        if ($this->logger == null) {
            return;
        }
        $this->logger->debug($message);
    }
}