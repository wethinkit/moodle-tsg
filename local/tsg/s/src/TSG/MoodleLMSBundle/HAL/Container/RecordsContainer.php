<?php
namespace TSG\MoodleLMSBundle\HAL\Container;


/**
 * Class RecordsContainer a Container whose items are flat records (PHP arrays).
 *
 * @package TSG\MoodleLMSBundle\HAL\Container
 */
class RecordsContainer extends MultiItemContainer
{

} 