<?php

namespace TSG\MoodleLMSBundle\HAL\Container;


use JMS\Serializer\Exclusion\ExclusionStrategyInterface;

class SingleItemContainer
{
    private $item;
    private $serializerGroups;
    private $excluder;

    public function __construct($item, array $serializerGroups, ExclusionStrategyInterface $excluder = null)
    {
        $this->item = $item;
        $this->serializerGroups = $serializerGroups;
        $this->excluder = $excluder;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return array
     */
    public function getSerializerGroups()
    {
        return $this->serializerGroups;
    }

    /**
     * @return ExclusionStrategyInterface
     */
    public function getExcluder()
    {
        return $this->excluder;
    }


}