<?php
namespace TSG\MoodleLMSBundle\HAL\Container;


/**
 * Class EntitiesContainer a Container whose items are Entities forming an Object graph.
 *
 * @package TSG\MoodleLMSBundle\HAL\Container
 */
class EntitiesContainer extends MultiItemContainer
{

} 