<?php
namespace TSG\MoodleLMSBundle\HAL\Container;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;

/**
 * Class Container storage for HAL items and associated info needed to produce a HAL list of the items.
 *
 * @package TSG\MoodleLMSBundle\HAL\Container
 */
class MultiItemContainer
{
    private $items;
    private $serializerGroups;
    private $route;
    private $offset;
    private $limit;
    private $excluder;

    public function __construct($items, array $serializerGroups, $route, $offset = false, $limit = false, ExclusionStrategyInterface $excluder = null)
    {
        $this->items = $items;
        $this->serializerGroups = $serializerGroups;
        $this->route = $route;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->excluder = $excluder;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function getSerializerGroups()
    {
        return $this->serializerGroups;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return \JMS\Serializer\Exclusion\ExclusionStrategyInterface
     */
    public function getExcluder()
    {
        return $this->excluder;
    }


} 