<?php

namespace TSG\MoodleLMSBundle\HAL;

use \stdClass;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface as ViewHandler;

use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;

use TSG\MoodleLMSBundle\Entity;
use TSG\MoodleLMSBundle\HAL\Container\RecordsContainer;
use TSG\MoodleLMSBundle\HAL\Container\SingleItemContainer;
use TSG\MoodleLMSBundle\HAL\Problem\ApiProblem;
use TSG\MoodleLMSBundle\HAL\Resource\CourseAssignmentResource;
use TSG\MoodleLMSBundle\HAL\Resource\CourseSessionResource;
use TSG\MoodleLMSBundle\HAL\Resource\ResourceList;

class HALViewHandler {

    private $serializer;
    private $router;
    private $logger;

    public function __construct(ViewHandler $viewHandler, RouterInterface $router,
                                Serializer $serializer, Logger $logger)
    {
        $viewHandler->registerHandler('hal', array($this, 'handle'));
        // $viewHandler->registerHandler('')

        $this->serializer = $serializer;
        $this->router = $router;
        $this->logger = $logger;
    }

    public function handle(ViewHandler $handler, View $view, Request $request, $format)
    {
        $this->logger->debug(__METHOD__ . ':  $format = ' . $format);

        $container = $view->getData();

        if ($container instanceof ApiProblem) {
            return new Response($this->serializeApiProblem($container), $container->getStatus(),
                array(
                    'Content-Type' => 'application/problem+json'
                )
            );
        }

        if ($container instanceof SingleItemContainer) {
            return new Response(
                $this->serializeSingleItemContainer($container), 200, $view->getHeaders());
        }

        $items = $container->getItems();
        $serializerGroups = $container->getSerializerGroups();
        $route = $container->getRoute();
        $offset = $container->getOffset();
        $limit = $container->getLimit();
        $excluder = $container->getExcluder();

        if ($container instanceof RecordsContainer) {
            $response = new stdClass();

            $response->_links = new stdClass();
            $response->_links->next = new stdClass();
            $response->_links->next->href = $route . '?first=' . ($offset + $limit) . '&max=' . $limit;
            $response->_links->prev->href = $route . '?first=' . ($offset - $limit) . '&max=' . $limit;

            $response->_embedded = new stdClass();
            $response->_embedded->record = $items;

            return new JsonResponse($response, 200, $view->getHeaders());
        }

        $resource = new ResourceList($items, $serializerGroups, $this->router, $route);
        $resource->prepare();
        if ($offset !== false) {
            $resource->paginate($offset, $limit);
        }

        $context = SerializationContext::create()
            ->setSerializeNull(true)
            ->setGroups($serializerGroups);
        if ($excluder) {
            $context->addExclusionStrategy($excluder);
        }

        $representation = $this->serializer->serialize($resource, 'json', $context);
        return new Response($representation, 200, $view->getHeaders());
    }

    private function serializeApiProblem(ApiProblem $problem)
    {
        $context = SerializationContext::create()
            ->setSerializeNull(true);

        $representation = $this->serializer->serialize($problem, 'json', $context);
        return $representation;
    }

    private function serializeSingleItemContainer(SingleItemContainer $container)
    {
        $item = $container->getItem();

        if ($item instanceof stdClass) {
            return json_encode($item);
        }

        $serializerGroups = $container->getSerializerGroups();

        if ($item instanceof Entity\CourseAssignment) {
            $resource = new CourseAssignmentResource($item, $serializerGroups, $this->router);
        }
        else if ($item instanceof Entity\CourseSession) {
            $resource = new CourseSessionResource($item, $serializerGroups, $this->router);
        }

        $resource->prepare();

        $context = SerializationContext::create()
            ->setSerializeNull(true)
            ->setGroups($serializerGroups);
        if ($container->getExcluder()) {
            $context->addExclusionStrategy($container->getExcluder());
        }
        $representation = $this->serializer->serialize($resource, 'json', $context);

        return $representation;
    }
} 