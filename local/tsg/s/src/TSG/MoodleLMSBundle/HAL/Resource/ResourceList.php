<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 9/4/14
 * Time: 4:12 PM
 */

namespace TSG\MoodleLMSBundle\HAL\Resource;

use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Routing\RouterInterface;
use JMS\Serializer\Annotation as Serializer;

class ResourceList
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     * @Serializer\Exclude()
     */
    protected $router;

    /**
     * @var string
     * @Serializer\Exclude()
     */
    private $route;

    /**
     * @var array
     * @Serializer\Exclude()
     */
    private $serializerGroups;


    /**
     * @var mixed
     * @Serializer\Exclude()
     */
    protected $resources;

    /**
     * @var string type of resource in $this->resources
     * @Serializer\Exclude()
     */
    protected $resourceTypeName;

    /**
     * @Serializer\Groups({"short","detailed"})
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("_embedded")
     */
    public function getEmbeddedResources()
    {
        return array($this->resourceTypeName => $this->resources);
    }


    /**
     * @var array HAL links
     *
     * @Serializer\Groups({"short","detailed"})
     */
    protected $_links;


    public function __construct($entities, array $serializerGroups, RouterInterface $router, $route)
    {
        $entities = $entities instanceof PersistentCollection ? $entities->toArray() : $entities;

        $this->router = $router;
        $this->route = $route;
        $this->serializerGroups = $serializerGroups;

        $this->_links = array();
        $this->resources = array();

        if (count($entities) == 0) {
            $this->resourceTypeName = 'empty';
            return;
        }

        $firstEntity = current(array_slice($entities, 0, 1));
        $this->resourceTypeName = lcfirst($firstEntity->getEntityName());
        $resourceClass = '\\' . __NAMESPACE__ . '\\'
            . $firstEntity->getEntityName() . 'Resource';

        foreach ($entities as $e) {
            $this->resources[] = new $resourceClass($e, $serializerGroups, $router);
        }
    }


    public function prepare()
    {
        if (is_array($this->route)) {
            foreach ($this->route as $rel => $href) {
                $this->_links[$rel] = array ('href' => $href);
            }
        }
        else {
            $this->_links['self'] = array(
                'href' => $this->route
            );
        }

        foreach ($this->resources as $r) {
            $r->prepare();
        }
    }

    /**
     * @param $offset int
     */
    public function paginate($offset, $limit)
    {
        $this->_links['next'] = array(
            'href' => $this->route .
                '?first=' . ($offset + $limit) .
                '&max=' . $limit
        );

        if ($offset - count($this->resources) > -1) {
            $this->_links['prev'] = array(
                'href' => $this->route .
                    '?first=' . ($offset - $limit) .
                    '&max=' . $limit
            );
        }
    }
} 