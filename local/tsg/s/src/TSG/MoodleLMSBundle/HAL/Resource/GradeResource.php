<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 12/10/14
 * Time: 2:32 PM
 */

namespace TSG\MoodleLMSBundle\HAL\Resource;


use Symfony\Component\Routing\RouterInterface;
use TSG\MoodleLMSBundle\Entity\Entity;

class GradeResource extends Resource
{
    const ROUTE = 'tsg_moodle_lms.grade';

    public function __construct(Entity $entity, array $serializerGroups, RouterInterface $router)
    {
        parent::__construct($entity, $serializerGroups, $router);
    }

    public function prepare()
    {
        $this->_links['self'] = array(
            // 'href' => $this->router->generate(self::ROUTE, array('id' => $this->entity->getId()))
        );

        $this->_links['certificate'] = array(
            'href' => $this->router->generate('tsg_moodle_lms.certificate.index', array(
                'gradeid' => '__gradeid__'
            ))
        );
    }
} 