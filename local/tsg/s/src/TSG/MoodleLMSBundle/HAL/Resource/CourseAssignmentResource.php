<?php

namespace TSG\MoodleLMSBundle\HAL\Resource;


use Symfony\Component\Routing\RouterInterface;
use TSG\MoodleLMSBundle\Entity\Entity;

class CourseAssignmentResource extends Resource
{
    const ROUTE = 'NA';

    public function __construct(Entity $entity, array $serializerGroups, RouterInterface $router)
    {
        parent::__construct($entity, $serializerGroups, $router);
    }

    public function prepare()
    {
        global $CFG;

        $this->_links['self'] = array(
            'href' => $CFG->wwwroot
        );
    }
}