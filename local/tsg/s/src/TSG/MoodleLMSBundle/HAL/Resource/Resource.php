<?php

namespace TSG\MoodleLMSBundle\HAL\Resource;

use Symfony\Component\Routing\RouterInterface;
use JMS\Serializer\Annotation as Serializer;
use TSG\MoodleLMSBundle\Entity\Entity;

abstract class Resource
{
    /**
     * @var Entity
     *
     * @Serializer\Groups({"short","detailed"})
     * @Serializer\Inline()
     */
    protected $entity;

    /**
     * @var array
     * @Serializer\Exclude()
     */
    protected $serializerGroups;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     * @Serializer\Exclude()
     */
    protected $router;

    /**
     * @var array HAL links
     *
     * @Serializer\Groups({"short","detailed"})
     */
    protected $_links;

    public function __construct($entity, array $serializerGroups, RouterInterface $router)
    {
        $this->entity = $entity;
        $this->serializerGroups = $serializerGroups;
        $this->router = $router;
        $this->_links = array();
    }

    public abstract function prepare();
} 