<?php

namespace TSG\MoodleLMSBundle\HAL\Resource;

use TSG\MoodleLMSBundle\Entity\Entity;
use Symfony\Component\Routing\RouterInterface;

class SessionRosterResource extends Resource
{
    const ROUTE = 'tsg_moodle_lms.session.update';
    const ROSTER_ROUTE = 'tsg_moodle_lms.roster.index';

    public function __construct(Entity $entity, array $serializerGroups, RouterInterface $router)
    {
        parent::__construct($entity, $serializerGroups, $router);
    }

    public function prepare()
    {
        /*
        $this->_links['self'] = array(
            'href' => $this->router->generate(self::ROUTE, array(
                'id' => $this->entity->getId())
            )
        );

        $this->_links['roster'] = array(
            'href' => $this->router->generate(self::ROSTER_ROUTE, array(
                'sessionid' => $this->entity->getId())
            )
        );
        */
    }
}