<?php

namespace TSG\MoodleLMSBundle\HAL\Resource;

use Symfony\Component\Routing\RouterInterface;
use JMS\Serializer\Annotation as JMSAnnotation;
use TSG\MoodleLMSBundle\Entity\Entity;
use TSG\MoodleLMSBundle\Entity\Id;

class CourseCategoryResource extends Resource {
    const ROUTE = 'tsg_moodle_lms';

    public function __construct(Entity $entity, array $serializerGroups, RouterInterface $router)
    {
        parent::__construct($entity, $serializerGroups, $router);
    }

    public function prepare()
    {
        /*
        $this->_links['self'] = array(
            'href' => $this->router->generate(self::ROUTE, array('id' => $this->entity->getId()))
        );
        */
    }
} 