<?php


namespace TSG\MoodleLMSBundle\HAL\Resource;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Routing\RouterInterface;
use TSG\MoodleLMSBundle\Entity\UserGroup;

class UserGroupResource
{
    const ROUTE = 'tsg_moodle_lms';

    /**
     * @var array
     * @Serializer\Exclude()
     */
    private $serializerGroups;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     * @Serializer\Exclude()
     */
    private $router;


    /**
     * @var array HAL links
     *
     * @Serializer\Groups({"short","detailed"})
     */
    protected $_links;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     */
    private $id;

    /**
     * @var self[]
     *
     * @Serializer\Groups({"parents"})
     */
    private $parent;

    /**
     * @var self[]
     *
     * @Serializer\Groups({"children"})
     */
    private $children;


    /**
     * @var \TSG\MoodleLMSBundle\Entity\User[]
     *
     * @Serializer\Groups({"collections"})
     */
    private $users;

    /**
     * @var int
     *
     * @Serializer\Groups({"short"})
     */
    private $level = 0;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     */
    private $name;

    /**
     * @var boolean
     *
     * @Serializer\Groups({"short"})
     */
    private $isProfileGroup;



    public function __construct(UserGroup $entity, array $serializerGroups, RouterInterface $router)
    {
        $this->serializerGroups = $serializerGroups;
        $this->router = $router;

        /*
         * Copy entity properties
         */

        $this->id = $entity->getId();
        $this->parent = $entity->getParent();
        $this->children = array();
        $this->users = $entity->getUsers();
        $this->level = $entity->getLevel();
        $this->name = $entity->getName();
        $this->isProfileGroup = $entity->isProfileGroup();

        // replace $entity->children with resources instead of entities
        if ($entity->hasChildren()) {
            foreach ($entity->getChildren() as $childEntity) {
                $this->children[] = new UserGroupResource($childEntity, $serializerGroups, $router);
            }
        }
    }

    public function prepare()
    {
        /*
        $this->_links['self'] = array(
            'href' => $this->router->generate(self::ROUTE, array('id' => $this->entity->getId()))
        );
        */

        $this->_links['member'] = array(
            'href' => $this->router->generate('tsg_moodle_lms.usergroup.rest.members.fetch', array(
                'groupid' => $this->id
            ))
        );

        $this->_links['manager'] = array(
            'href' => $this->router->generate('tsg_moodle_lms.usergroup.rest.managers.fetch', array(
                'groupid' => $this->id
            ))
        );

        if ($this->children) {
            array_walk($this->children, function($c) { $c->prepare(); });
        }
    }
} 