<?php
namespace TSG\MoodleLMSBundle\HAL\Problem;


use Symfony\Component\Routing\RouterInterface;

class NotLoggedInProblem extends ApiProblem
{
    const TYPE_ROUTE = 'tsg_moodle_lms.api.problem.notloggedin';
    const TITLE      = 'Not Logged In';
    const STATUS     = 401; // http status code 'Unauthorized'


    protected function __construct($type, $detail='', $instance='')
    {
        parent::__construct($type, self::TITLE, self::STATUS, $detail, $instance);
    }


    public static function create(RouterInterface $router, $detail='', $instance='')
    {
        $type = $router->generate(self::TYPE_ROUTE);
        return new NotLoggedInProblem($type, $detail, $instance);
    }
}