<?php
namespace TSG\MoodleLMSBundle\HAL\Problem;


use JMS\Serializer\Annotation as Serializer;


/**
 * Class ApiProblem
 * @package TSG\MoodleLMSBundle\HAL\Problem
 *
 * @Serializer\ReadOnly
 * @Serializer\AccessType("public_method")
 */
class ApiProblem
{
    /**
     * @var string a URI that uniquely identifies this problem type
     */
    private $type;

    /**
     * @var string a short, human-readable description of this problem type.  Should be the same
     *             for all occurrences.
     */
    private $title;

    /**
     * @var int the HTTP status code
     */
    private $status;

    /**
     * @var string (optional) a human readable explanation specific to this occurrence of the problem.
     */
    private $detail;

    /**
     * @var string (optional) a URI that identifies the specific occurrence of the problem.
     */
    private $instance;


    protected function __construct($type, $title, $status, $detail='', $instance='')
    {
        $this->type = $type;
        $this->title = $title;
        $this->status = $status;
        $this->detail = $detail;
        $this->instance = $instance;
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * @return mixed
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @return mixed
     */
    public function getInstance()
    {
        return $this->instance;
    }



}