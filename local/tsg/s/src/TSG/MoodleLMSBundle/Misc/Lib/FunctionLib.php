<?php

namespace TSG\MoodleLMSBundle\Misc\Lib;

/**
 * Class FunctionLib provides a library of utility functions not specific to any one area
 *
 * @package TSG\MoodleLMSBundle\Misc\Lib
 */
class FunctionLib
{
    /**
     * Index an array of objects by keys obtained from each object through functions passed
     * in $keyFunctions.  Each key function provides one level of the index.  For example:
     *
     * Given:
     *
     * ```php
     * $objects      = array($o1, $o2, $o3)
     * $keyFunctions = array('k1' => function ($v) { return $v->getProperty1(); },
     *                       'k2' => function ($v) { return $v->getX()->getSubProperty2(); })
     * ```
     * If:
     * ```
     * $o1->getProperty1() == 'a', $o1->getX()->getSubProperty2() == 'b'
     * $o2->getProperty1() == 'c', $o2->getX()->getSubProperty2() == 'd'
     * $o3->getProperty1() == 'c', $o3->getX()->getSubProperty2() == 'd'
     * ```
     *
     * Returns:
     *
     * ```php
     * array(
     *     'a' => array(
     *         'b' => array($o1)
     *     ),
     *
     *     'c' => array(
     *         'd' => array($o2, $o3)
     *     )
     * )
     * ```
     *
     * @api
     *
     * @param array $objects array of objects
     * @param array $keyFunctions array of functions to be applied to each object to obtain keys
     *
     * @return array indexed by the result of each keyFunction
     */
    public static function indexObjectArray(array $objects, array $keyFunctions)
    {
        $indexed = array();

        array_walk($objects, function ($value) use (&$indexed, $keyFunctions) {

            // get the keys for this entry in $array
            $keys =
                array_map(function ($k) use ($value) {
                    return $k($value); // call the key function
                }, $keyFunctions);

            // start at the root of $indexedArray and insert a key at successively deeper
            // levels, one per keyFunction
            $element = &$indexed;
            foreach ($keys as $k) {
                $element = &$element[$k];
            }

            // add the current entry in $array to
            $element[] = $value;
        });

        return $indexed;
    }
}