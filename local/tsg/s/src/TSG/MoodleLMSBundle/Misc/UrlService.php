<?php

namespace TSG\MoodleLMSBundle\Misc;


use Symfony\Component\Routing\RouterInterface;
use TSG\MoodleLMSBundle\HAL\Problem\NotLoggedInProblem;

class UrlService
{
    private $router;
    private $useAccelerator;

    public function __construct(RouterInterface $router, $useAccelerator)
    {
        $this->router = $router;
        $this->useAccelerator = $useAccelerator;
    }

    public function assignment()
    {
        return array(
            'assignAndEnrollUser' =>
                $this->router->generate('tsg_moodle_lms.assignmentmanagement.api.assign_and_enroll_user'),

            'search' => 
                $this->router->generate('tsg_moodle_lms.assignmentmanagement.api.search')
        );
    }

    public function course($courseid = null)
    {
        $courseid = is_null($courseid) ? '__courseid__' : $courseid;

        return array(
            'view' => '/course/view.php?id=' . $courseid,

            'findByPattern' => $this->useAccelerator ?
                // '/local/tsg/s/web/acc/user_autocomplete.php' :
                $this->router->generate('tsg_moodle_lms.course.findByPattern') :
                $this->router->generate('tsg_moodle_lms.course.findByPattern'),

            'sessions' => $this->router->generate('tsg_moodle_lms.course.sessions', array(
                'courseid' => $courseid
            ))
        );
    }

    public function courseCategory($categoryid = null)
    {
        $categoryid = is_null($categoryid) ? '__categoryid__' : $categoryid;

        return array(
            'children' => $this->router->generate('tsg_moodle_lms.coursecategory.children', array(
                'parentid' => $categoryid
            ))
        );
    }

    public function frontpage()
    {
        return array(
            'incompletesForCurrentUser' =>
                $this->router->generate('tsg_moodle_lms.frontpage.api.incompletes_for_current_user'),

            'completionsForCurrentUser' =>
                $this->router->generate('tsg_moodle_lms.frontpage.api.completions_for_current_user')
        );
    }

    public function grade()
    {

    }

    public function session($sessionid = null)
    {
        $sessionid = is_null($sessionid) ? '__sessionid__' : $sessionid;

        return array(
            'insert' => $this->router->generate('tsg_moodle_lms.session.insert'),
            'update' => $this->router->generate('tsg_moodle_lms.session.update', array(
                'id' => $sessionid
            )),
            'delete' => $this->router->generate('tsg_moodle_lms.session.delete', array(
                'id' => $sessionid
            ))
        );
    }

    public function user()
    {
        return array(
            'findByPattern' => $this->useAccelerator ?
                '/local/tsg/s/web/acc/user_autocomplete.php' :
                $this->router->generate('tsg_moodle_lms.user.findByPattern')
        );
    }

    public function userGroup()
    {
        return array(
            'fetch_all' =>
                $this->router->generate('tsg_moodle_lms.usergroup.rest.fetch.all'),
            'add' =>
                $this->router->generate('tsg_moodle_lms.usergroup.rest.add'),
            'remove' =>
                $this->router->generate('tsg_moodle_lms.usergroup.rest.delete', array(
                    'groupid' => '__groupid__'
                )),

            'members' => array(
                'add' =>
                    $this->router->generate('tsg_moodle_lms.usergroup.rest.members.add', array(
                        'groupid' => '__groupid__'
                    )),
                'remove' =>
                    $this->router->generate('tsg_moodle_lms.usergroup.rest.members.remove', array(
                        'groupid' => '__groupid__'
                    ))
            ),

            'managers' => array(
                'add' =>
                    $this->router->generate('tsg_moodle_lms.usergroup.rest.managers.add', array(
                        'groupid' => '__groupid__'
                    )),
                'remove' =>
                    $this->router->generate('tsg_moodle_lms.usergroup.rest.managers.remove', array(
                        'groupid' => '__groupid__'
                    ))
            )
        );
    }

    public function apiProblem()
    {
        return array(
            'notloggedin' => $this->router->generate('tsg_moodle_lms.api.problem.notloggedin'),
            'badresponse' => $this->router->generate('tsg_moodle_lms.api.problem.badresponse')
        );
    }
}