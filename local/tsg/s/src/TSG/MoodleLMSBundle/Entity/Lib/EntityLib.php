<?php
namespace TSG\MoodleLMSBundle\Entity\Lib;

use TSG\MoodleLMSBundle\Entity\CourseAssignment;
use TSG\MoodleLMSBundle\Entity\CourseSession;
use TSG\MoodleLMSBundle\Entity\Grade;
use TSG\MoodleLMSBundle\Misc\Lib\FunctionLib;

class EntityLib
{
    /**
     * Given a list of CourseAssignments and related Grades, return a new list of CourseAssignments
     * with completion properties (isComplete, completionDate) set properly.
     *
     * @param CourseAssignment[] $assignments
     * @param Grade[] $grades
     * @return CourseAssignment[]
     */
    public static function addCompletionInfoToAssignments($assignments, $grades)
    {
        if (empty($assignments)) {
            return $assignments;
        }

        $grades = FunctionLib::indexObjectArray($grades,
            array(
                'userId'   => function (Grade $g) { return $g->getUser()->getId(); },
                'courseId' => function (Grade $g) { return $g->getItem()->getCourse()->getId(); }
            )
        );

        $withGrades = array_map(function ($a) use ($grades) {

            $a->setGrades($grades[$a->getUser()->getId()][$a->getCourse()->getId()]);
            return $a;

        }, $assignments);

        return array_map(function ($a) {

            $completionGrade = EntityLib::completionGradeForAssignment($a, $a->getGrades());
            if ($completionGrade) {
                $a->setCompletionDate($completionGrade->getTimeModified());
            }
            else {
                $a->setComplete(false);
            }
            return $a;

        }, $withGrades);
    }

    /**
     * Find the completion grade for the given CourseAssignment from the given Grade[].
     *
     * Searches grades for a passing score with the appropriate GradeItem::idNumber.  idNumber
     * must follow the pattern 'YYYY%' where YYYY is the year of CourseAssignment::deadline.
     *
     * @param CourseAssignment $assignment
     * @param Grade[] $grades
     * @return Grade|false
     */
    public static function completionGradeForAssignment(CourseAssignment $assignment, $grades)
    {
        if (!is_array($grades)) {
            return false;
        }

        foreach ($grades as $g) {
            if ($g->getItem()->getCourse()->getId() === $assignment->getCourse()->getId() &&
                strpos($g->getItem()->getIdNumber(), date('Y', $assignment->getDeadline())) === 0 &&
                $g->getFinalGrade() >= $g->getItem()->getGradePass()) {

                return $g;
            }
        }

        return false;
    }

    /**
     * @param Grade[] $grades
     * @param CourseSession[] $sessions
     * @return Grade[]
     */
    public static function addSessionsToGrades($grades, $sessions)
    {
        $sessions = FunctionLib::indexObjectArray($sessions, array(
            'id' => function (CourseSession $s) { return $s->getId(); }
        ));

        $gradesWithSessions = array_map(function (Grade $g) use ($sessions) {

            $s = $sessions[$g->getItem()->getItemInstance()];
            if ($s) {
                $g->getItem()->setSession($s[0]);
            }
            return $g;

        }, $grades);

        return $gradesWithSessions;
    }
}