<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserInfo
 *
 * @ORM\Table(name="lsc_user_info")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\UserInfoRepository")
 */
class UserInfo implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="info")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="hire_date", type="bigint")
     */
    private $hireDate;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="termination_date", type="bigint")
     */
    private $terminationDate;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="isactive", type="smallint")
     */
    private $isActive;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="idnum", type="string", length=30)
     */
    private $idnum;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     * @return UserInfo
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set hireDate
     *
     * @param integer $hireDate
     * @return UserInfo
     */
    public function setHireDate($hireDate)
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    /**
     * Get hireDate
     *
     * @return integer 
     */
    public function getHireDate()
    {
        return $this->hireDate;
    }

    /**
     * Set terminationDate
     *
     * @param integer $terminationDate
     * @return UserInfo
     */
    public function setTerminationDate($terminationDate)
    {
        $this->terminationDate = $terminationDate;

        return $this;
    }

    /**
     * Get terminationDate
     *
     * @return integer 
     */
    public function getTerminationDate()
    {
        return $this->terminationDate;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     * @return UserInfo
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer 
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * Set idnum
     *
     * @param string $idnum
     * @return UserInfo
     */
    public function setIdnum($idnum)
    {
        $this->idnum = $idnum;

        return $this;
    }

    /**
     * Get idnum
     *
     * @return string 
     */
    public function getIdnum()
    {
        return $this->idnum;
    }

    public function getEntityName()
    {
        return 'UserInfo';
    }

    public function toArray()
    {
        return array();
    }
}
