<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 12/4/14
 * Time: 5:38 PM
 */

namespace TSG\MoodleLMSBundle\Entity;


interface Entity
{
    public function getEntityName();
    public function toArray();
} 