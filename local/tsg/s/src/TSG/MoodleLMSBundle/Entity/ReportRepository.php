<?php

namespace TSG\MoodleLMSBundle\Entity;


use Symfony\Component\HttpFoundation\ParameterBag;

class ReportRepository
{
    private $logger;
    private $em;


    public function __construct($em, $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }


    public function assignments(ParameterBag $params, $first=0, $max=10)
    {
        $this->logger->debug(__METHOD__ . ': entry');
        $this->logger->debug(__METHOD__ . ': $first=' . $first . ', $max=' . $max);

        if ($max > 0) {
            // paged results
            $ids = $this->assignmentIds($params, $first, $max);
            $queryResult = $this->assignmentsForIds($ids);
        }
        else {
            // all results
            $queryResult = $this->assignmentsX($params);
        }


        /*
         * Flatten object graph
         */

        $flat = array();
        foreach ($queryResult as $q) {

            // Flatten the assignment object into a record
            $record = array(
                'record_type' => 'assignment',
                'user_id' => $q->getUser()->getId(),
                'user_lastname' => $q->getUser()->getLastName(),
                'user_firstname' => $q->getUser()->getFirstName(),
                'user_email' => $q->getUser()->getEmail(),
                'course_id' => $q->getCourse()->getId(),
                'course_fullname' => $q->getCourse()->getFullName(),
                'course_format' => $q->getCourse()->getFormat(),
                'course_category_name' => $q->getCourse()->getCategory()->getName()
            );

            // Add User.info (which may be empty) fields
            $userInfo = $q->getUser()->getInfo();

            $record['user_termination_date'] = !empty($userInfo) ? $userInfo->getTerminationDate() : '';
            $record['user_isactive'] = !empty($userInfo) ? $userInfo->isActive() : '';

            // Add Course.info (which may be empty) fields
            $courseInfo = $q->getCourse()->getInfo();

            $record['course_hours'] = empty($courseInfo) ? '' : $courseInfo->getHours();
            $record['course_credits'] = empty($courseInfo) ? '' : $courseInfo->getCredits();
            $record['course_cost_per_student'] = empty($courseInfo) ? '' : $courseInfo->getCostPerStudent();

            // Assignment specific fields
            $record['deadline'] = $q->getDeadline();

            // Dummy fields for other record types
            $record['final_grade'] = '';
            $record['completion_date'] = '';
            $record['session_startdate'] = '';
            $record['attended'] = '';
            $record['hours_attended'] = '';

            $flat[] = $record;
        }

        $this->attachProfileGroups($flat);

        return $flat;
    }

    /**
     * Queries database for completion (Grade) records that match criteria specified
     * in $params.  The structure of the response is as follows:
     *
     * array(
     *  'grade_id' => ..,
     *  'user_id' => ..,
     *  'user_lastname' => ..,
     *  'user_firstname' => ..,
     *  'course_id' => ..,
     *  'course_fullname' => ..,
     *  'course_format' ..,
     *  'course_hours' => ..,
     *  'course_credits' => ..,
     *  'course_cost_per_student' => ..,
     *  'course_category_name' => ..,
     *  'final_grade' => ..,
     *  'completion_date' => ..,
     *  'user_profilegroups => array(
     *      profileGroupId_1 => "memberGroupName",
     *      profileGroupId_2 => "memberGroupName"
     *  )
     * )
     *
     * @param ParameterBag $params
     * @param int $first
     * @param int $max
     * @return array a flattened array of grade objects and associations
     */
    public function completions(ParameterBag $params, $first = 0, $max = 10)
    {
        $this->logger->debug(__METHOD__ . ': entry');
        $this->logger->debug(__METHOD__ . ': $first=' . $first . ', $max=' . $max);

        if ($max > 0) {
            // paged results
            $ids = $this->completionIds($params, $first, $max);
            $queryResult = $this->completionsForIds($ids);
        }
        else {
            // all results
            $queryResult = $this->completionsX($params);
        }


        /*
         * Flatten object graph
         */

        $flat = array();
        foreach ($queryResult as $q) {

            // Flatten the grade object into a record
            $record = array(
                'record_type' => 'completion',
                'user_id' => $q->getUser()->getId(),
                'user_lastname' => $q->getUser()->getLastName(),
                'user_firstname' => $q->getUser()->getFirstName(),
                'user_email' => $q->getUser()->getEmail(),
                'course_id' => $q->getItem()->getCourse()->getId(),
                'course_fullname' => $q->getItem()->getCourse()->getFullName(),
                'course_format' => $q->getItem()->getCourse()->getFormat(),
                'course_category_name' => $q->getItem()->getCourse()->getCategory()->getName()
            );

            // Add User.info (which may be empty) fields
            $userInfo = $q->getUser()->getInfo();

            $record['user_termination_date'] = !empty($userInfo) ? $userInfo->getTerminationDate() : '';
            $record['user_isactive'] = !empty($userInfo) ? $userInfo->isActive() : '';

            // Add Course.info (which may be empty) fields
            $courseInfo = $q->getItem()->getCourse()->getInfo();

            $record['course_hours'] = empty($courseInfo) ? '' : $courseInfo->getHours();
            $record['course_credits'] = empty($courseInfo) ? '' : $courseInfo->getCredits();
            $record['course_cost_per_student'] = empty($courseInfo) ? '' : $courseInfo->getCostPerStudent();

            // Completion record specific fields
            $record['grade_id'] = $q->getId();
            $record['final_grade'] = $q->getFinalGrade();
            $record['completion_date'] = $q->getTimeModified();

            // Dummy fields for other record types
            $record['deadline'] = '';
            $record['session_startdate'] = '';
            $record['attended'] = '';
            $record['hours_attended'] = '';

            $flat[] = $record;
        }



        $this->attachProfileGroups($flat);

        return $flat;
    }

    public function enrollments(ParameterBag $params, $first=0, $max=10)
    {
        $this->logger->debug(__METHOD__ . ': entry');
        $this->logger->debug(__METHOD__ . ': $first=' . $first . ', $max=' . $max);

        if ($max > 0) {
            // paged results
            $ids = $this->enrollmentIds($params, $first, $max);
            $queryResult = $this->enrollmentsForIds($ids);
        }
        else {
            // all results
            $queryResult = $this->enrollmentsX($params);
        }

        /*
         * Flatten object graph
         */

        $flat = array();
        foreach ($queryResult as $q) {

            // Flatten the grade object into a record
            $record = array(
                'record_type' => 'enrollment',
                'user_id' => $q->getUser()->getId(),
                'user_lastname' => $q->getUser()->getLastName(),
                'user_firstname' => $q->getUser()->getFirstName(),
                'user_email' => $q->getUser()->getEmail(),
                'course_id' => $q->getSession()->getCourse()->getId(),
                'course_fullname' => $q->getSession()->getCourse()->getFullName(),
                'course_format' => $q->getSession()->getCourse()->getFormat(),
                'course_category_name' => $q->getSession()->getCourse()->getCategory()->getName()
            );

            // Add User.info (which may be empty) fields
            $userInfo = $q->getUser()->getInfo();

            $record['user_termination_date'] = !empty($userInfo) ? $userInfo->getTerminationDate() : '';
            $record['user_isactive'] = !empty($userInfo) ? $userInfo->isActive() : '';

            // Add Course.info (which may be empty) fields
            $courseInfo = $q->getSession()->getCourse()->getInfo();

            $record['course_hours'] = empty($courseInfo) ? '' : $courseInfo->getHours();
            $record['course_credits'] = empty($courseInfo) ? '' : $courseInfo->getCredits();
            $record['course_cost_per_student'] = empty($courseInfo) ? '' : $courseInfo->getCostPerStudent();

            // Enrollment specific fields
            $record['enrollment_id'] = $q->getId();
            $record['session_startdate'] = $q->getSession()->getStartDate();
            $record['attended'] = $q->getAttended();
            $record['hours_attended'] = $q->getHoursAttended();

            // Dummy fields for other record types
            $record['deadline'] = '';
            $record['final_grade'] = '';
            $record['completion_date'] = '';

            $flat[] = $record;
        }

        $this->attachProfileGroups($flat);

        return $flat;
    }

    private function assignmentIds(ParameterBag $params, $first=0, $max=10)
    {
        /*
         * Find id's of assignment records within the first/max range
         */

        $qb = $this->em->createQueryBuilder();
        $qb->select('distinct a.id')
            ->from('TSGMoodleLMSBundle:CourseAssignment', 'a')
            ->join('a.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->leftJoin('u.groups', 'ug')
            ->leftJoin('u.tags', 'utag')
            ->join('a.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->leftJoin('c.tags', 'ctag')
            ->join('c.category', 'cat');

        $this->filterForManagedGroups($qb, 'ug', $params);
        $this->addAssignmentCriteria($params, $qb);

        $ids = $qb->getQuery()
            ->setFirstResult($first)
            ->setMaxResults($max)
            ->getResult();

        return $ids;
    }

    private function assignmentsForIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        $qb = $this->em->createQueryBuilder();
        $qb->select('a, u, ui, c, ci, cat')
            ->from('TSGMoodleLMSBundle:CourseAssignment', 'a')
            ->join('a.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->join('a.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->join('c.category', 'cat');

        $in = array();
        foreach ($ids as $id) {
            $in[] = $id['id'];
        }

        $qb->where($qb->expr()->in('a.id', $in));
        $assignments = $qb->getQuery()
            ->getResult();

        return $assignments;
    }

    private function assignmentsX(ParameterBag $params)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('a, u, ui, c, ci, cat')
            ->from('TSGMoodleLMSBundle:CourseAssignment', 'a')
            ->join('a.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->leftJoin('u.groups', 'ug')
            ->leftJoin('u.tags', 'utag')
            ->join('a.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->leftJoin('c.tags', 'ctag')
            ->join('c.category', 'cat');

        $this->filterForManagedGroups($qb, 'ug', $params);
        $this->addAssignmentCriteria($params, $qb);

        $this->logger->debug(__METHOD__ . ': executing query');
        $assignments = $qb->getQuery()
            ->getResult();

        $this->logger->debug(__METHOD__ . ': exit, returning ' . count($assignments) . ' records');
        return $assignments;
    }

    private function addAssignmentCriteria(ParameterBag $params, &$qb)
    {
        $this->addQueryParam($qb, 'c.id', 'courseId', $params->get('courseId'));
        $this->addQueryParam($qb, 'ctag.id', 'ctagname', $params->get('tagId'));
        $this->addQueryParam($qb, 'utag.id', 'utagname', $params->get('tagId'));
        $this->addQueryParam($qb, 'u.id', 'userId', $params->get('userId'));

        $userGroupId = $params->get('userGroupId');
        $this->logger->debug('userGroupId = ' . $userGroupId);

        $includeSubGroups = $params->get('includeUserSubGroups');
        $this->logger->debug('includeSubGroups = ' . $includeSubGroups);

        if ($userGroupId != 0) {
            $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
            $selectedGroup = $userGroupRepo->find($params->get('userGroupId'));

            if ($includeSubGroups !== 'true') {
                $this->logger->debug('filtering for selected group');
                $qb->andWhere(':groupId member of u.groups');
                $qb->setParameter(':groupId', $selectedGroup);
            }
            else {
                $this->logger->debug('filtering for selected group\'s children');
                $subGroups = $selectedGroup->getChildren();

                $this->logger->debug('building expression');

                $x = '(';
                for ($i = 0; $i < count($subGroups); $i++) {
                    $sg = $subGroups[$i];
                    $this->logger->debug($sg->getName());

                    $x .= ':group_' . $i . ' member of u.groups';
                    if (($i + 1) < count($subGroups))
                        $x .= (' or ' . PHP_EOL);

                    $qb->setParameter(':group_' . $i, $sg);
                }
                $x .= ')';

                $qb->andWhere($x);
            }
        }

        $includeTerminated = $params->get('includedTerminated');
        if ($includeTerminated === 'false') {
            $qb->andWhere($qb->expr()->isNull('ui.terminationDate'));
        }

        $includeInactive = $params->get('includeInactive');
        if ($includeInactive === 'false') {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('ui.isActive', 1),
                $qb->expr()->isNull('ui.isActive')
            ));
        }

        $hiredWithin = $params->get('hiredWithin');
        if ($hiredWithin !== null && $hiredWithin !== 'false') {
            $hiredWithinDays = $params->get('hiredWithinDays');
            $today = mktime(0,0,0);
            $hireDate = $today - (24*60*60*$hiredWithinDays);
            $qb->andWhere($qb->expr()->gt('ui.hireDate', ':hireDate'));
            $qb->setParameter('hireDate', $hireDate);
        }

        $startDate = $params->get('startDate');
        $startDate = $startDate !== 'false' ? $startDate / 1000 : false;
        if ($startDate !== false) {
            $qb->andWhere($qb->expr()->gte('a.deadline', ':startDate'));
            $qb->setParameter('startDate', $startDate);
        }

        $endDate = $params->get('endDate');
        $endDate = $endDate !== 'false' ? ($endDate / 1000 + 60*60*24) : false;
        if ($endDate !== false) {
            $qb->andWhere($qb->expr()->lt('a.deadline', ':endDate'));
            $qb->setParameter('endDate', $endDate);
        }
    }


    private function completionIds(ParameterBag $params, $first=0, $max=10)
    {
        /*
         * Find id's of grade records within the first/max range
         */

        $qb = $this->em->createQueryBuilder();

        $qb->select('distinct g.id')
            ->from('TSGMoodleLMSBundle:Grade', 'g')
            ->join('g.item', 'gi')
            ->join('g.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->leftJoin('u.groups', 'ug')
            ->leftJoin('u.tags', 'utag')
            ->join('gi.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->leftJoin('c.tags', 'ctag')
            ->join('c.category', 'cat')
            ->where($qb->expr()->in('gi.itemModule', array('ilt', 'quiz', 'scorm')))
            ->andWhere($qb->expr()->gte('g.finalGrade', 'gi.gradePass'));

        $this->filterForManagedGroups($qb, 'ug', $params);
        $this->addCompletionCriteria($params, $qb);

        $this->logger->debug(__METHOD__ . ': executing query');
        $ids = $qb->getQuery()
            ->setFirstResult($first)
            ->setMaxResults($max)
            ->getResult();

        $this->logger->debug(__METHOD__ . ': exit, returning ' . count($ids) . ' ids');
        return $ids;
    }

    private function completionsForIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        /*
         * Use grade id's filter for full query
         */

        $qb = $this->em->createQueryBuilder();
        $qb->select('g, gi, c, ci, cat, u, ui')
            ->from('TSGMoodleLMSBundle:Grade', 'g')
            ->join('g.item', 'gi')
            ->join('g.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->join('gi.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->join('c.category', 'cat');

        $in = array();
        foreach ($ids as $id) {
            $in[] = $id['id'];
        }

        $qb->where($qb->expr()->in('g.id', $in));
        $queryResult = $qb->getQuery()
            ->getResult();

        return $queryResult;
    }

    private function completionsX(ParameterBag $params)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('g, gi, c, ci, cat, u, ui')
            ->from('TSGMoodleLMSBundle:Grade', 'g')
            ->join('g.item', 'gi')
            ->join('g.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->leftJoin('u.groups', 'ug')
            ->leftJoin('u.tags', 'utag')
            ->join('gi.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->leftJoin('c.tags', 'ctag')
            ->join('c.category', 'cat')
            ->where($qb->expr()->in('gi.itemModule', array('ilt', 'quiz', 'scorm')))
            ->andWhere($qb->expr()->gte('g.finalGrade', 'gi.gradePass'));

        $this->logger->debug(__METHOD__ . ': built query');

        $this->filterForManagedGroups($qb, 'ug', $params);
        $this->addCompletionCriteria($params, $qb);

        $this->logger->debug(__METHOD__ . ': executing query');
        $completions = $qb->getQuery()
            ->getResult();

        $this->logger->debug(__METHOD__ . ': exit, returning ' . count($completions) . ' records');
        return $completions;
    }

    private function filterForManagedGroups(&$qb, $groupsAlias, ParameterBag $params)
    {
        if ($params->has('filterForManagedGroupsOfUserId')) {

            $mgqb = $this->em->createQueryBuilder();
            $mgqb->select('mg.id')
                ->from('TSGMoodleLMSBundle:User', 'u')
                ->join('u.managedGroups', 'mg')
                ->where($mgqb->expr()->eq('u.id', ':userid'))
                ->setParameter('userid', $params->get('filterForManagedGroupsOfUserId'));

            $managedGroupIds = $mgqb->getQuery()->getResult();
            $managedGroupIds = array_map(function ($v) { return $v['id']; }, $managedGroupIds );

            $qb->where(

                $qb->expr()->orX(

                    $qb->expr()->eq('u', ':self'),

                    empty($managedGroupIds) ?
                        '1=1' :
                        $qb->expr()->in('ug', $managedGroupIds)
                )
            );

            global $USER;
            $qb->setParameter('self', $USER->id);
        }
    }

    /**
     * @param ParameterBag $params
     * @param $qb
     * @return mixed
     */
    private function addCompletionCriteria(ParameterBag $params, &$qb)
    {
        $this->addQueryParam($qb, 'c.id', 'courseId', $params->get('courseId'));
        $this->addQueryParam($qb, 'ctag.id', 'ctagname', $params->get('tagId'));
        $this->addQueryParam($qb, 'utag.id', 'utagname', $params->get('tagId'));
        $this->addQueryParam($qb, 'u.id', 'userId', $params->get('userId'));

        $this->logger->debug(__METHOD__ . ': added basic parameters');

        $userGroupId = $params->get('userGroupId');
        $this->logger->debug('userGroupId = ' . $userGroupId);

        $includeSubGroups = $params->get('includeUserSubGroups');
        $this->logger->debug('includeSubGroups = ' . $includeSubGroups);

        if ($userGroupId != 0) {
            $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
            $selectedGroup = $userGroupRepo->find($params->get('userGroupId'));

            if ($includeSubGroups !== 'true') {
                $this->logger->debug('filtering for selected group');
                $qb->andWhere(':groupId member of u.groups');
                $qb->setParameter(':groupId', $selectedGroup);
            }
            else {
                $this->logger->debug('filtering for selected group\'s children');
                $subGroups = $selectedGroup->getChildren();

                $this->logger->debug('building expression');

                $x = '(';
                for ($i = 0; $i < count($subGroups); $i++) {
                    $sg = $subGroups[$i];
                    $this->logger->debug($sg->getName());

                    $x .= ':group_' . $i . ' member of u.groups';
                    if (($i + 1) < count($subGroups))
                        $x .= (' or ' . PHP_EOL);

                    $qb->setParameter(':group_' . $i, $sg);
                }
                $x .= ')';

                $qb->andWhere($x);
            }
        }

        $hiredWithin = $params->get('hiredWithin');
        if ($hiredWithin !== null && $hiredWithin !== 'false') {
            $hiredWithinDays = $params->get('hiredWithinDays');
            $today = mktime(0,0,0);
            $hireDate = $today - (24*60*60*$hiredWithinDays);
            $qb->andWhere($qb->expr()->gt('ui.hireDate', ':hireDate'));
            $qb->setParameter('hireDate', $hireDate);
        }

        $includeTerminated = $params->get('includedTerminated');
        if ($includeTerminated == null || $includeTerminated === 'false') {
            $qb->andWhere($qb->expr()->isNull('ui.terminationDate'));
        }

        $includeInactives = $params->get('includeInactives');
        if ($includeInactives == null || $includeInactives === 'false') {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('ui.isActive', 1),
                $qb->expr()->isNull('ui.isActive')
            ));
        }

        $startDate = $params->get('startDate');
        $startDate = $startDate !== 'false' ? $startDate / 1000 : false;
        if ($startDate !== false) {
            $qb->andWhere($qb->expr()->gte('g.timeModified', ':startDate'));
            $qb->setParameter('startDate', $startDate);
        }

        $endDate = $params->get('endDate');
        $endDate = $endDate !== 'false' ? ($endDate / 1000 + 60 * 60 * 24) : false;
        if ($endDate !== false) {
            $qb->andWhere($qb->expr()->lt('g.timeModified', ':endDate'));
            $qb->setParameter('endDate', $endDate);
        }
    }


    private function enrollmentIds(ParameterBag $params, $first=0, $max=10)
    {
        /*
         * Find id's of enrollment records within the first/max range
         */

        $qb = $this->em->createQueryBuilder();
        $qb->select('distinct r.id')
            ->from('TSGMoodleLMSBundle:SessionRoster', 'r')
            ->join('r.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->leftJoin('u.groups', 'ug')
            ->leftJoin('u.tags', 'utag')
            ->join('r.session', 's')
            ->join('s.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->leftJoin('c.tags', 'ctag')
            ->join('c.category', 'cat');

        $this->filterForManagedGroups($qb, 'ug', $params);
        $this->addEnrollmentCriteria($params, $qb);

        $ids = $qb->getQuery()
            ->setFirstResult($first)
            ->setMaxResults($max)
            ->getResult();

        return $ids;
    }

    private function enrollmentsForIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        /*
         * Use enrollment id's filter for full query
         */

        $qb = $this->em->createQueryBuilder();
        $qb->select('r, u, ui, s, c, ci, cat')
            ->from('TSGMoodleLMSBundle:SessionRoster', 'r')
            ->join('r.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->join('r.session', 's')
            ->join('s.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->join('c.category', 'cat');

        $in = array();
        foreach ($ids as $id) {
            $in[] = $id['id'];
        }

        $qb->where($qb->expr()->in('r.id', $in));
        $queryResult = $qb->getQuery()
            ->getResult();

        return $queryResult;
    }

    private function enrollmentsX(ParameterBag $params)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('r, u, ui, s, c, ci, cat')
            ->from('TSGMoodleLMSBundle:SessionRoster', 'r')
            ->join('r.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->leftJoin('u.groups', 'ug')
            ->leftJoin('u.tags', 'utag')
            ->join('r.session', 's')
            ->join('s.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->leftJoin('c.tags', 'ctag')
            ->join('c.category', 'cat');

        $this->logger->debug(__METHOD__ . ': built query');

        $this->filterForManagedGroups($qb, 'ug', $params);
        $this->addEnrollmentCriteria($params, $qb);

        $this->logger->debug(__METHOD__ . ': executing query');
        $enrollments = $qb->getQuery()
            ->getResult();

        $this->logger->debug(__METHOD__ . ': exit, returning ' . count($enrollments) . ' records');
        return $enrollments;
    }

    private function addEnrollmentCriteria(ParameterBag $params, &$qb)
    {
        $this->addQueryParam($qb, 'c.id', 'courseId', $params->get('courseId'));
        $this->addQueryParam($qb, 'ctag.id', 'ctagname', $params->get('tagId'));
        $this->addQueryParam($qb, 'utag.id', 'utagname', $params->get('tagId'));
        $this->addQueryParam($qb, 'u.id', 'userId', $params->get('userId'));

        $userGroupId = $params->get('userGroupId');
        $this->logger->debug('userGroupId = ' . $userGroupId);

        $includeSubGroups = $params->get('includeUserSubGroups');
        $this->logger->debug('includeSubGroups = ' . $includeSubGroups);

        if ($userGroupId != 0) {
            $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
            $selectedGroup = $userGroupRepo->find($params->get('userGroupId'));

            if ($includeSubGroups !== 'true') {
                $this->logger->debug('filtering for selected group');
                $qb->andWhere(':groupId member of u.groups');
                $qb->setParameter(':groupId', $selectedGroup);
            }
            else {
                $this->logger->debug('filtering for selected group\'s children');
                $subGroups = $selectedGroup->getChildren();

                $this->logger->debug('building expression');

                $x = '(';
                for ($i = 0; $i < count($subGroups); $i++) {
                    $sg = $subGroups[$i];
                    $this->logger->debug($sg->getName());

                    $x .= ':group_' . $i . ' member of u.groups';
                    if (($i + 1) < count($subGroups))
                        $x .= (' or ' . PHP_EOL);

                    $qb->setParameter(':group_' . $i, $sg);
                }
                $x .= ')';

                $qb->andWhere($x);
            }
        }

        $hiredWithin = $params->get('hiredWithin');
        if ($hiredWithin !== null && $hiredWithin !== 'false') {
            $hiredWithinDays = $params->get('hiredWithinDays');
            $today = mktime(0,0,0);
            $hireDate = $today - (24*60*60*$hiredWithinDays);
            $qb->andWhere($qb->expr()->gt('ui.hireDate', ':hireDate'));
            $qb->setParameter('hireDate', $hireDate);
        }

        $includeTerminated = $params->get('includedTerminated');
        if ($includeTerminated === 'false') {
            $qb->andWhere($qb->expr()->isNull('ui.terminationDate'));
        }

        $includeInactive = $params->get('includeInactive');
        if ($includeInactive === 'false') {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('ui.isActive', 1),
                $qb->expr()->isNull('ui.isActive')
            ));
        }

        $startDate = $params->get('startDate');
        $startDate = $startDate !== 'false' ? $startDate / 1000 : false;
        if ($startDate !== false) {
            $qb->andWhere($qb->expr()->gte('s.startDate', ':startDate'));
            $qb->setParameter('startDate', $startDate);
        }

        $endDate = $params->get('endDate');
        $endDate = $endDate !== 'false' ? ($endDate / 1000 + 60*60*24) : false;
        if ($endDate !== false) {
            $qb->andWhere($qb->expr()->gte('s.startDate', ':endDate'));
            $qb->setParameter('endDate', $startDate);
        }
    }


    private function addQueryParam(&$qb, $field, $name, $value)
    {
        if ($this->isParamPresent($value)) {
            $qb->andWhere($qb->expr()->eq($field, ':'.$name));
            $qb->setParameter($name, $value);
        }
    }

    private function isParamPresent($value)
    {
        return $value != 0 && $value != 'false';
    }


    /**
     * Attach profile group info to users.  This is a map of each profile group's id
     * to a sub-group name, where the sub-group is the one of which a user is a
     * member.  E.g. profile group "Department" (id = 1) and membership in the
     * "Accounting" sub-group of "Department" would give an entry in the map of:
     *     1 => "Accounting"
     *
     * @param array $records
     */
    private function attachProfileGroups(&$records)
    {
        $this->logger->debug(__METHOD__ . ': entry');

        if (empty($records)) {
            return;
        }

        $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:User');

        $userIds = array();
        foreach ($records as $r) {
            $userIds[] = $r['user_id'];
        }
        $userIds = array_unique($userIds, SORT_NUMERIC);
        $this->logger->debug('   ' . 'userIds: ' . print_r($userIds, true));

        $profileGroupMembership = $userGroupRepo->getProfileGroupMembershipForUsers($userIds);

        $this->logger->debug('    ' . 'processing ' . count($records) . ' records');
        foreach ($records as &$r) {
            $logmsg = '        ' . 'user id ' . $r['user_id'] . ', ';
            $r['user_profilegroups'] = array();

            $user = $profileGroupMembership[$r['user_id']];
            $logmsg .= ($user === null ? 'null' : get_class($user)) . ', ';
            if ($user === null) continue;

            $groups = $user->getGroups();
            $logmsg .= count($groups) . ' groups: ';
            $this->logger->debug($logmsg);

            if (empty($groups)) continue;

            foreach ($user->getGroups() as $g) {
                $parent = $g->getParent();
                $this->logger->debug('            ' . $parent[0]->getName() . ': ' . $g->getName());
                $r['user_profilegroups'][$parent[0]->getId()] = $g->getName();
            }
        }

        $this->logger->debug(__METHOD__ . ': exit');
    }


    public function match(Specification $specification)
    {
        return $this->doMatch($specification);
    }
    public function getAlias() { return 'r'; }
} 