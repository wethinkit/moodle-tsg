<?php

namespace TSG\MoodleLMSBundle\Entity\Pattern;


use Doctrine\ORM\Query\Expr;
use TSG\MoodleLMSBundle\Entity\UserFields;

class UserPattern
{
    /**
     * @var String
     */
    private $lastName;

    /**
     * @var String
     */
    private $firstName;


    /**
     * @param string $lastName
     * @param string|null $firstName
     */
    public function __construct($lastName, $firstName = null)
    {
        if (empty($lastName) || '' === trim($lastName)) {
            throw new \InvalidArgumentException('lastName cannot be empty');
        }

        $this->lastName = trim($lastName);
        $this->firstName = trim($firstName);
        $this->expr = new Expr();
    }

    public function where($alias)
    {
        $expr = new Expr();

        if (empty($this->firstName)) {
            return $expr->like($alias . '.lastName', $expr->literal($this->lastName . '%'));
        }
        else {
            return $expr->andX(
                $expr->like($alias . '.lastName', $expr->literal($this->lastName . '%')),
                $expr->like($alias . '.firstName', $expr->literal($this->firstName . '%'))
            );
        }
    }

    private function f($op, $fieldName, $value)
    {
        switch ($op) {
            case 'like':

        }
    }
}