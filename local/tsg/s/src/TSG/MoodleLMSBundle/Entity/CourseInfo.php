<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CourseInfo
 *
 * @ORM\Table(name="lsc_course_info")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\CourseInfoRepository")
 */
class CourseInfo implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Course
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="info")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     */
    private $course;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="hours", type="decimal")
     */
    private $hours;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="credits", type="decimal")
     */
    private $credits;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="cost_per_student", type="decimal")
     */
    private $costPerStudent;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set course
     *
     * @param \stdClass $course
     * @return CourseInfo
     */
    public function setCourse($course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \stdClass 
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set hours
     *
     * @param string $hours
     * @return CourseInfo
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return string 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set credits
     *
     * @param string $credits
     * @return CourseInfo
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * Get credits
     *
     * @return string 
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * Set costPerStudent
     *
     * @param string $costPerStudent
     * @return CourseInfo
     */
    public function setCostPerStudent($costPerStudent)
    {
        $this->costPerStudent = $costPerStudent;

        return $this;
    }

    /**
     * Get costPerStudent
     *
     * @return string 
     */
    public function getCostPerStudent()
    {
        return $this->costPerStudent;
    }

    public function getEntityName()
    {
        return 'CourseInfo';
    }

    public function toArray()
    {
        return array();
    }


}
