<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * SessionRoster
 *
 * @ORM\Table(name="lsc_course_section_enroll")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\SessionRosterRepository")
 */
class SessionRoster implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var CourseSession
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="CourseSession")
     * @ORM\JoinColumn(name="course_section_id", referencedColumnName="id")
     */
    private $session;

    /**
     * @var Course Assignment
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\OneToOne(targetEntity="CourseAssignment", inversedBy="sessionEnrollment")
     * @ORM\JoinColumn(name="course_assignment_id", referencedColumnName="id")
     */
    private $assignment;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="attended", type="smallint")
     */
    private $attended = 0;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="hours_attended", type="decimal", nullable=true)
     */
    private $hoursAttended;

    /**
     * @var Grade
     *
     * @Serializer\Groups({"associations"})
     */
    private $grade;


    public function __construct($id)
    {
        $this->id = $id;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     * @return SessionRoster
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set session
     *
     * @param \stdClass $session
     * @return SessionRoster
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return \stdClass 
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set assignment
     *
     * @param CourseAssignment $assignment
     * @return SessionRoster
     */
    public function setAssignment($assignment)
    {
        $this->assignment = $assignment;

        return $this;
    }

    /**
     * Get assignment
     *
     * @return \stdClass 
     */
    public function getAssignment()
    {
        return $this->assignment;
    }

    /**
     * Set attended
     *
     * @param integer $attended
     * @return SessionRoster
     */
    public function setAttended($attended)
    {
        $this->attended = $attended;

        return $this;
    }

    /**
     * Get attended
     *
     * @return integer
     */
    public function getAttended()
    {
        return $this->attended !== 0;
    }

    /**
     * Set hoursAttended
     *
     * @param string $hoursAttended
     * @return SessionRoster
     */
    public function setHoursAttended($hoursAttended)
    {
        $this->hoursAttended = $hoursAttended;

        return $this;
    }

    /**
     * Get hoursAttended
     *
     * @return string 
     */
    public function getHoursAttended()
    {
        return $this->hoursAttended;
    }



    /*
     * Non-persistent properties
     */


    /**
     * @return Grade
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param Grade $grade
     */
    public function setGrade(Grade $grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return float
     *
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"short"})
     */
    public function maxHours()
    {
        return $this->getSession()->getCourse()->getInfo()->getHours();
    }

    /**
     * @return bool
     *
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"short"})
     */
    public function isComplete()
    {
        $grade = $this->getGrade();
        if (!$grade) {
            return false;
        }

        return $grade->getFinalGrade() >= $grade->getItem()->getGradePass();
    }

    /**
     * @return bool|int
     *
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"short"})
     */
    public function completionDate()
    {
        $grade = $this->getGrade();
        return $grade ? $grade->getTimeModified() : false;
    }


    public function getEntityName()
    {
        return 'SessionRoster';
    }

    public function toArray()
    {
        return array();
    }
}
