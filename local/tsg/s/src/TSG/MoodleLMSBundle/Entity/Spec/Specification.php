<?php
namespace TSG\MoodleLMSBundle\Entity\Spec;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

interface Specification
{

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     *
     * @return \Doctrine\ORM\Query\Expr
     */
    public function match(QueryBuilder $qb, $alias);


    /**
     * @param Query $query
     */
    public function modifyQuery(Query $query);
}