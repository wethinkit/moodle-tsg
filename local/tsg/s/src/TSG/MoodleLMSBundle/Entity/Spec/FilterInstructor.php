<?php

namespace TSG\MoodleLMSBundle\Entity\Spec;


use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class FilterInstructor implements Specification
{
    private $pattern;

    public function __construct($pattern)
    {
        $this->pattern = $pattern;
    }

    public function match(QueryBuilder $qb, $alias)
    {
        $qb->setParameter('instructor', $this->pattern.'%');
        return $qb->expr()->like($alias . '.instructor', ':instructor');
    }

    public function modifyQuery(Query $query) { /* empty */ }
} 