<?php

namespace TSG\MoodleLMSBundle\Entity\Spec;


use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class Condition implements Specification
{
    protected $field;
    protected $value;

    public function __construct($field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    public function match(QueryBuilder $qb, $alias)
    {
        $x = $alias.'_'.$this->field;
        $qb->setParameter($x, $this->value);
        return $qb->expr()->like($alias.'.'.$this->field, ':'.$x);
    }

    public function modifyQuery(Query $query) { /* empty */ }
} 