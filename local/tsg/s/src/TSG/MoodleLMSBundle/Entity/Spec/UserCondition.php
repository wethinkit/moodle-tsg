<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 11/21/14
 * Time: 9:58 AM
 */

namespace TSG\MoodleLMSBundle\Entity\Spec;


use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class UserCondition extends Condition
{
    public function match(QueryBuilder $qb, $alias)
    {
        $x = $alias.'_'.$this->field;
        $qb->setParameter($x, $this->value);
        return $qb->expr()->like($alias.'.'.$this->field, ':'.$x);
    }

    public function modifyQuery(Query $query) { /* empty */ }
} 