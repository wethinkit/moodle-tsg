<?php

namespace TSG\MoodleLMSBundle\Entity\Spec;


use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class AndX implements Specification
{
    private $children;

    public function __construct()
    {
        $this->children = func_get_args();
    }

    public function match(QueryBuilder $qb, $alias)
    {
        return call_user_func_array(array($qb->expr(), 'andX'),
            array_map(function($specification) use ($qb, $alias) {
                return $specification->match($qb, $alias);
            }, $this->children)
        );
    }

    public function modifyQuery(Query $query)
    {
        foreach ($this->children as $c)
        {
            $c->modifyQuery($query);
        }
    }
} 