<?php

namespace TSG\MoodleLMSBundle\Entity\Spec;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;


class AsObject implements Specification
{
    private $root;

    public function __construct(Specification $root)
    {
        $this->root = $root;
    }

    public function modifyQuery(Query $query)
    {
        $query->setHydrationMode(Query::HYDRATE_OBJECT);
    }

    public function match(QueryBuilder $qb, $alias)
    {
        return $this->root->match($qb, $alias);
    }
} 