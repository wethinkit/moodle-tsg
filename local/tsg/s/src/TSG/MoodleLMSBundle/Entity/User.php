<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\UserRepository")
 */
class User implements Entity, \ArrayAccess
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var UserInfo[]
     *
     * @Serializer\Groups({"detailed", "associations"})
     * @ORM\OneToOne(targetEntity="UserInfo", mappedBy="user", cascade={"persist"})
     */
    private $info;

    /**
     * @var UserGroup[]
     *
     * @Serializer\Groups({"collections"})
     *
     * @ORM\ManyToMany(targetEntity="UserGroup", inversedBy="users")
     * @ORM\JoinTable(name="lsc_user_group_user",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn("group_id", referencedColumnName="id")})
     */
    private $groups;

    /**
     * @var UserGroup[]
     *
     * @Serializer\Groups({"collections"})
     *
     * @ORM\ManyToMany(targetEntity="UserGroup")
     * @ORM\JoinTable(name="lsc_user_allowed_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn("lsc_user_group_id", referencedColumnName="id")})
     */
    private $managedGroups;

    /**
     * @var Tag[]
     *
     * @Serializer\Groups({"collections"})
     *
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="lsc_user_training_area",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn("training_area_id", referencedColumnName="id")})
     */
    private $tags;



    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="auth", type="string", length=20)
     */
    private $auth;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="confirmed", type="smallint")
     */
    private $confirmed;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="policyAgreed", type="smallint")
     */
    private $policyAgreed;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="deleted", type="smallint")
     */
    private $deleted;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="suspended", type="smallint")
     */
    private $suspended;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="mnethostid", type="bigint")
     */
    private $mnethostid;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="userName", type="string", length=100)
     */
    private $userName;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="idnumber", type="string", length=255)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="firstName", type="string", length=100)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="lastName", type="string", length=100)
     */
    private $lastName;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="city", type="string", length=120)
     */
    private $city;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="country", type="string", length=2)
     */
    private $country;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="lang", type="string", length=30)
     */
    private $lang;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="theme", type="string", length=50)
     */
    private $theme;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="timeZone", type="string", length=100)
     */
    private $timeZone;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="timeCreated", type="bigint")
     */
    private $timeCreated;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="timeModified", type="bigint")
     */
    private $timeModified;


    public function __construct()
    {
        $this->groups        = new ArrayCollection();
        $this->managedGroups = new ArrayCollection();
        $this->tags          = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \TSG\MoodleLMSBundle\Entity\UserInfo[] $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return \TSG\MoodleLMSBundle\Entity\UserInfo[]
     */
    public function getInfo()
    {
        return $this->info;
    }


    /**
     * @param \TSG\MoodleLMSBundle\Entity\UserGroup $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return ArrayCollection of \TSG\MoodleLMSBundle\Entity\UserGroup
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param UserGroup $group
     */
    public function addGroup(UserGroup $group)
    {
        $this->groups->add($group);
    }

    /**
     * @param UserGroup $group
     */
    public function removeGroup(UserGroup $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * @return UserGroup[]
     */
    public function getManagedGroups()
    {
        return $this->managedGroups;
    }

    /**
     * @param UserGroup[] $managedGroups
     */
    public function setManagedGroups($managedGroups)
    {
        $this->managedGroups = $managedGroups;
    }


    /**
     * @param \TSG\MoodleLMSBundle\Entity\Tag[] $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return \TSG\MoodleLMSBundle\Entity\Tag
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param \TSG\MoodleLMSBundle\Entity\Tag $tag
     */
    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);
    }

    /**
     * Set auth
     *
     * @param string $auth
     * @return User
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * Get auth
     *
     * @return string 
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * Set confirmed
     *
     * @param integer $confirmed
     * @return User
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return integer 
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Set policyAgreed
     *
     * @param integer $policyAgreed
     * @return User
     */
    public function setPolicyAgreed($policyAgreed)
    {
        $this->policyAgreed = $policyAgreed;

        return $this;
    }

    /**
     * Get policyAgreed
     *
     * @return integer 
     */
    public function getPolicyAgreed()
    {
        return $this->policyAgreed;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set suspended
     *
     * @param integer $suspended
     * @return User
     */
    public function setSuspended($suspended)
    {
        $this->suspended = $suspended;

        return $this;
    }

    /**
     * Get suspended
     *
     * @return integer 
     */
    public function getSuspended()
    {
        return $this->suspended;
    }

    /**
     * Set mnethostid
     *
     * @param integer $mnethostid
     * @return User
     */
    public function setMnethostid($mnethostid)
    {
        $this->mnethostid = $mnethostid;

        return $this;
    }

    /**
     * Get mnethostid
     *
     * @return integer 
     */
    public function getMnethostid()
    {
        return $this->mnethostid;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return User
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set idnumber
     *
     * @param string $idnumber
     * @return User
     */
    public function setIdnumber($idnumber)
    {
        $this->idnumber = $idnumber;

        return $this;
    }

    /**
     * Get idnumber
     *
     * @return string 
     */
    public function getIdnumber()
    {
        return $this->idnumber;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return User
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string 
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return User
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set timeZone
     *
     * @param string $timeZone
     * @return User
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * Get timeZone
     *
     * @return string 
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * Set timeCreated
     *
     * @param integer $timeCreated
     * @return User
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return integer 
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * Set timeModified
     *
     * @param integer $timeModified
     * @return User
     */
    public function setTimeModified($timeModified)
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    /**
     * Get timeModified
     *
     * @return integer 
     */
    public function getTimeModified()
    {
        return $this->timeModified;
    }

    public function getEntityName()
    {
        return "User";
    }

    public function toArray()
    {
        return array();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        // TODO: Finish adding properties.
        return in_array($offset, array('id'));
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        // TODO: Finish adding properties.
        switch ($offset) {
            case 'id':
                return $this->getId();
            default:
                return null;
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }


}
