<?php

namespace TSG\MoodleLMSBundle\Entity;

use TSG\MoodleLMSBundle\Entity\Spec\Specification;

/**
 * CourseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CourseRepository extends BaseRepository
{
    public function match(Specification $specification)
    {
        return $this->doMatch($specification);
    }

    public function getAlias() { return 'c'; }

    public function findByPattern($pattern, $orderBy = null, $limit = null, $offset = null)
    {
        $c = $this->getAlias();
        $qb = $this->createQueryBuilder($c);
        $qb->where($qb->expr()->like($c.'.fullName', $qb->expr()->literal($pattern.'%')));

        if ($orderBy !== null) {
            $orderBy = explode(' ', $orderBy);
            $qb->orderBy($c.'.'.trim($orderBy[0]), count($orderBy) > 1 ? $orderBy[1] : 'asc');
        }

        $query = $qb->getQuery();

        if ($offset !== null) {
            $query->setFirstResult($offset);
        }

        if ($limit !== null) {
            $query->setMaxResults($limit);
        }

        return $query->getResult();
    }
}
