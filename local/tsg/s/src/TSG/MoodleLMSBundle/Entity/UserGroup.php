<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserGroup
 *
 * @ORM\Table(name="lsc_user_group")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\UserGroupRepository")
 */
class UserGroup implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var self[]
     *
     * @Serializer\Groups({"parents"})
     * @ORM\ManyToMany(targetEntity="UserGroup", inversedBy="children")
     * @ORM\JoinTable(name="lsc_user_group_group",
     *      joinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")})
     */
    private $parent;

    /**
     * @var self[]
     *
     * @Serializer\Groups({"children"})
     * @ORM\ManyToMany(targetEntity="UserGroup", mappedBy="parent")
     * @ORM\OrderBy({"name" = "ASC"})
     *
     */
    private $children;


    /**
     * @var User[]
     *
     * @Serializer\Groups({"collections"})
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     * @ORM\OrderBy({"lastName" = "asc", "firstName" = "asc"})
     */
    private $users;

    /**
     * @var User[]
     *
     * @Serializer\Groups({"collections"})
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\OrderBy({"lastName" = "asc", "firstName" = "asc"})
     * @ORM\JoinTable(name="lsc_user_allowed_groups",
     *      joinColumns={@ORM\JoinColumn(name="lsc_user_group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")})
     */
    private $managers;

    /**
     * @var int
     *
     * @Serializer\Groups({"short"})
     */
    private $level = 0;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="isProfileGroup", type="boolean")
     */
    private $isProfileGroup;


    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->parent = new ArrayCollection();
        $this->managers = new ArrayCollection();
        $this->users = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \TSG\MoodleLMSBundle\Entity\UserGroup $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return boolean
     */
    public function hasChildren()
    {
        return count($this->getChildren()) > 0;
    }

    /**
     * @return ArrayCollection|User[]
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * @param ArrayCollection $managers collection of users who manage this group
     */
    public function setManagers(ArrayCollection $managers)
    {
        $this->managers = $managers;
    }

    /**
     * @param \TSG\MoodleLMSBundle\Entity\User[] $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return \TSG\MoodleLMSBundle\Entity\User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }



    /**
     * Set name
     *
     * @param string $name
     * @return UserGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isProfileGroup
     *
     * @param boolean $isProfileGroup
     * @return UserGroup
     */
    public function setIsProfileGroup($isProfileGroup)
    {
        $this->isProfileGroup = $isProfileGroup;

        return $this;
    }

    /**
     * Get isProfileGroup
     *
     * @return boolean 
     */
    public function isProfileGroup()
    {
        return $this->isProfileGroup;
    }


    public function getEntityName()
    {
        return "UserGroup";
    }

    public function isRoot()
    {
        return $this->getName() == 'root';
    }

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'level' => $this->getLevel(),
            'name' => $this->getName()
        );
    }
}
