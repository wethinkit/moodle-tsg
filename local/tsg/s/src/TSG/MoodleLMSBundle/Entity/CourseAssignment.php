<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CourseAssignment
 *
 * @ORM\Table(name="lsc_course_assignment")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\CourseAssignmentRepository")
 */
class CourseAssignment implements Entity, \ArrayAccess
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Course
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     */
    private $course;

    /**
     * @var SessionRoster
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\OneToOne(targetEntity="SessionRoster", mappedBy="assignment")
     */
    private $sessionEnrollment;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="deadline", type="bigint")
     */
    private $deadline;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="reason", type="string", length=255)
     */
    private $reason;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="repeat_interval", type="bigint")
     */
    private $repeatInterval;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="repeat_period", type="string", length=20)
     */
    private $repeatPeriod;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="repeat_processed", type="bigint")
     */
    private $repeatProcessed;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="source", type="string", length=100)
     */
    private $source;

    /**
     * @var self
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\OneToOne(targetEntity="CourseAssignment")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    private $grades;

    /**
     * @var bool
     *
     * @Serializer\Groups({"short"})
     */
    private $isComplete;

    /**
     * @var int unix time stamp of completion date
     *
     * @Serializer\Groups({"short"})
     */
    private $completionDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return CourseAssignment
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set course
     *
     * @param \stdClass $course
     * @return CourseAssignment
     */
    public function setCourse($course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \stdClass 
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Get sessionEnrollment
     *
     * @return SessionRoster
     */
    public function getSessionEnrollment()
    {
        return $this->sessionEnrollment;
    }

    /**
     * Set sessionEnrollment
     *
     * @param SessionRoster $value
     * @return self
     */
    public function setSessionEnrollment($value)
    {
        $this->sessionEnrollment = $value;
        return $this;
    }

    /**
     * Set deadline
     *
     * @param integer $deadline
     * @return CourseAssignment
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return integer 
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return CourseAssignment
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set repeatInterval
     *
     * @param integer $repeatInterval
     * @return CourseAssignment
     */
    public function setRepeatInterval($repeatInterval)
    {
        $this->repeatInterval = $repeatInterval;

        return $this;
    }

    /**
     * Get repeatInterval
     *
     * @return integer 
     */
    public function getRepeatInterval()
    {
        return $this->repeatInterval;
    }

    /**
     * Set repeatPeriod
     *
     * @param string $repeatPeriod
     * @return CourseAssignment
     */
    public function setRepeatPeriod($repeatPeriod)
    {
        $this->repeatPeriod = $repeatPeriod;

        return $this;
    }

    /**
     * Get repeatPeriod
     *
     * @return string 
     */
    public function getRepeatPeriod()
    {
        return $this->repeatPeriod;
    }

    /**
     * Set repeatProcessed
     *
     * @param integer $repeatProcessed
     * @return CourseAssignment
     */
    public function setRepeatProcessed($repeatProcessed)
    {
        $this->repeatProcessed = $repeatProcessed;

        return $this;
    }

    /**
     * Get repeatProcessed
     *
     * @return integer 
     */
    public function getRepeatProcessed()
    {
        return $this->repeatProcessed;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return CourseAssignment
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set parent
     *
     * @param \stdClass $parent
     * @return CourseAssignment
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \stdClass 
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getEntityName()
    {
        return 'CourseAssignment';
    }

    public function toArray()
    {
        return array();
    }

    /**
     * @return mixed
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * @param mixed $grades
     */
    public function setGrades($grades)
    {
        $this->grades = $grades;
    }



    /**
     * Get isComplete
     *
     * @return bool
     */
    public function isComplete()
    {
        return $this->isComplete;
    }

    public function setComplete($trueOrFalse)
    {
        $this->isComplete = $trueOrFalse;
        if (!$trueOrFalse) {
            $this->setCompletionDate(null);
        }
    }

    /**
     * Get completionDate
     *
     * @return int
     */
    public function getCompletionDate()
    {
        return $this->completionDate;
    }

    /**
     * Set completionDate
     *
     * @param $value int
     */
    public function setCompletionDate($value)
    {
        $this->isComplete = $value ? true : false;
        $this->completionDate = $value;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return in_array($offset, array('user', 'course'));
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        switch ($offset) {
            case 'user':
                return $this->getUser();
            case 'course':
                return $this->getCourse();
            default:
                return null;
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }


}
