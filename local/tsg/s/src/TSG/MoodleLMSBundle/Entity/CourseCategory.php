<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CourseCategory
 *
 * @ORM\Table(name="course_categories")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\CourseCategoryRepository")
 */
class CourseCategory implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Serializer\Groups({"collections"})
     *
     * @ORM\OneToMany(targetEntity="CourseCategory", mappedBy="parent")
     **/
    private $children;

    /**
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="CourseCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     **/
    private $parent;

    /**
     * @var \TSG\MoodleLMSBundle\Entity\Course[]
     * @Serializer\Groups({"collections"})
     * @ORM\OneToMany(targetEntity="Course", mappedBy="category")
     */
    private $courses;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="idnumber", type="string", length=100)
     */
    private $idnumber;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="descriptionFormat", type="smallint")
     */
    private $descriptionFormat;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="sortOrder", type="bigint")
     */
    private $sortOrder;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="courseCount", type="bigint")
     */
    private $courseCount;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="visible", type="smallint")
     */
    private $visible;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="timeModified", type="bigint")
     */
    private $timeModified;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="depth", type="bigint")
     */
    private $depth;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="theme", type="string", length=50)
     */
    private $theme;


    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Course[]
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * @param Course[] $courses
     */
    public function setCourses($courses)
    {
        $this->courses = $courses;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CourseCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idnumber
     *
     * @param string $idnumber
     * @return CourseCategory
     */
    public function setIdnumber($idnumber)
    {
        $this->idnumber = $idnumber;

        return $this;
    }

    /**
     * Get idnumber
     *
     * @return string 
     */
    public function getIdnumber()
    {
        return $this->idnumber;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CourseCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionFormat
     *
     * @param integer $descriptionFormat
     * @return CourseCategory
     */
    public function setDescriptionFormat($descriptionFormat)
    {
        $this->descriptionFormat = $descriptionFormat;

        return $this;
    }

    /**
     * Get descriptionFormat
     *
     * @return integer 
     */
    public function getDescriptionFormat()
    {
        return $this->descriptionFormat;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return CourseCategory
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set courseCount
     *
     * @param integer $courseCount
     * @return CourseCategory
     */
    public function setCourseCount($courseCount)
    {
        $this->courseCount = $courseCount;

        return $this;
    }

    /**
     * Get courseCount
     *
     * @return integer 
     */
    public function getCourseCount()
    {
        return $this->courseCount;
    }

    /**
     * Set visible
     *
     * @param integer $visible
     * @return CourseCategory
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return integer 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set timeModified
     *
     * @param integer $timeModified
     * @return CourseCategory
     */
    public function setTimeModified($timeModified)
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    /**
     * Get timeModified
     *
     * @return integer 
     */
    public function getTimeModified()
    {
        return $this->timeModified;
    }

    /**
     * Set depth
     *
     * @param integer $depth
     * @return CourseCategory
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get depth
     *
     * @return integer 
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return CourseCategory
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return CourseCategory
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    public function getEntityName()
    {
        return 'CourseCategory';
    }

    public function toArray()
    {
        return array();
    }


}
