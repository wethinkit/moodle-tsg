<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Grade
 *
 * @ORM\Table(name="grade_grades")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\GradeRepository")
 */
class Grade implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var GradeItem
     *
     * * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="GradeItem")
     * @ORM\JoinColumn(name="itemid", referencedColumnName="id")
     */
    private $item;

    /**
     * @var User
     *
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userid", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="rawGrade", type="decimal")
     */
    private $rawGrade;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="rawGradeMax", type="decimal")
     */
    private $rawGradeMax;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="rawGradeMin", type="decimal")
     */
    private $rawGradeMin;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="finalGrade", type="decimal")
     */
    private $finalGrade;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="timeCreated", type="bigint")
     */
    private $timeCreated;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="timeModified", type="bigint")
     */
    private $timeModified;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get item
     *
     * @return GradeItem
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set item
     *
     * @param GradeItem
     * @return Grade
     */
    public function setItem(GradeItem $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Grade
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Set rawGrade
     *
     * @param string $rawGrade
     * @return Grade
     */
    public function setRawGrade($rawGrade)
    {
        $this->rawGrade = $rawGrade;

        return $this;
    }

    /**
     * Get rawGrade
     *
     * @return string 
     */
    public function getRawGrade()
    {
        return $this->rawGrade;
    }

    /**
     * Set rawGradeMax
     *
     * @param string $rawGradeMax
     * @return Grade
     */
    public function setRawGradeMax($rawGradeMax)
    {
        $this->rawGradeMax = $rawGradeMax;

        return $this;
    }

    /**
     * Get rawGradeMax
     *
     * @return string 
     */
    public function getRawGradeMax()
    {
        return $this->rawGradeMax;
    }

    /**
     * Set rawGradeMin
     *
     * @param string $rawGradeMin
     * @return Grade
     */
    public function setRawGradeMin($rawGradeMin)
    {
        $this->rawGradeMin = $rawGradeMin;

        return $this;
    }

    /**
     * Get rawGradeMin
     *
     * @return string 
     */
    public function getRawGradeMin()
    {
        return $this->rawGradeMin;
    }

    /**
     * Set finalGrade
     *
     * @param string $finalGrade
     * @return Grade
     */
    public function setFinalGrade($finalGrade)
    {
        $this->finalGrade = $finalGrade;

        return $this;
    }

    /**
     * Get finalGrade
     *
     * @return string 
     */
    public function getFinalGrade()
    {
        return $this->finalGrade;
    }

    /**
     * Set timeCreated
     *
     * @param integer $timeCreated
     * @return Grade
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return integer 
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * Set timeModified
     *
     * @param integer $timeModified
     * @return Grade
     */
    public function setTimeModified($timeModified)
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    /**
     * Get timeModified
     *
     * @return integer 
     */
    public function getTimeModified()
    {
        return $this->timeModified;
    }


    public function getEntityName()
    {
        return "Grade";
    }

    public function toArray()
    {
        return array();
    }
}
