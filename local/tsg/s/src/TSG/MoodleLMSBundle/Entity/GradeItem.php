<?php

namespace TSG\MoodleLMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * GradeItem
 *
 * @ORM\Table(name="grade_items")
 * @ORM\Entity(repositoryClass="TSG\MoodleLMSBundle\Entity\GradeItemRepository")
 */
class GradeItem implements Entity
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Serializer\Groups({"associations"})
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumn(name="courseid", referencedColumnName="id")
     */
    private $course;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="itemName", type="string", length=255)
     */
    private $itemName;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="itemType", type="string", length=30)
     */
    private $itemType;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="itemModule", type="string", length=30)
     */
    private $itemModule;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="itemInstance", type="bigint")
     */
    private $itemInstance;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="itemNumber", type="bigint")
     */
    private $itemNumber;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="idNumber", type="string", length=255)
     */
    private $idNumber;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="gradeType", type="smallint")
     */
    private $gradeType;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="gradeMax", type="decimal")
     */
    private $gradeMax;

    /**
     * @var string
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="gradeMin", type="decimal")
     */
    private $gradeMin;

    /**
     * @var string
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="gradePass", type="decimal")
     */
    private $gradePass;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="sortOrder", type="bigint")
     */
    private $sortOrder;

    /**
     * @var integer
     *
     * @Serializer\Groups({"detailed"})
     *
     * @ORM\Column(name="timeCreated", type="bigint")
     */
    private $timeCreated;

    /**
     * @var integer
     *
     * @Serializer\Groups({"short"})
     *
     * @ORM\Column(name="timeModified", type="bigint")
     */
    private $timeModified;

    /**
     * @var CourseSession
     *
     * @Serializer\Groups({"associations"})
     */
    private $session;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }



    /**
     * Set itemName
     *
     * @param string $itemName
     * @return GradeItem
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string 
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Set itemType
     *
     * @param string $itemType
     * @return GradeItem
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get itemType
     *
     * @return string 
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * Set itemModule
     *
     * @param string $itemModule
     * @return GradeItem
     */
    public function setItemModule($itemModule)
    {
        $this->itemModule = $itemModule;

        return $this;
    }

    /**
     * Get itemModule
     *
     * @return string 
     */
    public function getItemModule()
    {
        return $this->itemModule;
    }

    /**
     * Set itemInstance
     *
     * @param integer $itemInstance
     * @return GradeItem
     */
    public function setItemInstance($itemInstance)
    {
        $this->itemInstance = $itemInstance;

        return $this;
    }

    /**
     * Get itemInstance
     *
     * @return integer 
     */
    public function getItemInstance()
    {
        return $this->itemInstance;
    }

    /**
     * Set itemNumber
     *
     * @param integer $itemNumber
     * @return GradeItem
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return integer 
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set idNumber
     *
     * @param string $idNumber
     * @return GradeItem
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * Get idNumber
     *
     * @return string 
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * Set gradeType
     *
     * @param integer $gradeType
     * @return GradeItem
     */
    public function setGradeType($gradeType)
    {
        $this->gradeType = $gradeType;

        return $this;
    }

    /**
     * Get gradeType
     *
     * @return integer 
     */
    public function getGradeType()
    {
        return $this->gradeType;
    }

    /**
     * Set gradeMax
     *
     * @param string $gradeMax
     * @return GradeItem
     */
    public function setGradeMax($gradeMax)
    {
        $this->gradeMax = $gradeMax;

        return $this;
    }

    /**
     * Get gradeMax
     *
     * @return string 
     */
    public function getGradeMax()
    {
        return $this->gradeMax;
    }

    /**
     * Set gradeMin
     *
     * @param string $gradeMin
     * @return GradeItem
     */
    public function setGradeMin($gradeMin)
    {
        $this->gradeMin = $gradeMin;

        return $this;
    }

    /**
     * Get gradeMin
     *
     * @return string 
     */
    public function getGradeMin()
    {
        return $this->gradeMin;
    }

    /**
     * Set gradePass
     *
     * @param string $gradePass
     * @return GradeItem
     */
    public function setGradePass($gradePass)
    {
        $this->gradePass = $gradePass;

        return $this;
    }

    /**
     * Get gradePass
     *
     * @return string 
     */
    public function getGradePass()
    {
        return $this->gradePass;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return GradeItem
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set timeCreated
     *
     * @param integer $timeCreated
     * @return GradeItem
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return integer 
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * Set timeModified
     *
     * @param integer $timeModified
     * @return GradeItem
     */
    public function setTimeModified($timeModified)
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    /**
     * Get timeModified
     *
     * @return integer 
     */
    public function getTimeModified()
    {
        return $this->timeModified;
    }

    public function getEntityName()
    {
        return 'GradeItem';
    }

    public function toArray()
    {
        return array();
    }

    /**
     * @return CourseSession
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param CourseSession $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }


}
