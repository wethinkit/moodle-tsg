<?php

namespace TSG\MoodleLMSBundle\Entity;


use Doctrine\ORM\EntityRepository;
use TSG\MoodleLMSBundle\Entity\Spec\Specification;

abstract class BaseRepository extends EntityRepository
{
    abstract public function getAlias();
    abstract public function match(Specification $specification);

    protected function doMatch(Specification $specification)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb = $qb->select($this->getAlias())
            ->from($this->_entityName, $this->getAlias());

        $query = $qb->where($specification->match($qb, $this->getAlias()))->getQuery();
        $specification->modifyQuery($query);

        return $query->getResult();
    }

    /**
     * Converts a collection of Entities to a collection of arrays
     *
     * @param Entity[] $entities
     *
     * @return array[] array of array's with each element containing a hash of
     *                 the properties of the given Entity
     */
    public function toArrays($entities) {
        $array = array();
        foreach ($entities as $e) {
            $array[] = $e->toArray();
        }
        return $array;
    }
} 