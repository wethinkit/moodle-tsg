<?php


namespace TSG\MoodleLMSBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TSG\MoodleLMSBundle\Entity\Pattern\UserPattern;
use TSG\MoodleLMSBundle\Entity\UserFields;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;


class UserController extends BaseController
{
    /**
     * @View()
     */
    public function findByPatternAction(Request $request)
    {
        $pattern = $request->query->get('pattern');

        global $CFG;
        require_once($CFG->dirroot . LOCAL_TSG_ROOT . '/locallib.php');

        $filterForManagedGroupsOfUserId = false;
        if (!local_tsg_isadmin()) {
            global $USER;
            $filterForManagedGroupsOfUserId = $USER->id;
        }

        $pattern = explode(',', $pattern);
        $pattern = count($pattern) < 2 ?
            new UserPattern($pattern[0]) : new UserPattern($pattern[0], $pattern[1]);


        $repo = $this->em->getRepository('TSGMoodleLMSBundle:User');
        $users = $repo->findByPattern($pattern, $filterForManagedGroupsOfUserId, 'lastName', 15, 0);

        return new EntitiesContainer($users, array('short'),
            $this->router->generate('tsg_moodle_lms.user.findByPattern'));
    }

    public function findByPatternUsingAccelerator(Request $request)
    {
        require_login();

        $service = new \UserService();
        $content = $service->findByPattern($_REQUEST['pattern']);

        return new JsonResponse($content);
    }
} 