<?php

namespace TSG\MoodleLMSBundle\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TSG\MoodleLMSBundle\Entity\CourseSession;
use TSG\MoodleLMSBundle\Entity\SessionRoster;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;
use FOS\RestBundle\Controller\Annotations\View;
use TSG\MoodleLMSBundle\HAL\Container\SingleItemContainer;
use TSG\MoodleLMSBundle\HAL\Serialization\MaxDepthExclusionStrategy;

class CourseSessionController extends BaseController
{

    public function indexAction($courseid)
    {
        $courseRepo = $this->em->getRepository('TSGMoodleLMSBundle:Course');
        $course = $courseRepo->find($courseid);

        $output = $this->moodleGlobals->getOutput();
        $page = $this->moodleGlobals->getPage();

        $page->set_context(\context_system::instance());

        $params = array(array(
            'courseId' => $courseid,
            'urls' => array(
                'course' => $this->urlService->course($courseid),
                'session' => $this->urlService->session()
            )
        ));
        $page->requires->yui_module('moodle-local_tsg-sessions',
            'M.local_tsg.sessions.init', $params);

        $page->set_title('Sessions');
        // $page->set_heading('Assign Users to Course');
        $page->set_cacheable(false);
        $page->navbar->add($course->getFullName(), new \moodle_url('/course/view.php', array('id' => $course->getId())));
        $page->navbar->add('Sessions');

        $content  = $output->header();
        $content .= $this->templating->render('TSGMoodleLMSBundle:sessions:index.html.twig');
        $content .= $output->footer();

        return new Response($content);
    }

    /**
     * @View
     *
     * @param $courseid
     * @return EntitiesContainer
     */
    public function fetchSessionsForCourseAction($courseid)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('s')
            ->from('TSGMoodleLMSBundle:CourseSession', 's')
            ->join('s.course', 'c')
            ->where($qb->expr()->eq('c.id', ':courseid'))
            ->orderBy('s.startDate', 'desc');

        $qb->setParameter(':courseid', $courseid);

        return new EntitiesContainer($qb->getQuery()->getResult(), array('short'), array(
            'new' => $this->router->generate('tsg_moodle_lms.session.insert')
        ));
    }


    /**
     * @View
     *
     * @param Request $request
     * @return SingleItemContainer
     */
    public function insertAction(Request $request)
    {
        $toInsert = json_decode($request->getContent());

        $session = new CourseSession();

        $course = $this->em->find('TSGMoodleLMSBundle:Course', $toInsert->courseId);
        $session->setCourse($course);

        $startDate = new \DateTime($toInsert->startDate);
        $session->setStartDate($startDate->getTimestamp());

        $endDate = new \DateTime($toInsert->endDate);
        $session->setEndDate($endDate->getTimestamp());

        $session->setSchedule($toInsert->schedule);
        $session->setLocation($toInsert->location);
        $session->setCapacity($toInsert->capacity);
        $session->setInstructor($toInsert->instructor);

        $this->em->persist($session);
        $this->em->flush();

        return new SingleItemContainer($session, array('short'));
    }


    /**
     * @View
     *
     * @param $id
     * @param Request $request
     * @return SingleItemContainer
     */
    public function updateAction($id, Request $request)
    {
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:CourseSession');
        $session = $repo->find($id);

        $toUpdate = json_decode($request->getContent());

        $startDate = new \DateTime($toUpdate->startDate);
        $session->setStartDate($startDate->getTimestamp());

        $endDate = new \DateTime($toUpdate->endDate);
        $session->setEndDate($endDate->getTimestamp());

        $session->setSchedule($toUpdate->schedule);
        $session->setLocation($toUpdate->location);
        $session->setCapacity($toUpdate->capacity);
        $session->setInstructor($toUpdate->instructor);

        $this->em->flush();

        return new SingleItemContainer($session, array('short'));
    }


    /**
     * @View
     *
     * @param $id
     * @return SingleItemContainer
     */
    public function deleteAction($id)
    {
        $deleted = new \stdClass;
        $deleted->id = $id;

        $toDelete = $this->em->find('TSGMoodleLMSBundle:CourseSession', $id);
        $this->em->remove($toDelete);
        $this->em->flush();

        return new SingleItemContainer($deleted, array('short'));
    }


    /**
     * @View
     *
     * @param $sessionid
     * @return EntitiesContainer
     */
    public function fetchRosterAction($sessionid)
    {
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:CourseSession');
        $roster = $repo->rosterWithCompletionInfo($sessionid);
        return new EntitiesContainer($roster, array('short','associations'), array(
            'update' => $this->router->generate('tsg_moodle_lms.roster.update', array(
                'sessionid' => $sessionid
            ))
        ), false, false, new MaxDepthExclusionStrategy(4, $this->logger));
    }


    /**
     * @View
     *
     * @param int $sessionid
     * @param Request $request
     *
     * @return EntitiesContainer
     */
    public function updateRosterAction($sessionid, Request $request)
    {
        $rosterEntryRepo = $this->em->getRepository('TSGMoodleLMSBundle:SessionRoster');
        $sessionRepo = $this->em->getRepository('TSGMoodleLMSBundle:CourseSession');

        /*
         * Build a hash of entries in request keyed by SessionRoster.id.
         */

        $requestEntries = json_decode($request->getContent());
        $ids = array_map(function($r) { return $r->id; }, $requestEntries);
        $requestEntries = array_combine($ids, $requestEntries);


        /*
         * Fetch the current SessionRoster entries and update them
         * with info from $requestEntries.
         */

        $entries = $rosterEntryRepo->findByIds($ids);

        foreach ($entries as $e) {
            $re = $requestEntries[$e->getId()];
            $e->setAttended($re->attended ? 1 : 0);
            $e->setHoursAttended($re->hours_attended);

            $rosterEntryRepo->setCompletionStatus($e, $re->is_complete);
        }


        /*
         * Flush to the db and return the updated roster
         */

        $this->em->flush();

        return new EntitiesContainer($sessionRepo->rosterWithCompletionInfo($sessionid),
            array('short', 'associations'), array(
                'update' => $this->router->generate('tsg_moodle_lms.roster.update', array(
                    'sessionid' => $sessionid
                ))
            ), false, false, new MaxDepthExclusionStrategy(4, $this->logger)
        );
    }
}