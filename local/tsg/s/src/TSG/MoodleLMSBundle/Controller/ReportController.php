<?php
namespace TSG\MoodleLMSBundle\Controller;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\View;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;
use TSG\MoodleLMSBundle\HAL\Container\RecordsContainer;

class ReportController extends BaseController
{
    private $reportRepository;

    public function setReportRepository($reportRepository)
    {
        $this->reportRepository = $reportRepository;;
    }

    public function indexAction()
    {
        // require_login();
        $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
        $userGroups = $userGroupRepo->getCompleteHierarchy(true);
        $userGroups = $userGroupRepo->toArrays($userGroups);

        $profileGroups = $userGroupRepo->getProfileGroups();
        $profileGroups = $userGroupRepo->toArrays($profileGroups);

        $output = $this->moodleGlobals->getOutput();
        $page = $this->moodleGlobals->getPage();

        $page->set_context(\context_system::instance());

        $params = array(array(
            'urls' => array(
                'report' => array(
                    'run' => $this->router->generate('tsg_moodle_lms.report.run') .
                        '?first=0&max=25'
                ),
                'course' => array(
                    'findByPattern' => $this->router->generate('tsg_moodle_lms.course.findByPattern')
                ),
                'tag' => array(
                    'findByPattern' => $this->router->generate('tsg_moodle_lms.tag.findByPattern')
                ),
                'user' => $this->urlService->user()
            ),
            'userGroups' => $userGroups,
            'profileGroups' => $profileGroups
        ));
        $page->requires->yui_module('moodle-local_tsg-report', 'M.local_tsg.report.init', $params);

        $page->set_title('Reports');
        $page->set_heading('Reports');
        $page->set_cacheable(false);
        $page->navbar->add('Reports');

        $content  = $output->header();
        $content .= $this->templating->render('TSGMoodleLMSBundle:report:index.html.twig');
        $content .= $output->footer();

        return new Response($content);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \TSG\MoodleLMSBundle\HAL\Container\Container
     *
     * @View()
     */
    public function runAction(Request $request)
    {
        $query = $request->query;
        $params = $request->request;

        global $CFG;
        require_once($CFG->dirroot . LOCAL_TSG_ROOT . '/locallib.php');
        if (!local_tsg_isadmin()) {
            global $USER;
            $params->set('filterForManagedGroupsOfUserId', $USER->id);
        }

        $this->logger->debug(__METHOD__ . ':  checking accept header');
        $contentType = 'text/csv';
        $accept = $request->getAcceptableContentTypes();
        $this->logger->debug(__METHOD__ . ': accept = ' . print_r($accept, true));
        foreach ($accept as $a) {
            if ($a == 'application/hal+json') {
                $contentType = $a;
                break;
            }
        }
        $this->logger->debug(__METHOD__ . ':  response content type will be ' . $contentType);

        $first = $contentType !== 'text/csv' ? $query->get('first') : 0;
        $max = $contentType !== 'text/csv' ? $query->get('max') : 0;
        
        $serializerGroups = array('short','associations','collections');
        $route = $this->router->generate('tsg_moodle_lms.report.run');

        switch ($params->get('reportType')) {
            case 'completions':
                $records = $this->completions($params, $first, $max);
                break;
            case 'assignments':
                $records = $this->assignments($params, $first, $max);
                break;
            case 'enrollments':
                $records = $this->enrollments($params, $first, $max);
                break;
            default:
                $records = array();
                break;
        }

        switch ($contentType) {
            case 'text/csv':
                $csv = $this->csv($records, $params);
                $this->logger->debug(__METHOD__ . ': returning CSV Response');
                return new Response($csv, 200, array(
                    'Content-Type' => 'text/csv',
                    'Content-Disposition' => 'attachment; filename="report.csv"'
                ));
            default:
                $this->logger->debug(__METHOD__ . ': returning RecordsContainer');
                return new RecordsContainer(
                    $records,
                    $serializerGroups,
                    $route,
                    $first,
                    $max
                );
        }
    }

    private function csv($records, ParameterBag $params)
    {
        $delimiter = ',';
        $enclosure = '"';

        // Field Names
        $headers = array();
        $headers[] = $this->csv_encode('Course Name', $enclosure);
        $headers[] = $this->csv_encode('Category', $enclosure);
        $headers[] = $this->csv_encode('Last Name', $enclosure);
        $headers[] = $this->csv_encode('First Name', $enclosure);
        $headers[] = $this->csv_encode('Email', $enclosure);

        $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
        $selectedOptionalCols = array();
        foreach ($params as $key => $value) {
            $this->logger->debug(__METHOD__ . ': $key = ' . $key);
            if (strpos($key, 'optionalCol_id=') !== 0) {
                continue;
            }


            $id = substr($key, 15); // $key to the right of 'optionalCol_id='
            $group = $userGroupRepo->find($id);
            $headers[] = $this->csv_encode($group->getName(), $enclosure);
            $selectedOptionalCols[] = $group;
        }

        $headers[] = $this->csv_encode('Type', $enclosure);
        // $headers[] = $this->csv_encode('Status', $enclosure);
        $headers[] = $this->csv_encode('Deadline', $enclosure);
        $headers[] = $this->csv_encode('Enrollment', $enclosure);
        $headers[] = $this->csv_encode('Attended', $enclosure);
        $headers[] = $this->csv_encode('Hours Attended', $enclosure);
        $headers[] = $this->csv_encode('Grade', $enclosure);
        $headers[] = $this->csv_encode('Completion Date', $enclosure);
        $headers[] = $this->csv_encode('Hours', $enclosure);
        $headers[] = $this->csv_encode('Credits', $enclosure);
        $headers[] = $this->csv_encode('Cost (per student)', $enclosure);
        $headers[] = $this->csv_encode('Instructor', $enclosure);

        $csv = implode($delimiter, $headers)."\n";

        // Report Data
        foreach ($records as $r) {
            $row = array();
            $row[] = $this->csv_encode($r['course_fullname'], $enclosure);
            $row[] = $this->csv_encode($r['course_category_name'], $enclosure);
            $row[] = $this->csv_encode($r['user_lastname'], $enclosure);
            $row[] = $this->csv_encode($r['user_firstname'], $enclosure);
            $row[] = $this->csv_encode($r['user_email'], $enclosure);

            foreach ($selectedOptionalCols as $col) {
                $x = $r['user_profilegroups'][$col->getId()];
                $row[] = $x ? $this->csv_encode($x, $enclosure) : '';
            }

            $row[] = $this->csv_encode(ucfirst($r['record_type']), $enclosure);
            // $row[] = $this->csv_encode($r->status, $enclosure);
            $row[] = $this->csv_encode(empty($r['deadline']) ?
                '' : date('n/j/Y', $r['deadline']), $enclosure);
            $row[] = $this->csv_encode(empty($r['session_startdate']) ?
                '' : date('n/j/Y', $r['session_startdate']), $enclosure);
            $row[] = $this->csv_encode($r['attended'], $enclosure);
            $row[] = $this->csv_encode($r['hours_attended'], $enclosure);
            if ($r['course_format'] != 'ilt') {
                $row[] = $this->csv_encode($r['final_grade'], $enclosure);
            }
            else {
                $row[] = '';
            }
            $row[] = $this->csv_encode(empty($r['completion_date']) ?
                '' : date('n/j/Y', $r['completion_date']), $enclosure);
            $row[] = $this->csv_encode($r['course_hours'], $enclosure);
            $row[] = $this->csv_encode($r['course_credits'], $enclosure);
            $row[] = $this->csv_encode($r['course_cost_per_student'], $enclosure);

            /*
            $instructor = 'N/A';
            if ($r->Course->CourseSection) {
                $instructor = $r->Course->CourseSection->instructor;
            }
            else if ($r->CourseSection) {
                $instructor = $r->CourseSection->instructor;
            }
            $row[] =$this->csv_encode($instructor, $enclosure);
            */

            $csv .= implode($delimiter, $row)."\n";
        }

        return $csv;
    }

    private function csv_encode($value, $enclosure)
    {
        $enc_enclosure = $enclosure.$enclosure;
        $value = str_replace($enclosure, $enc_enclosure, $value);

        return $enclosure.$value.$enclosure;
    }

    public function assignments(ParameterBag $params, $first=0, $max=10)
    {
        return $this->reportRepository->assignments($params, $first, $max);
    }

    public function completions(ParameterBag $params, $first = 0, $max = 10)
    {
        return $this->reportRepository->completions($params, $first, $max);
    }

    public function enrollments(ParameterBag $params, $first=0, $max=10)
    {
        return $this->reportRepository->enrollments($params, $first, $max);
    }
}