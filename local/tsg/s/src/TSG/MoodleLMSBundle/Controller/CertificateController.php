<?php
namespace TSG\MoodleLMSBundle\Controller;


use Symfony\Component\HttpFoundation\Response;

class CertificateController extends BaseController
{
    /**
     * Check that the passed in $gradeid was made by the current user and that it is a passing
     * grade.  If so, render a certificate.
     *
     * @param int $gradeid id of Grade record whose certificate is sought
     * @return Response
     */
    public function indexAction($gradeid)
    {
        require_login();

        // note that this is the Moodle user object, not a domain User object
        $user   = $this->moodleGlobals->getUser();
        $repo   = $this->em->getRepository('TSGMoodleLMSBundle:Grade');

        $grade = $repo->find($gradeid);
        if ($grade->getUser()->getId() != $user->id) {
            return new Response('Forbidden', 403);
        }

        $gradeItem = $grade->getItem();
        if ($grade->getFinalGrade() < $gradeItem->getGradePass()) {
            return new Response('You have not completed this course', 404);
        }

        $course    = $gradeItem->getCourse();
        if ($course->getFormat() === 'ilt') {
            $sessionRepo = $this->em->getRepository('TSGMoodleLMSBundle:CourseSession');
            $session     = $sessionRepo->find($gradeItem->getItemInstance());

            $rosterRepo  = $this->em->getRepository('TSGMoodleLMSBundle:SessionRoster');
            $rosterEntry = $rosterRepo->findOneBy(array(
                'user'    => $grade->getUser(),
                'session' => $session
            ));

            $completionDate = $session     ? date('m/d/Y', $session->getEndDate()) : '';
            $hoursAttended  = $rosterEntry ? $rosterEntry->getHoursAttended()      : 0;
            $instructor     = $session     ? $session->getInstructor()             : '';
        }
        else {
            $completionDate = date('m/d/Y', $grade->getTimeModified());
            $hoursAttended  = 0;
            $instructor     = '';
        }


        $output = $this->moodleGlobals->getOutput();

        $content = $this->templating->render('TSGMoodleLMSBundle:certificate:index.html.twig',
            array(
                'firstname'       => $grade->getUser()->getFirstname(),
                'lastname'        => $grade->getUser()->getLastname(),
                'coursename'      => $course->getFullname(),
                'completion_date' => $completionDate,
                'hours_attended'  => $hoursAttended,
                'instructor'      => $instructor,
                'border_pic_url'  => $output->pix_url('border-blue', 'theme'),
                'seal_pic_url'    => $output->pix_url('seal-ribbon', 'theme')
            )
        );

        return new Response($content);
    }
}