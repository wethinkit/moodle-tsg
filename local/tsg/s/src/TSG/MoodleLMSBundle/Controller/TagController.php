<?php


namespace TSG\MoodleLMSBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;


class TagController extends BaseController
{
    /**
     * @View()
     */
    public function findByPatternAction(Request $request)
    {
        $pattern = $request->query->get('pattern');
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:Tag');
        $tags = $repo->findByPattern($pattern, 'name', 10, 0);

        return new EntitiesContainer($tags, array('short'));
    }
} 