<?php

namespace TSG\MoodleLMSBundle\Controller;


use Symfony\Component\HttpFoundation\Response;

class UserGroupManagementController extends BaseController
{

    public function indexAction()
    {
        $userGroupRepo = $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
        $groupHiearchy = $userGroupRepo->getCompleteHierarchy(true);
        $groupHiearchy = $userGroupRepo->toArrays($groupHiearchy);

        $output = $this->moodleGlobals->getOutput();
        $page = $this->moodleGlobals->getPage();

        $page->set_context(\context_system::instance());

        $params = array(array(
            'urls' => array(
                'usergroup' => $this->urlService->userGroup(),
                'user' => $this->urlService->user()
            ),
            'groupHiearchy' => $groupHiearchy
        ));

        $page->requires->yui_module('moodle-local_tsg-usergroupmanagement',
            'M.local_tsg.usergroupmanagement.init', $params);

        $page->set_title('User Groups');
        $page->set_heading('User Groups');
        $page->set_cacheable(false);
        $page->navbar->add('User Groups');

        $content  = $output->header();
        $content .= $this->templating->render('TSGMoodleLMSBundle:usergroupmanagement:index.html.twig');
        $content .= $output->footer();

        return new Response($content);
    }
}