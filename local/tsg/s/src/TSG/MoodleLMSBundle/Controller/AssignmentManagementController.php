<?php

namespace TSG\MoodleLMSBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TSG\MoodleLMSBundle\Entity\CourseAssignment;
use TSG\MoodleLMSBundle\Entity\SessionRoster;
use TSG\MoodleLMSBundle\HAL\Container\RecordsContainer;
use TSG\MoodleLMSBundle\HAL\Container\SingleItemContainer;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;
use TSG\MoodleLMSBundle\HAL\Serialization\ExclusionRule;
use TSG\MoodleLMSBundle\HAL\Serialization\MaxDepthExclusionStrategy;
use TSG\MoodleLMSBundle\HAL\Serialization\RulesExclusionStrategy;

class AssignmentManagementController extends BaseController
{

    public function indexAction($courseid)
    {
        $courseRepo = $this->em->getRepository('TSGMoodleLMSBundle:Course');
        $course = $courseRepo->find($courseid);

        $output = $this->moodleGlobals->getOutput();
        $page = $this->moodleGlobals->getPage();

        $page->set_context(\context_system::instance());

        $params = array(array(
            'courseId' => $courseid,
            'urls' => array(
                'assignment' => $this->urlService->assignment(),
                'user' => $this->urlService->user(),
                'course' => $this->urlService->course()
            )
        ));

        $page->requires->yui_module('moodle-local_tsg-assignmentmanagement',
            'M.local_tsg.assignmentmanagement.init', $params);


        $page->set_title('Assign Users to Course');
        // $page->set_heading('Assign Users to Course');
        $page->set_cacheable(false);
        $page->navbar->add($course->getFullName(),
            new \moodle_url('/course/view.php', array('id' => $course->getId())));
        $page->navbar->add('Assign Users');

        $content  = $output->header();
        $content .= $this->templating->render('TSGMoodleLMSBundle:assignmentmanagement:index.html.twig');
        $content .= $output->footer();

        return new Response($content);
    }

    /**
     * @param Request $request
     * @return JsonResponse|SingleItemContainer
     *
     * @View()
     */
    public function assignAndEnrollUserAction(Request $request)
    {
        $data = json_decode($request->getContent());

        $user = $this->em->find('TSGMoodleLMSBundle:User', $data->userId);

        /*
         * Assignment
         */

        $assignment = new CourseAssignment();
        $assignment->setUser($user);

        $course = $this->em->find('TSGMoodleLMSBundle:Course', $data->assignment->courseId);
        $assignment->setCourse($course);

        $deadline = new \DateTime($data->assignment->deadline);
        $assignment->setDeadline($deadline->getTimestamp());

        $assignment->setReason($data->assignment->reason);
        $assignment->setRepeatInterval($data->assignment->repeatInterval);
        $assignment->setRepeatPeriod($data->assignment->repeatPeriod);


        /*
         * Enrollment (optional)
         */

        $enrollment = false;
        if (property_exists($data, 'enrollment') && !empty($data->enrollment->sessionId)) {
            $enrollment = new SessionRoster();
            $enrollment->setUser($user);
            $enrollment->setAssignment($assignment);

            $session = $this->em->find('TSGMoodleLMSBundle:CourseSession', $data->enrollment->sessionId);
            if (!$session) {
                $error = new \stdClass();
                $error->type = "http://example.com/errors/sessionid-required-for-enrollment";
                $error->title = "A valid session id is required for enrollment.";

                return new JsonResponse($error, 500);
            }

            $enrollment->setSession($session);
        }

        $this->em->persist($assignment);
        if ($enrollment) {
            $this->em->persist($enrollment);
        }
        $this->em->flush();

        return new SingleItemContainer($assignment, array('short'));
    }

    /**
     * @param Request $request
     * @return EntitiesContainer
     *
     * @View()
     */
    public function searchAction(Request $request)
    {
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:CourseAssignment');
        $criteria = array();

        global $CFG;
        require_once($CFG->dirroot . LOCAL_TSG_ROOT . '/locallib.php');
        if (!local_tsg_isadmin()) {
            global $USER;
            $criteria['filterForManagedGroupsOfUserId'] = $USER->id;
        }

        if ($request->query->has('courseId'))
            $criteria['courseid'] = $request->query->get('courseId');

        if ($request->query->has('deadlineStart'))
            $criteria['deadlineStart'] = new \DateTime($request->query->get('deadlineStart'));

        if ($request->query->has('deadlineEnd'))
            $criteria['deadlineEnd'] = new \DateTime($request->query->get('deadlineEnd'));

        $filterForCompletes   = $request->query->get('filterForCompletes') == 'true';
        $filterForIncompletes = $request->query->get('filterForIncompletes') == 'true';


        /*
         * If neither complete nor incomplete are selected, show both, otherwise show what has
         * been selected.
         */

        if (!$filterForCompletes && !$filterForIncompletes) {
            $criteria['filterForCompletes'] = $criteria['filterForIncompletes'] = true;
        }
        else {
            $criteria['filterForCompletes'] = $filterForCompletes;
            $criteria['filterForIncompletes'] = $filterForIncompletes;
        }


        $matches = $repo->search($criteria, true);


        $exclusionRules = array();
        $exclusionRules[] = new ExclusionRule('CourseCategory', 'parent', 5);

        $exclusionStrategy = new RulesExclusionStrategy($exclusionRules, 6);

        return new EntitiesContainer($matches, array('short', 'associations'), '', false, false,
            $exclusionStrategy);
    }
}