<?php
namespace TSG\MoodleLMSBundle\Controller;


use FOS\RestBundle\Controller\Annotations\View;
use TSG\MoodleLMSBundle\Entity\Lib\EntityLib;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;
use TSG\MoodleLMSBundle\HAL\Problem\NotLoggedInProblem;
use TSG\MoodleLMSBundle\HAL\Serialization\ExclusionRule;
use TSG\MoodleLMSBundle\HAL\Serialization\RulesExclusionStrategy;

class FrontPageController extends BaseController
{
    /**
     * @View
     * @return EntitiesContainer containing a CourseAssignment[] of incomplete assignments
     */
    public function incompletesForCurrentUserAction()
    {
        if (!isloggedin()) {
            return NotLoggedInProblem::create($this->router);
        }

        $user = $this->moodleGlobals->getUser();
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:CourseAssignment');

        $criteria = array();
        $criteria['userid'] = $user->id;

        $assignments = $repo->search($criteria, true);

        $incompleteAssignments = array_filter($assignments,
            function ($a) {
                return !$a->isComplete();
            }
        );

        $exclusionRules = array();
        $exclusionRules[] = new ExclusionRule('CourseCategory', 'parent', 5);

        $exclusionStrategy = new RulesExclusionStrategy($exclusionRules, 5, $this->logger);

        return new EntitiesContainer($incompleteAssignments, array('short','associations'), '',
            false, false, $exclusionStrategy);
    }

    /**
     * @View
     * @return EntitiesContainer containing a Grade[] of passing grades
     */
    public function completionsForCurrentUserAction()
    {
        if (!isloggedin()) {
            return NotLoggedInProblem::create($this->router);
        }

        $user = $this->moodleGlobals->getUser();
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:Grade');

        $completions             = $repo->completionsForUser($user->id);
        $sessions                = $repo->sessionsForGrades($completions);
        $completionsWithSessions = EntityLib::addSessionsToGrades($completions, $sessions);

        $exclusionRules    = array();
        $exclusionRules[]  = new ExclusionRule('CourseCategory', 'parent', 6);
        $exclusionStrategy = new RulesExclusionStrategy($exclusionRules, 6, $this->logger);


        return new EntitiesContainer($completionsWithSessions, array('short','associations'),
            '', false, false, $exclusionStrategy);
    }
}