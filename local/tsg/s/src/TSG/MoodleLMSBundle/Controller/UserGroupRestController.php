<?php

namespace TSG\MoodleLMSBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TSG\MoodleLMSBundle\Entity\UserGroup;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;

class UserGroupRestController extends BaseController
{
    /**
     * @View
     */
    public function fetchAllAction()
    {
        $userGroupRepo = $this->getUserGroupRepository();
        $userGroups = $userGroupRepo->getCompleteHierarchy(false);

        $selfRoute = $this->router->generate('tsg_moodle_lms.usergroup.rest.fetch.all');
        return new EntitiesContainer($userGroups->getChildren(), array('short','children'), $selfRoute);
    }

    public function addAction(Request $request)
    {
        $groupInRequest = json_decode($request->getContent());

        $group = new UserGroup();
        $group->setName($groupInRequest->name);
        $group->setIsProfileGroup(false);

        if ($groupInRequest->parent->id === 'root') {
            $parent = $this->getUserGroupRepository()->findBy(array('name' => 'root'));
            $parent = current($parent);
        }
        else {
            $parent = $this->getUserGroupRepository()->find($groupInRequest->parent->id);
        }
        
        $group->getParent()->add($parent);

        $this->em->persist($group);
        $this->em->flush();

        return new JsonResponse($group);
    }

    public function deleteAction($groupid)
    {
        $group = $this->getUserGroupRepository()->find($groupid);
        $this->em->remove($group);
        $this->em->flush();

        return new JsonResponse();
    }

    /**
     * @View
     * @param $groupid int id of TSG\MoodleLMSBundle\Entity\UserGroup whose members are sought
     * @return EntitiesContainer container with a TSG\MoodleLMSBundle\Entity\UserGroup[] of the
     *      members of $groupid and supporting metadata
     */
    public function fetchMembersAction($groupid)
    {
        $repo = $this->getUserGroupRepository();
        $group = $repo->find($groupid);
        $members = $group && $group->getUsers() ? $group->getUsers() : array();

        $selfRoute = $this->router->generate('tsg_moodle_lms.usergroup.rest.members.fetch', array(
            'groupid' => $groupid
        ));
        return new EntitiesContainer($members, array('short'), $selfRoute);
    }

    /**
     * @View
     * @param $groupid int id of of TSG\MoodleLMSBundle\Entity\UserGroup whose managers are sought
     * @return EntitiesContainer container with a TSG\MoodleLMSBundle\Entity\UserGroup[] of the
     *      managers of $groupid and supporting metadata
     */
    public function fetchManagersAction($groupid)
    {
        $repo = $this->getUserGroupRepository();
        $group = $repo->find($groupid);
        $managers = $group && $group->getManagers() ? $group->getManagers() : array();

        $selfRoute = $this->router->generate('tsg_moodle_lms.usergroup.rest.managers.fetch', array(
            'groupid' => $groupid
        ));
        return new EntitiesContainer($managers, array('short'), $selfRoute);
    }

    public function addMemberAction($groupid, Request $request)
    {
        $userInRequest = json_decode($request->getContent());
        $group = $this->getUserGroupRepository()->find($groupid);
        $user = $this->getUserRepository()->find($userInRequest->id);

        $user->getGroups()->add($group);
        $this->em->flush();

        return new JsonResponse($user);
    }

    public function removeMembersAction($groupid, Request $request)
    {
        $usersInRequest = json_decode($request->getContent());

        $group = $this->getUserGroupRepository()->find($groupid);

        $userRepo = $this->getUserRepository();
        foreach ($usersInRequest as $u) {
            $user = $userRepo->find($u->id);
            $user->getGroups()->removeElement($group);
        }

        $this->em->flush();
        return new JsonResponse();
    }

    public function addManagerAction($groupid, Request $request)
    {
        $userInRequest = json_decode($request->getContent());
        $group = $this->getUserGroupRepository()->find($groupid);
        $user = $this->getUserRepository()->find($userInRequest->id);

        $group->getManagers()->add($user);
        $this->em->flush();

        return new JsonResponse($user);
    }

    public function removeManagersAction($groupid, Request $request)
    {
        $usersInRequest = json_decode($request->getContent());

        $group = $this->getUserGroupRepository()->find($groupid);
        $managers = $group->getManagers();

        $userRepo = $this->getUserRepository();
        foreach ($usersInRequest as $u) {
            $user = $userRepo->find($u->id);
            $managers->removeElement($user);
        }

        $this->em->flush();
        return new JsonResponse();
    }


    private function getUserGroupRepository()
    {
        return $this->em->getRepository('TSGMoodleLMSBundle:UserGroup');
    }
    private function getUserRepository()
    {
        return $this->em->getRepository('TSGMoodleLMSBundle:User');
    }
}