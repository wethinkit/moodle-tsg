<?php
namespace TSG\MoodleLMSBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;
use TSG\MoodleLMSBundle\HAL\Serialization\MaxDepthExclusionStrategy;

class CourseCategoryController extends BaseController
{
    /**
     * @View
     * @param int $categoryid
     * @return EntitiesContainer children of $categoryid
     */
    public function childrenAction($parentid)
    {
        $children = $this->em->getRepository('TSGMoodleLMSBundle:CourseCategory')
            ->children($parentid);

        return new EntitiesContainer($children, array('short','collections'),'',false, false,
            new MaxDepthExclusionStrategy(5, $this->logger));
    }
}