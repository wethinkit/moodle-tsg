<?php
namespace TSG\MoodleLMSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use TSG\MoodleLMSBundle\HAL\Container\EntitiesContainer;

class CourseController extends BaseController
{
    /**
     * @View()
     */
    public function findByPatternAction(Request $request)
    {
        $pattern = $request->query->get('pattern');
        $repo = $this->em->getRepository('TSGMoodleLMSBundle:Course');
        $courses = $repo->findByPattern($pattern, 'fullName', 10, 0);

        return new EntitiesContainer($courses, array('short'));
    }
} 