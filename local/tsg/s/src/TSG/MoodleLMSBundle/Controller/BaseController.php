<?php
namespace TSG\MoodleLMSBundle\Controller;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;
use TSG\MoodleLMSBundle\HAL\HALViewHandler;
use TSG\MoodleGlobalsBundle\Facade\MoodleGlobals;
use Doctrine\ORM\EntityManagerInterface as EntityManager;
use TSG\MoodleLMSBundle\Misc\UrlService;

class BaseController {

    protected $moodleGlobals;
    protected $persistence;
    protected $em;
    protected $viewHandler;
    protected $urlService;
    protected $templating;
    protected $router;
    protected $logger;

    public function __construct(
        MoodleGlobals $moodleGlobals,
        $persistence,
        EntityManager $em,
        HALViewHandler $viewHandler,
        UrlService $urlService,
        EngineInterface $templating,
        RouterInterface $router,
        $logger)
    {
        $this->moodleGlobals = $moodleGlobals;
        $this->persistence = $persistence;
        $this->em = $em;
        $this->viewHandler = $viewHandler;
        $this->urlService = $urlService;
        $this->templating = $templating;
        $this->router = $router;
        $this->logger = $logger;

        $moodleGlobals->getPage()->requires->jquery();
    }
}