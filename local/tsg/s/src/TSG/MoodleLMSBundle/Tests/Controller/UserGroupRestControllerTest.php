<?php

namespace TSG\MoodleLMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class UserGroupRestControllerTest extends KernelTestCase
{

    const GROUPID_WITH_MANAGERS = 6;
    private static $controller;

    public static function setUpBeforeClass()
    {
        define('CLI_SCRIPT', true);

        echo 'booting kernel'.PHP_EOL;
        self::bootKernel();
        $GLOBALS['kernel'] = static::$kernel;

        self::$controller = static::$kernel->getContainer()->get('tsg_moodle_lms.controller.usergroup.rest');
    }


    public function testFetchManagersAction()
    {
        $managers = self::$controller->fetchManagersAction(self::GROUPID_WITH_MANAGERS);
        $managers = $managers->getItems();
        $this->assertCount(5, $managers);

        echo PHP_EOL;
        foreach ($managers as $m) {
            echo $m->getLastName() . ', ' . $m->getFirstName() . PHP_EOL;
        }
    }
}