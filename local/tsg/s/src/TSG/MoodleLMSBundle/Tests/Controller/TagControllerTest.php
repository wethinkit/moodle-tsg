<?php


namespace TSG\MoodleLMSBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class TagControllerTest extends KernelTestCase
{
    private static $controller;

    public static function setUpBeforeClass()
    {
        define('CLI_SCRIPT', true);

        echo 'booting kernel'.PHP_EOL;
        self::bootKernel();
        $GLOBALS['kernel'] = static::$kernel;

        self::$controller = static::$kernel->getContainer()->get('tsg_moodle_lms.controller.tag');
    }


    public function testFindByPatternAction()
    {
        $request = $this->getMockBuilder('\Symfony\Component\HttpFoundation\Request')
            ->getMock();
        $query = $this->getMockBuilder('\Symfony\Component\HttpFoundation\ParameterBag')
            ->getMock();
        $request->query = $query;
        $query->method('get')->willReturn('c');

        $matches = self::$controller->findByPatternAction($request);

        $this->assertEquals(3, count($matches->getEntities()), 'oops');
    }
} 