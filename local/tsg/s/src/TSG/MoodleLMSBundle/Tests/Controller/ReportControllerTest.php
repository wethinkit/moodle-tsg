<?php

namespace TSG\MoodleLMSBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TSG\MoodleLMSBundle\Tests\TSGKernelTestCase;

class ReportControllerTest extends TSGKernelTestCase
{
    private static $TEST_COURSE = 3990; // DSNAP Annual Training 2013
    private static $DEFAULT_PARAMETERS = array(
        'courseId' => 'false',
        'tagId' => 'false',
        'userId' => 'false',
        'userGroupId' => 0,
        'startDate' => 'false',
        'endDate' => 'false',
        'includeTerminated' => 'false',
        'includeInactives' => 'false',
        'reportType' => 'false'
    );

    private static $controller;

    public static function setupBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$controller = static::$kernel->getContainer()->get('tsg_moodle_lms.controller.report');
    }

    public function testFindAssignments()
    {
        $this->findRecords(
            array(
                'Accept' => 'application/hal+json'
            ),
            array(
                'first' => 0,
                'max' => 10
            ), array(
                'reportType' => 'assignments',
                // 'userId' => 42163
            )
        );
    }

    public function testFindCompletionsPage()
    {
        $this->findRecords(
            array(
                'Accept' => 'application/hal+json'
            ),
            array(
                'first' => 0,
                'max' => 10
            ), array(
                'reportType' => 'completions',
                // 'userId' => 42163
            )
        );
    }

    public function testFindCompletionsCsv()
    {
        $response = $this->findRecords(
            array(
                'Accept' => 'text/csv'
            ),
            array(),
            array(
                'reportType' => 'completions',
                'userId' => 42163
            )
        );

        $this->assertInstanceOf('\Symfony\Component\HttpFoundation\Response', $response);
        $this->assertEquals('text/csv', $response->headers->get('Content-Type'));
    }

    public function testFindCompletionsWithOptionalColsCsv()
    {
        $response = $this->findRecords(
            array(
                'Accept' => 'text/csv'
            ),
            array(),
            array(
                'reportType' => 'completions',
                'userId' => 42163,
                'optionalCol_id=2' => 'true' // group id 2 is Region
            )
        );

        $this->assertInstanceOf('\Symfony\Component\HttpFoundation\Response', $response);
        $this->assertEquals('text/csv', $response->headers->get('Content-Type'));

        $rows = explode("\n", $response->getContent());
        for ($i = 1; $i < count($rows); $i++) {
            if (empty($rows[$i])) {
                continue;
            }
            $fields = str_getcsv($rows[$i]);
            $this->assertEquals('Region 06 (Lake Charles)', $fields[4], 'expected to find region in 4th column');
        }
    }


    public function testFindEnrollments()
    {
        $this->findRecords(
            array(
                'Accept' => 'application/hal+json'
            ),
            array(
                'first' => 1000,
                'max' => 5
            ), array(
                'reportType' => 'enrollments',
                // 'userId' => 42163
            )
        );
    }

    public function testFindCompletionsForCourse()
    {
        $completions = $this->findRecords(
            array(
                'Accept' => 'application/hal+json'
            ),
            array(
                'first' => 0,
                'max' => 10
            ), array(
                'reportType' => 'completions',
                'courseId' => self::$TEST_COURSE
            )
        );

        foreach ($completions as $c) {
            $this->assertEquals(self::$TEST_COURSE, $c['course_id'],
                'only course ' . self::$TEST_COURSE . ' records should be present' . print_r($c, true));
        }
    }

    public function testFindCompletionsWithoutTerminated()
    {
        $completions = $this->findRecords(
            array(
                'Accept' => 'application/hal+json'
            ),
            array(
                'first' => 0,
                'max' => 10
            ), array(
                'reportType' => 'completions',
                'includeTerminated' => 'false'
            )
        );

        foreach ($completions as $c) {
            $this->assertEmpty($c['user_termination_date'],
                'no records for terminated users should be present' . print_r($c, true));
        }
    }

    public function testFindCompletionsWithoutInactives()
    {
        $completions = $this->findRecords(
            array(
                'Accept' => 'application/hal+json'
            ),
            array(
                'first' => 0,
                'max' => 10
            ), array(
                'reportType' => 'completions',
                'includeInactives' => 'false'
            )
        );

        foreach ($completions as $c) {
            $this->assertTrue($c['user_isactive'] == null || $c['user_isactive'] == 1,
                'no records for terminated users should be present' . print_r($c, true));
        }
    }


    /**
     * Runs ReportController::runAction method with a Request constructed from the
     * passed in parameters.  Checks the structure of the results and then returns them.
     *
     * @param array $headers request headers (e.g. array('Accept' => 'application/hal+json'))
     * @param array $queryParams query string parameters
     * @param array $bodyParams request body parameters
     * @return array flattened TSG\MoodleLMSBundle\Entity\[CourseAssignment|Grade|SessionRoster]
     *      object graph
     *
     */
    private function findRecords(array $headers = array(), array $queryParams = array(), array $bodyParams = array())
    {
        $request = new Request($queryParams, array_merge(self::$DEFAULT_PARAMETERS, $bodyParams));
        $request->headers->add($headers);


        /*
         * Query the db
         */
        $result = self::$controller->runAction($request);
        if ($result instanceof Response) {
            return $result;
        }

        $records = $result->getItems();
        $expectedCount = $request->query->get('max');
        $this->assertCount($expectedCount, $records, 'expected ' . $expectedCount . ' records');


        /*
         * Check the structure of the response
         */

        switch ($request->request->get('reportType')) {
            case 'assignments':
                $expectedRecordType = 'assignment';
                break;
            case 'completions':
                $expectedRecordType = 'completion';
                break;
            case 'enrollments':
                $expectedRecordType = 'enrollment';
                break;
            default:
                $expectedRecordType = 'error';
        }

        foreach ($records as $r) {
            $this->assertEquals($expectedRecordType, $r['record_type'],
                'record_type should be "' . $expectedRecordType . '"');
            $this->assertCommonStructure($r);
        }


        return $records;
    }



    private function assertCommonStructure(array $record)
    {
        $this->assertArrayHasKey('record_type', $record, 'expected record_type field to be present');
        $this->assertArrayHasKey('user_id', $record, 'expected user_id field to be present');
        $this->assertArrayHasKey('user_lastname', $record, 'expected user_lastname field to be present');
        $this->assertArrayHasKey('user_firstname', $record, 'expected user_firstname field to be present');
        $this->assertArrayHasKey('user_termination_date', $record, 'expected user_termination_date field to be present');
        $this->assertArrayHasKey('user_isactive', $record, 'expected user_isactive field to be present');
        $this->assertArrayHasKey('course_id', $record, 'expected course_id field to be present');
        $this->assertArrayHasKey('course_fullname', $record, 'expected course_fullname field to be present');
        $this->assertArrayHasKey('course_format', $record, 'expected course_format field to be present');
        $this->assertArrayHasKey('course_hours', $record, 'expected course_hours field to be present');
        $this->assertArrayHasKey('course_credits', $record, 'expected course_credits field to be present');
        $this->assertArrayHasKey('course_cost_per_student', $record, 'expected course_cost_per_student field to be present');
        $this->assertArrayHasKey('course_category_name', $record, 'expected course_category_name field to be present');

        $this->assertArrayHasKey('user_profilegroups', $record, 'expected user_profilegroups field to be present');

        $userProfileGroups = $record['user_profilegroups'];
        $this->assertInternalType('array', $userProfileGroups, 'expected user_profilegroups to be an array');

        foreach ($userProfileGroups as $profileGroupId => $memberGroupName) {
            $this->assertInternalType('int', $profileGroupId, 'expected user_profilegroups keys to be ints');
            $this->assertInternalType('string', $memberGroupName, 'expected user_profilegroups values to be strings');
        }

        $this->assertArrayHasKey('deadline', $record, "deadline field should be present");
        $this->assertArrayHasKey('final_grade', $record, 'expected final_grade field to be present');
        $this->assertArrayHasKey('completion_date', $record, 'expected completion_date field to be present');
        $this->assertArrayHasKey('session_startdate', $record, 'expected session_startdate field to be present');
        $this->assertArrayHasKey('attended', $record, 'expected attended field to be present');
        $this->assertArrayHasKey('hours_attended', $record, 'expected hours_attended field to be present');
    }
} 