<?php


namespace TSG\MoodleLMSBundle\Tests\Controller;

use TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib;
use TSG\MoodleLMSBundle\Tests\TSGKernelTestCase;


class AssignmentManagementControllerTest extends TSGKernelTestCase
{
    private static $controller;

    public static function setupBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$controller =
            static::$kernel->getContainer()->get('tsg_moodle_lms.controller.assignmentmanagement');
    }


    public function testAssignmentsForCurrentUserAction()
    {
        global $USER;
        $USER->id = 1763;

        $assignments = self::$controller->assignmentsForCurrentUserAction();
        $this->assertNotNull($assignments, 'expected assignments for user 1763');

        /*
        echo EntityToStringLib::arrayToString($assignments->getItems(),
            array('\TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib', 'assignmentToString'));
        */

        $viewHandler = $this->getMockBuilder('FOS\RestBundle\View\ViewHandlerInterface')
            ->getMock();

        $view = $this->getMockBuilder('\FOS\RestBundle\View\View')->getMock();
        $view->method('getHeaders')->willReturn(array('Content-Type' => 'application/hal+json'));
        $view->method('getData')->willReturn($assignments);

        $request = $this->getMockBuilder('\Symfony\Component\HttpFoundation\Request')
            ->getMock();

        $halViewHandler = static::$kernel->getContainer()->get('tsg_moodle_lms.hal.viewhandler');
        $response = $halViewHandler->handle($viewHandler, $view, $request, 'hal');

        $this->assertNotNull($response, '$response cannot be null');
    }


    /*
    public function testFindByPatternAction()
    {
        $request = $this->getMockBuilder('\Symfony\Component\HttpFoundation\Request')
            ->getMock();
        $query = $this->getMockBuilder('\Symfony\Component\HttpFoundation\ParameterBag')
            ->getMock();
        $request->query = $query;
        $query->method('get')->willReturn('c');

        $matches = self::$controller->findByPatternAction($request);

        $this->assertEquals(3, count($matches->getEntities()), 'oops');
    }
    */
} 