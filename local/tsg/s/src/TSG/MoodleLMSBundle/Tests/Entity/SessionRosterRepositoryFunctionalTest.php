<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 1/15/15
 * Time: 2:22 PM
 */

namespace TSG\MoodleLMSBundle\Tests\Entity;


class SessionRosterRepositoryFunctionalTest extends BaseEntityRepositoryTest
{

    public function test1()
    {
        $enrollments = self::$repository->findBy(array(), null, 5, 0);
        $this->assertCount(5, $enrollments, 'should have 5 enrollments');
    }

    public function test2()
    {
        $qb = self::$em->createQueryBuilder();
        $qb->select('r, u, ui, s, c, ci, cat')
            ->from('TSGMoodleLMSBundle:SessionRoster', 'r')
            ->join('r.user', 'u')
            ->leftJoin('u.info', 'ui')
            ->join('r.session', 's')
            ->join('s.course', 'c')
            ->leftJoin('c.info', 'ci')
            ->join('c.category', 'cat')
            // ->where($qb->expr()->in('r.id', array(132364, 132365)))
            ->where($qb->expr()->in('r.id', array(96003, 13714, 29997, 30155, 38764)));


        $enrollments = $qb->getQuery()
            // ->setFirstResult(0)
            // ->setMaxResults(2)
            ->getResult();

        $this->assertCount(5, $enrollments, 'should have 5 enrollments');
    }


    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:SessionRoster';
    }
} 