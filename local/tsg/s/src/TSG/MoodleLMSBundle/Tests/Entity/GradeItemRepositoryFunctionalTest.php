<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

class GradeItemRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    const CLASSNAME = 'GradeItemRepositoryFunctionalTest';

    public function testX()
    {
        echo self::CLASSNAME.'::testX()'.PHP_EOL;

        $qb = self::$repository->createQueryBuilder('gi');
        $entities = $qb
            ->where($qb->expr()->eq('gi.id', 12))
            ->getQuery()
            ->getResult();

        echo 'entity: '.$entities[0]->getItemModule().PHP_EOL;
    }

    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:GradeItem';
    }
}