<?php

namespace TSG\MoodleLMSBundle\Tests\Entity;


class UserGroupRepositoryFunctionalTest extends BaseEntityRepositoryTest
{

    public function testFlattening()
    {
        $root = self::$repository->findOneBy(array('name' => 'Region'));
        // echo 'found ' . count($region) . ' root group(s)' . PHP_EOL;

        $flat = self::$repository->flattenGroupHierarchy($root);
        $this->assertTrue(!empty($flat));

        /*
        echo 'flattened hiearchy has ' . count($flat) . ' groups' . PHP_EOL;
        foreach ($flat as $group) {
            echo $group->getLevel() . ':  ' . $group->getName() . PHP_EOL;
        }
        */

        // $this->assertEquals(true, true);
    }

    public function testGetCompleteHierarchy()
    {
        $root = self::$repository->getCompleteHierarchy(true);
        $this->assertTrue(!empty($root));

        /*
        foreach ($root as $group) {
            echo $group->getLevel() . ':  ' . $group->getName() . PHP_EOL;
        }
        */

        // $this->assertEquals(true, true);
    }

    public function testGetProfileGroups()
    {
        $profileGroups = self::$repository->getProfileGroups();
        $this->assertTrue(!empty($profileGroups));

        /*
        foreach ($profileGroups as $g) {
            echo $g->getId().': '.$g->getName().PHP_EOL;
        }
        */

        // $this->assertEquals(true, true);
    }

    public function testGetManagers()
    {
        $group = self::$repository->find(6);
        $this->assertTrue(!empty($group));

        echo PHP_EOL;
        foreach ($group->getManagers() as $m) {
            echo $m->getLastName() . ', ' . $m->getFirstName() . PHP_EOL;
        }
    }

    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:UserGroup';
    }
} 