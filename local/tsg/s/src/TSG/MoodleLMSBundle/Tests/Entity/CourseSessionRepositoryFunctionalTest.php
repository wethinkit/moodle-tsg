<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use TSG\MoodleLMSBundle\Entity\Spec\AsObject;
use TSG\MoodleLMSBundle\Entity\Spec\Condition;
use TSG\MoodleLMSBundle\Entity\Spec\FilterInstructor;

class CourseSessionRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    private static $classname = 'CourseSessionRepositoryFunctionalTest';


    public function testFindFromStartDate()
    {
        echo self::$classname.'::testFindFromStartDate()' . PHP_EOL;

        $sessions = self::$repository->findFromStartDate(1406851200, true);
        $this->assertEquals(48, count($sessions), 'should have 48 sessions from 8/1/2014');
    }

    public function testMatchInstructor()
    {
        echo self::$classname.'::testMatchInstructor()' . PHP_EOL;

        $specification = new AsObject(new FilterInstructor('Terry'));
        $sessions = self::$repository->match($specification);

        $this->assertEquals(count($sessions), 7, 'should have 7 sessoins matching \'Terry%\'');
    }

    public function testMatchCondition()
    {
        echo self::$classname.'::testMatchCondition()' . PHP_EOL;

        $specification = new AsObject(new Condition('id', 11474));
        $session = self::$repository->match($specification);

        echo $session[0]->getCourse()->getFullName().PHP_EOL;
    }


    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:CourseSession';
    }
}