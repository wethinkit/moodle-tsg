<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

use TSG\MoodleLMSBundle\Entity\CourseAssignment;

class CourseAssignmentRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    const CLASSNAME = 'CourseAssignmentRepositoryFunctionalTest';

    public function testFindByUser()
    {
        // $this->log(self::CLASSNAME, 'testFindByUser');

        $assignments = self::$repository->findByUser(7012);
        // echo $this->arrayToString($assignments, array($this, 'assignmentToString')) . PHP_EOL;
        // ob_flush();
        $this->assertCount(4, $assignments, 'expected 4 assignments for user 7012');
    }

    public function testFindByUserWithCompletionInfo()
    {
        // $this->log(self::CLASSNAME, 'testFindByUserWithCompletionInfo');

        $assignments = self::$repository->findByUserWithCompletionInfo(7012);
        $this->assignmentAssertions($assignments);
    }

    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:CourseAssignment';
    }


    private function assignmentAssertions($assignments)
    {
        foreach ($assignments as $a) {
            switch ($a->getId()) {
                case 43798:
                    $this->assertEquals(1300770000, $a->getCompletionDate(),
                        'incorrect completion date for assignment 43798');
                    break;
                case 91086:
                    $this->assertEquals(1337749200, $a->getCompletionDate(),
                        'incorrect completion date for assignment 91086');
                    break;
                case 102129:
                    $this->assertEquals(1358834400, $a->getCompletionDate(),
                        'incorrect completion date for assignment 102129');
                    break;
                case 102133:
                    $this->assertEquals(1358834400, $a->getCompletionDate(),
                        'incorrect completion date for assignment 102133');
                    break;
            }
        }
    }

    private function arrayToString(array $values, $fn, $string='')
    {
        if (empty($values)) {
            return $string;
        }

        $v = $values[0];
        $string .= call_user_func($fn, $v) . PHP_EOL;

        return $this->arrayToString(array_slice($values, 1), $fn, $string);
    }

    private function assignmentToString(CourseAssignment $assignment)
    {
        $value = $assignment->getId() . ': userId = ' . $assignment->getUser()->getId()
            . ', courseId = ' . $assignment->getCourse()->getId()
            . ', deadline = ' . $assignment->getDeadline()
            . ', completionDate = ' . $assignment->getCompletionDate();


        $grades = $assignment->getGrades();
        if (!empty($grades)) {
            $value .= PHP_EOL . $this->arrayToString($grades, array($this, 'gradeToString'));
        }

        return $value;
    }

    /**
     * @param Grade $grade
     * @return string
     */
    private function gradeToString($grade)
    {
        if (!$grade) {
            return 'n/a';
        }

        $id = $grade->getId();
        return '    ' . ($id ? $id : 0)
        . ': idNumber = '     . $grade->getItem()->getIdNumber()
        . ', gradePass = '    . $grade->getItem()->getGradePass()
        . ', finalGrade = '   . $grade->getFinalGrade()
        . ', timeModified = ' . $grade->getTimeModified();
    }
}