<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use TSG\MoodleLMSBundle\Entity\Spec\AsObject;
use TSG\MoodleLMSBundle\Entity\Spec\Condition;
use TSG\MoodleLMSBundle\Entity\Spec\FilterInstructor;

class UserRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    private static $TEST_USERS = array(86, 14592);

    const ANDREA_CORTEZ_ID = 86;
    const DENISE_MENESSES_ID = 14592;


    public function testHasUserInfo()
    {
        $user = self::$repository->find(self::$TEST_USERS[0]);

        $this->assertObjectHasAttribute('info', $user, 'User should have an ::info property');
        $this->assertInstanceOf('\TSG\MoodleLMSBundle\Entity\UserInfo', $user->getInfo(),
            'User::info should be an instance of UserInfo');
    }

    public function testGetProfileGroupsForUser()
    {
        $records = self::$repository->getProfileGroupsForUser(self::$TEST_USERS[0]);
        $this->assertNotEmpty($records, 'Found no matching user or user has no group memberships');

        foreach ($records as $group) {
            $parent = $group->getParent();
            $this->assertTrue($parent[0]->isProfileGroup(), 'Only profile groups should be included');
        }
    }

    public function testGetProfileGroupMembershipForUsers()
    {
        // echo PHP_EOL . __METHOD__ . ': entry' . PHP_EOL;
        $records = self::$repository->getProfileGroupMembershipForUsers(self::$TEST_USERS);

        $this->assertNotEmpty($records, 'Found no matching users or none with group memberships');

        foreach ($records as $user) {
            $groups = $user->getGroups();
            if (empty($groups)) {
                // echo 'no groups for ' . $user->getFirstName() . ' ' . $user->getLastName() . PHP_EOL;
                continue;
            }
            // echo '    ' . 'user id ' . $user->getId() . ' is a member of ' . count($groups) . ' groups' . PHP_EOL;

            foreach ($groups as $g) {
                $parent = $g->getParent();
                // echo '        ' . $g->getName() . ' --> ' . $parent[0]->getName() . PHP_EOL;
                $this->assertTrue($parent[0]->isProfileGroup(), 'Only profile groups should be included');
            }
        }

        // echo PHP_EOL . __METHOD__ . ': exit' . PHP_EOL;
    }

    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:User';
    }
}