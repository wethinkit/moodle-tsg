<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

class CourseRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    const CLASSNAME = 'CourseRepositoryFunctionalTest';

    public function testX()
    {
        echo self::CLASSNAME.'::textX()'.PHP_EOL;

        $qb = self::$repository->createQueryBuilder('c');
        $courses = $qb
            ->where($qb->expr()->between('c.id', 100, 110))
            ->getQuery()
            ->getResult();

        echo 'course: '.$courses[0]->getFullName().PHP_EOL;
        echo 'category: '.$courses[0]->getCategory()->getName().PHP_EOL;

    }

    public function testTrainingArea()
    {
        echo self::CLASSNAME.'::testTrainingArea()'.PHP_EOL;

        $qb = self::$repository->createQueryBuilder('c');
        $course = $qb
            ->addSelect('ta')
            ->join('c.trainingAreas', 'ta')
            ->where($qb->expr()->eq('c.id', 23))
            ->getQuery()
            ->getSingleResult();

        echo 'course: '.$course->getFullName().PHP_EOL;
        foreach ($course->getTrainingAreas() as $ta) {
            echo 'training area: '.$ta->getName().PHP_EOL;
        }
    }

    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:Course';
    }
}