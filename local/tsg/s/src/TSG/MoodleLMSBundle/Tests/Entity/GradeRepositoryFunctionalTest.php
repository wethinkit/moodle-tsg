<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

use TSG\MoodleLMSBundle\Entity\Lib\EntityLib;
use TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib;

class GradeRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    const CLASSNAME = 'GradeRepositoryFunctionalTest';
    const DEBORAH_CAGE_ID = 1763;


    public function donottestCompletionsForUser()
    {
        $completions = self::$repository->completionsForUser(self::DEBORAH_CAGE_ID);
        echo EntityToStringLib::arrayToString($completions,
            array('\TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib', 'gradeToString'));
    }

    public function donottestSessionsForGrades()
    {
        $grades   = self::$repository->completionsForUser(self::DEBORAH_CAGE_ID);
        $sessions = self::$repository->sessionsForGrades($grades);
        echo EntityToStringLib::arrayToString($sessions,
            array('\TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib', 'courseSessionToString'));
    }

    public function testF()
    {
        $grades   = self::$repository->completionsForUser(self::DEBORAH_CAGE_ID);
        $sessions = self::$repository->sessionsForGrades($grades);

        $gradesWithSessions = EntityLib::addSessionsToGrades($grades, $sessions);
        echo EntityToStringLib::arrayToString($gradesWithSessions,
            array('\TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib', 'gradeToString'));
    }


    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:Grade';
    }
}