<?php

namespace TSG\MoodleLMSBundle\Tests\Misc;


use TSG\MoodleLMSBundle\Entity\Course;
use TSG\MoodleLMSBundle\Entity\CourseAssignment;
use TSG\MoodleLMSBundle\Entity\Grade;
use TSG\MoodleLMSBundle\Entity\GradeItem;
use TSG\MoodleLMSBundle\Entity\User;
use TSG\MoodleLMSBundle\Entity\Lib\EntityLib;

class EntityLibTest extends \PHPUnit_Framework_TestCase
{
    public function testCompletionGradeForAssignment()
    {
        $assignment = $this->buildAssignment();
        $assignment->setGrades(array_merge(
            $this->incompleteGradesForAssignment($assignment),
            array($this->completedGradeForAssignment($assignment))
        ));

        $completionGrade = EntityLib::completionGradeForAssignment($assignment);
        $this->assertNotNull($completionGrade,
            'expected a completion grade for a completed assignment');


        $assignment = $this->buildAssignment();

        $completionGrade = EntityLib::completionGradeForAssignment($assignment);
        $this->assertFalse($completionGrade,
            'expected no completion grade for an assignment with no grades');


        $assignment = $this->buildAssignment();
        $assignment->setGrades($this->incompleteGradesForAssignment($assignment));

        $completionGrade = EntityLib::completionGradeForAssignment($assignment);
        $this->assertFalse($completionGrade,
            'expected no completion grade for an assignment with only incomplete grades');
    }


    private function buildAssignment()
    {
        $assignment = new CourseAssignment();
        $assignment->setDeadline(strtotime('2012-06-30'));

        $user = new User();
        $user->setFirstName('test');
        $user->setLastName('user');
        $assignment->setUser($user);

        $course = new Course();
        $course->setFullName('test course');

        return $assignment;
    }

    private function incompleteGradesForAssignment($assignment)
    {
        $user               = $assignment->getUser();
        $course             = $assignment->getCourse();
        $completionIdNumber = date('Y', $assignment->getDeadline()) . '.test';
        $inCompleteDate     = $assignment->getDeadline() - 2*24*60*60; // two days before deadline

        $grades = array();

        $grades[] = $this->buildGrade($user, $course, 'n/a', 80, 70, time());
        $grades[] = $this->buildGrade($user, $course, $completionIdNumber, 80, 70,
            $inCompleteDate);

        return $grades;
    }

    private function completedGradeForAssignment($assignment)
    {
        $user               = $assignment->getUser();
        $course             = $assignment->getCourse();
        $completionIdNumber = date('Y', $assignment->getDeadline()) . '.test';
        $completionDate     = $assignment->getDeadline() - 1*24*60*60; // one day before deadline

        return $this->buildGrade($user, $course, $completionIdNumber, 80, 90, $completionDate);
    }

    private function buildGrade($user, $course, $idNumber, $gradePass, $finalGrade, $timeModified)
    {
        $g = new Grade();
        $g->setUser($user);

        $gi = new GradeItem();
        $gi->setCourse($course);
        $gi->setIdNumber($idNumber);
        $gi->setGradePass($gradePass);
        $g->setItem($gi);

        $g->setFinalGrade($finalGrade);
        $g->setTimeModified($timeModified);

        return $g;
    }


    /**
     * @param Grade $grade
     * @return string
     */
    private function gradeToString($grade)
    {
        if (!$grade) {
            return 'n/a';
        }

        $id = $grade->getId();
        return '    ' . ($id ? $id : 0)
        . ': idNumber = '     . $grade->getItem()->getIdNumber()
        . ', gradePass = '    . $grade->getItem()->getGradePass()
        . ', finalGrade = '   . $grade->getFinalGrade()
        . ', timeModified = ' . $grade->getTimeModified();
    }
}