<?php
namespace TSG\MoodleLMSBundle\Tests\Entity;

class CourseCategoryRepositoryFunctionalTest extends BaseEntityRepositoryTest
{
    const CLASSNAME = 'CourseCategoryRepositoryFunctionalTest';

    public function testX()
    {
        echo self::CLASSNAME.'::testX()'.PHP_EOL;

        $qb = self::$repository->createQueryBuilder('cc');
        $categories = $qb
            ->where($qb->expr()->between('cc.id', 10, 20))
            ->getQuery()
            ->getResult();

        echo 'category: '.$categories[0]->getName().PHP_EOL;
    }

    protected static function getEntityName()
    {
        return 'TSGMoodleLMSBundle:CourseCategory';
    }
}