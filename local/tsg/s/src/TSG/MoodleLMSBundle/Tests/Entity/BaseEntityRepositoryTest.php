<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 11/20/14
 * Time: 6:45 PM
 */

namespace TSG\MoodleLMSBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class BaseEntityRepositoryTest extends KernelTestCase
{
    protected static $em;
    protected static $repository;

    public static function setUpBeforeClass()
    {
        define('CLI_SCRIPT', true);

        // echo 'booting kernel'.PHP_EOL;
        self::bootKernel();
        $GLOBALS['kernel'] = static::$kernel;

        // echo 'getting Entity Manager'.PHP_EOL;
        self::$em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();


        // echo 'getting repository'.PHP_EOL;
        self::$repository = static::$kernel->getContainer()
            ->get('doctrine')
            ->getRepository(static::getEntityName());
    }

    public static function tearDownAfterClass()
    {
        static::ensureKernelShutdown();
        self::$em->close();
    }

    protected function log($class, $method, $message='')
    {
        echo $class . '::' . $method . $message . PHP_EOL;
        ob_flush();
    }

    protected static function getEntityName() { return 'TSGMoodleLMSBundle:CourseAssignment'; }
} 