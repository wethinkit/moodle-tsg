<?php

namespace TSG\MoodleLMSBundle\Tests\Lib;


use TSG\MoodleLMSBundle\Entity\CourseAssignment;
use TSG\MoodleLMSBundle\Entity\CourseSession;
use TSG\MoodleLMSBundle\Entity\Grade;

class EntityToStringLib
{
    public static function arrayToString(array $values, $fn, $indent='', $string='')
    {
        if (empty($values)) {
            return $string;
        }

        $v = $values[0];
        $string .= $indent . call_user_func($fn, $v, $indent) . PHP_EOL;

        return self::arrayToString(array_slice($values, 1), $fn, $indent, $string);
    }


    public static function assignmentToString(CourseAssignment $assignment)
    {
        $value = $assignment->getId() . ': userId = ' . $assignment->getUser()->getId()
            . ', courseId = ' . $assignment->getCourse()->getId()
            . ', deadline = ' . $assignment->getDeadline()
            . ', completionDate = ' . $assignment->getCompletionDate();


        $grades = $assignment->getGrades();
        if (!empty($grades)) {
            $value .= PHP_EOL . self::arrayToString($grades,
                array('\TSG\MoodleLMSBundle\Tests\Lib\EntityToStringLib', 'gradeToString'), '    ');
        }

        return $value;
    }

    public static function gradeToString(Grade $grade)
    {
        $id = $grade->getId();
        $value = ($id ? $id : 0)
            . ': idNumber = '     . $grade->getItem()->getIdNumber()
            . ', gradePass = '    . $grade->getItem()->getGradePass()
            . ', finalGrade = '   . $grade->getFinalGrade()
            . ', timeModified = ' . $grade->getTimeModified();

        $session = $grade->getItem()->getSession();
        if ($session) {
            $value .= PHP_EOL . self::courseSessionToString($session, '    ');
        }
        return $value;
    }

    public static function courseSessionToString(CourseSession $session, $indent='')
    {
        return $indent . $session->getId()
            . ': start = '    . date('Y.m.d', $session->getStartDate())
            . ': end = '      . date('Y.m.d', $session->getEndDate())
            . ': schedule = ' . $session->getSchedule()
            . ': location = ' . $session->getLocation();
    }
}