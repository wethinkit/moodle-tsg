<?php

namespace TSG\MoodleLMSBundle\Tests;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TSGKernelTestCase extends KernelTestCase
{
    public static function setUpBeforeClass()
    {
        define('CLI_SCRIPT', true);

        // echo 'booting kernel'.PHP_EOL;
        self::bootKernel();
        $GLOBALS['kernel'] = static::$kernel;
    }
} 