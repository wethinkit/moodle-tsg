<?php

namespace TSG\ScratchBundle\Controller;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use TSG\ScratchBundle\Entity\User;

class UserController extends BaseController
{
    const REPOSITORY = 'TSGScratchBundle:User';


    /**
     * @View()
     */
    public function getUserAction($id)
    {

        $user = $this->em->getRepository(self::REPOSITORY)
            ->find($id);
        return $user;

        /*
        $view = View::create($user)
            ->setTemplate('TSGScratchBundle:Default:dbal.html.twig');

        return $this->viewHandler->handle($view);
        */
    }

    /**
     * @return array
     *
     * @View
     */
    public function getUsersAction()
    {
        $users = $this->em->createQueryBuilder()
            ->select('u')
            ->from('TSGScratchBundle:User', 'u')
            ->setFirstResult(0)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
        // echo '<pre>$users:'.print_r($users, true).'</pre>';
        return $users;
    }
} 