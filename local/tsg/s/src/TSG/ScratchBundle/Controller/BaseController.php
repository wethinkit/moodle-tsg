<?php
namespace TSG\ScratchBundle\Controller;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;
use TSG\ScratchBundle\HAL\HALViewHandler;
use TSG\MoodleGlobalsBundle\Facade\MoodleGlobals;
use Doctrine\ORM\EntityManagerInterface as EntityManager;

class BaseController {

    protected $moodleGlobals;
    protected $em;
    protected $viewHandler;
    protected $templating;
    protected $router;

    public function __construct(
        MoodleGlobals $moodleGlobals,
        EntityManager $em,
        HALViewHandler $viewHandler,
        EngineInterface $templating,
        RouterInterface $router)
    {
        $this->moodleGlobals = $moodleGlobals;
        $this->em = $em;
        $this->viewHandler = $viewHandler;
        $this->templating = $templating;
        $this->router = $router;
    }
} 