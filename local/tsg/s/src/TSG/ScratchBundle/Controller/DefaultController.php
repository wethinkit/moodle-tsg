<?php

namespace TSG\ScratchBundle\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use TSG\MoodleGlobalsBundle\Facade\MoodleGlobals;
use TSG\ScratchBundle\HAL\HALViewHandler;

class DefaultController extends BaseController
{
    public function index($name)
    {
        require_login();

        $output = $this->moodleGlobals->getOutput();
        $page = $this->moodleGlobals->getPage();

        $page->set_context(\context_system::instance());
        $page->set_title('Reports');
        $page->set_heading('Reports');
        $page->set_cacheable(false);
        $page->navbar->add('Reports');

        $content  = $output->header();
        $content .= $this->templating->render('TSGScratchBundle:Default:index.html.twig', array('name' => $name));
        $content .= $output->footer();

        return new Response($content);
    }

    public function dbal($userid)
    {
        // $users = $this->getUsersViaORM($userid);

        $output = $this->moodleGlobals->getOutput();
        $page = $this->moodleGlobals->getPage();

        $page->set_context(\context_system::instance());

        $urls = new \stdClass();
        $urls->user = new \stdClass();
        $urls->user->list = $this->router->generate('tsg_scratch.user.list');
        $page->requires->yui_module('moodle-local_tsg-scratch', 'M.local_tsg.scratch.init', array($urls));

        $page->set_title('DBAL');
        $page->set_heading('DBAL');
        $page->set_cacheable(false);
        $page->navbar->add('DBAL');

        $content  = $output->header();
        $content .= $this->templating->render('TSGScratchBundle:Default:dbal.html.twig');
        $content .= $output->footer();

        return new Response($content);
    }

    public function cAction()
    {
        $yo = new \Yo();
        $message = $yo->message();
        return new Response($message);
    }

    private function getUsersViaDBAL()
    {
        $config = new \Doctrine\DBAL\Configuration();

        $connectionParams = array(
            'dbname' => 'mydb',
            'user' => 'user',
            'password' => 'secret',
            'host' => 'localhost',
            'driverClass' => 'TSG\\MoodleDoctrineDBALBundle\\Driver\\Driver',
        );

        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        $users = array();
        $stmt = $conn->prepare('select id,lastname,firstname from user where id = ?');
        $stmt->bindValue(1, 2);
        $stmt->execute();
        while ($row = $stmt->fetch()) {
            $users[] = array(
                'id' => $row['id'],
                'lastname' => $row['lastname'],
                'firstname' => $row['firstname']
            );
        }

        return $users;
    }

    private function getUsersViaORM($userid)
    {
        $user = $this->em->getRepository('TSGScratchBundle:User')
            ->find($userid);

        return array($user);
    }
}
