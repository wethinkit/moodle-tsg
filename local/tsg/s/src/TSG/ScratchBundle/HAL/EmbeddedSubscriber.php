<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 9/5/14
 * Time: 6:41 PM
 */

namespace TSG\ScratchBundle\HAL;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use TSG\ScratchBundle\HAL\Resource\ResourceList;

class EmbeddedSubscriber implements EventSubscriberInterface {

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            array('event' => 'serializer.post_serialize',
                  'method' => 'embedResources',
                  'class' => 'TSG\\ScratchBundle\\HAL\\Resource\\ResourceList',
                  'format' => 'json'
            )
        );
    }

    public function embedResources(ObjectEvent $objectEvent)
    {
        $object = $objectEvent->getObject();

        $visitor = $objectEvent->getVisitor();

        $visitor->addData('serializer.post_serialize', 'nothing');
    }
}