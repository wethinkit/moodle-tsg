<?php

namespace TSG\ScratchBundle\HAL\Resource;

use Symfony\Component\Routing\RouterInterface;
use JMS\Serializer\Annotation as JMSAnnotation;

abstract class Resource
{
    /**
     * @var mixed
     *
     * @JMSAnnotation\Inline()
     */
    protected $entity;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     * @JMSAnnotation\Exclude()
     */
    protected $router;

    /**
     * @var array HAL links
     */
    protected $_links;

    public function __construct($entity, RouterInterface $router)
    {
        $this->entity = $entity;
        $this->router = $router;
        $this->_links = array();
    }

    public abstract function prepare();
} 