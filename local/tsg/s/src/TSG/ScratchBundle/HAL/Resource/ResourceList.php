<?php
/**
 * Created by PhpStorm.
 * User: sjones
 * Date: 9/4/14
 * Time: 4:12 PM
 */

namespace TSG\ScratchBundle\HAL\Resource;

use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\Routing\RouterInterface;
use JMS\Serializer\Annotation as JMSAnnotation;

class ResourceList
{
    const ROUTE = 'tsg_scratch.user.list';


    /**
     * @var mixed
     * @JMSAnnotation\Exclude()
     */
    protected $resources;

    /**
     * @var string type of resource in $this->resources
     * @JMSAnnotation\Exclude()
     */
    protected $resourceTypeName;

    /**
     * @JMSAnnotation\VirtualProperty()
     * @JMSAnnotation\SerializedName("_embedded")
     */
    public function getEmbeddedResources()
    {
        return array($this->resourceTypeName => $this->resources);
    }

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     * @JMSAnnotation\Exclude()
     */
    protected $router;

    /**
     * @var array HAL links
     */
    protected $_links;


    public function __construct(array $entities, RouterInterface $router)
    {
        $this->resourceTypeName = lcfirst($entities[0]->getEntityName());

        $resourceClass = __NAMESPACE__.'\\'.$entities[0]->getEntityName().'Resource';
        $this->resources = array();
        foreach ($entities as $e) {
            $this->resources[] = new $resourceClass($e, $router);
        }

        $this->router = $router;
        $this->_links = array();
    }


    public function prepare()
    {
        $this->_links['self'] = array(
            'href' => $this->router->generate(self::ROUTE)
        );

        foreach ($this->resources as $r) {
            $r->prepare();
        }
    }
} 