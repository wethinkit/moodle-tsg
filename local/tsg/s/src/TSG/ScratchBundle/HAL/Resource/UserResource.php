<?php

namespace TSG\ScratchBundle\HAL\Resource;

use Symfony\Component\Routing\RouterInterface;
use JMS\Serializer\Annotation as JMSAnnotation;
use TSG\ScratchBundle\Entity\Entity;
use TSG\ScratchBundle\Entity\Id;

class UserResource extends Resource {
    const ROUTE = 'tsg_scratch.user';

    public function __construct(Entity $entity, RouterInterface $router)
    {
        parent::__construct($entity, $router);
    }

    public function prepare()
    {
        $this->_links['self'] = array(
            'href' => $this->router->generate(self::ROUTE, array('id' => $this->entity->getId()))
        );
    }
} 