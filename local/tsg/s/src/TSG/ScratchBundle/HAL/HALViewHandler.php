<?php

namespace TSG\ScratchBundle\HAL;

use stdClass;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface as ViewHandler;

use JMS\Serializer\Serializer;

use TSG\ScratchBundle\Entity;
use TSG\ScratchBundle\HAL\Resource\ResourceList;
use TSG\ScratchBundle\HAL\Resource\UserResource;

class HALViewHandler {

    private $serializer;
    private $router;

    public function __construct(ViewHandler $viewHandler, RouterInterface $router, Serializer $serializer)
    {
        $viewHandler->registerHandler('hal', array($this, 'handle'));

        $this->serializer = $serializer;
        $this->router = $router;
    }

    public function handle(ViewHandler $handler, View $view, $request)
    {
        $data = $view->getData();
        if ($data instanceof Entity\User) {
            $resource = new UserResource($data, $this->router);
        }
        else if (is_array($data)) {
            $resource = new ResourceList($data, $this->router);
        }

        $resource->prepare();
        $representation = $this->serializer->serialize($resource, 'json');

        return new Response($representation, 200, $view->getHeaders());
    }
} 