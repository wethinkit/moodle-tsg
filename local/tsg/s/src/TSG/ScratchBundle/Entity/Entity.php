<?php

namespace TSG\ScratchBundle\Entity;

interface Entity extends Id
{
    public function getEntityName();
    public function getPluralEntityName();
}