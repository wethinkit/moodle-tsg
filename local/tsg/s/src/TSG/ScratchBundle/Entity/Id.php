<?php

namespace TSG\ScratchBundle\Entity;

interface Id
{
    function getId();
}