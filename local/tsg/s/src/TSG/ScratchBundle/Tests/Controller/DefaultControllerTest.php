<?php

namespace TSG\ScratchBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function __construct()
    {
        echo "defining CLI_SCRIPT".PHP_EOL;
        define('CLI_SCRIPT', true);
    }

    public function setUp()
    {

    }

    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertTrue($crawler->filter('html:contains("TSG")')->count() > 0);
    }
}
