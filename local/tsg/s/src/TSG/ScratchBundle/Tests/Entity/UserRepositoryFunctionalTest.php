<?php
namespace TSG\ScratchBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class UserRepositoryFunctionalTest extends KernelTestCase
{

    private $em;

    public function setUp()
    {
        define('CLI_SCRIPT', true);
        self::bootKernel();
        $GLOBALS['kernel'] = static::$kernel;
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }


    public function testX()
    {
        $users = $this->em->createQueryBuilder()
            ->select('u')
            ->from('TSGScratchBundle:User', 'u')
            ->setFirstResult(0)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
        echo '<pre>$users:'.print_r($users, true).'</pre>';

        $this->assertEquals(1, 1);
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }
}