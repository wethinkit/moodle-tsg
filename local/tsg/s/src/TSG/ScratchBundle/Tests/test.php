<?php

$assignments = build_assignments2();
$grades = build_grades();

$a_iter = new ArrayIterator($assignments);
$g_iter = new ArrayIterator($grades);

$g = $g_iter->current();
foreach ($a_iter as $a) {
    echo 'for:  a.u' . $a->userId . '.c' . $a->courseId .
        ', g' . $g->id . '.u' . $g->userId . '.c' . $g->item->courseId . PHP_EOL;

    if (!$g_iter->valid() || $a->userId < $g->userId || $a->courseId < $g->item->courseId) {
        echo 'marking incomplete' . PHP_EOL;
        echo 'next assignment' . PHP_EOL;
        $a->isComplete = 'false';
        continue;
    }

    if ($a->userId > $g->userId || $a->courseId > $g->item->courseId) {
        echo 'next grade' . PHP_EOL;
        do {
            $g_iter->next();
            if (!$g_iter->valid()) {
                break;
            }
            $g = $g_iter->current();
            echo 'do:   a.u' . $a->userId . '.c' . $a->courseId .
                ', g' . $g->id . '.u' . $g->userId . '.c' . $g->item->courseId . PHP_EOL;
        }
        while ($a->userId > $g->userId || $a->courseId > $g->item->courseId);

        if (!$g_iter->valid()) {
            echo 'marking incomplete' . PHP_EOL;
            $a->isComplete = 'false';
            continue;
        }

        $g = $g_iter->current();
        if ($a->userId < $g->userId || $a->courseId < $g->item->courseId) {
            continue;
        }
    }

    do {
        echo 'geq:  a.u' . $a->userId . '.c' . $a->courseId .
            ', g' . $g->id . '.u' . $g->userId . '.c' . $g->item->courseId .
            ' -- fg' . $g->finalGrade . '.gp' . $g->item->gradePass . PHP_EOL;

        if (!property_exists($a, 'isComplete') && $g->finalGrade >= $g->item->gradePass) {
            echo 'marking complete' . PHP_EOL;
            $a->isComplete = 'true';
        }

        $g_iter->next();
        if (!$g_iter->valid()) {
            break;
        }

        $g = $g_iter->current();
    }
    while ($a->userId == $g->userId && $a->courseId == $g->item->courseId);

    if (!property_exists($a, 'isComplete')) {
        echo 'marking incomplete' . PHP_EOL;
        $a->isComplete = 'false';
    }
}

print_r($assignments);

function build_assignments1()
{
    $assignments = array();

    $a = new stdClass;
    $a->userId = 1;
    $a->courseId = 1;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 1;
    $a->courseId = 2;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 2;
    $a->courseId = 1;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 2;
    $a->courseId = 2;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 3;
    $a->courseId = 2;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 4;
    $a->courseId = 1;
    $assignments[] = $a;

    return $assignments;
}

function build_assignments2()
{
    $assignments = array();

    $a = new stdClass;
    $a->userId = 1;
    $a->courseId = 1;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 2;
    $a->courseId = 1;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 3;
    $a->courseId = 1;
    $assignments[] = $a;

    $a = new stdClass;
    $a->userId = 4;
    $a->courseId = 1;
    $assignments[] = $a;

    return $assignments;
}

function build_grades()
{
    $grades = array();

    $g = new stdClass;
    $g->id = 1;
    $g->userId = 2;
    $g->finalGrade = 70;
    $g->item = new stdClass;
    $g->item->courseId = 1;
    $g->item->gradePass = 80;
    $grades[] = $g;

    $g = new stdClass;
    $g->id = 2;
    $g->userId = 2;
    $g->finalGrade = 90;
    $g->item = new stdClass;
    $g->item->courseId = 1;
    $g->item->gradePass = 80;
    $grades[] = $g;

    $g = new stdClass;
    $g->id = 3;
    $g->userId = 2;
    $g->finalGrade = 50;
    $g->item = new stdClass;
    $g->item->courseId = 2;
    $g->item->gradePass = 80;
    $grades[] = $g;

    $g = new stdClass;
    $g->id = 4;
    $g->userId = 3;
    $g->finalGrade = 85;
    $g->item = new stdClass;
    $g->item->courseId = 2;
    $g->item->gradePass = 80;
    $grades[] = $g;

    return $grades;
}