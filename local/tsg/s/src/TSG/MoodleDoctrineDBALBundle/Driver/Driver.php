<?php

namespace TSG\MoodleDoctrineDBALBundle\Driver;

use Doctrine\DBAL\Driver as DriverInterface;

class Driver implements DriverInterface {

    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = array())
    {
        global $kernel;
        $kernel->boot();
        $moodleGlobals = $kernel->getContainer()->get('tsg_moodle_globals.facade');
        return new MoodleConnection($moodleGlobals->getDb());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'moodle';
    }

    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(\Doctrine\DBAL\Connection $conn)
    {
        return new \Doctrine\DBAL\Schema\MySqlSchemaManager($conn);
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
        return new \Doctrine\DBAL\Platforms\MySqlPlatform();
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabase(\Doctrine\DBAL\Connection $conn)
    {
        $params = $conn->getParams();

        return $params['dbname'];
    }
} 