<?php
// define('ABORT_AFTER_CONFIG', true);
require_once('../../../../../config.php');
require_login();

$service = new TSG\Accelerator\UserService();
$content = json_encode($service->findByPattern($_REQUEST['pattern']));

header('Content-Type: application/json', true, 200);
echo $content;