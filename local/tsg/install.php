<?php
/**
 * @package    local_tsg
 * @copyright  2015 The Sententia Group, LLC
 */

define('CLI_SCRIPT', true);
require(dirname(dirname(dirname(__FILE__))).'/config.php');

$retval = 'not initialized';
system('chown -R www-data:www-data /var/lib/moodle', $retval);
echo "chown -R www-data:www-data /var/lib/moodle returned: $retval".PHP_EOL;

set_config('theme', 'tsg');
echo 'configured site to use TSG theme'.PHP_EOL;

set_config('frontpage', '');
set_config('frontpageloggedin', '');
echo 'configured front page to use TSG Catalog/Catalog/My Courses/My Transcripts control'.PHP_EOL;

global $DB;
$rootusergroup = new stdClass();
$rootusergroup->name = 'root';
$rootusergroup->isprofilegroup = 0;
$rootusergroup->id = $DB->insert_record('lsc_user_group', $rootusergroup);
echo 'added root user group to user_group table'.PHP_EOL;
