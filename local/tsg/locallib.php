<?php

function local_tsg_build_navigation_node($labelkey, $urlpath, $iconpath)
{
    $label = get_string($labelkey, 'local_tsg');
    $url = new moodle_url($urlpath);

    $node = navigation_node::create(
        $label,
        $url,
        navigation_node::NODETYPE_LEAF,
        $label,
        $labelkey,
        new pix_icon($iconpath, $label)
    );

    $node->make_active();
    return $node;
}

function local_tsg_index_array(array $array, array $keys)
{
    $indexedArray = array();

    array_walk($array, function ($value) use (&$indexedArray, $keys) {

        $keysToIndexedArray =
            array_map(function ($k) use ($value) {
                return $value[$k];
            }, $keys);

        $element = &$indexedArray;
        foreach ($keysToIndexedArray as $k) {
            $element = &$element[$k];
        }
        $element[] = $value;
    });
    return $indexedArray;
}

function local_tsg_isadmin()
{
    global $USER;
    $admin = get_admins();
    foreach ($admin as $a) {
        if ($USER->id === $a->id) {
            return true;
        }
    }
    return false;
}


/**
 * Adds a header and checkbox group to $mform for tags.  The tags checkbox
 * group is named local_tsg_tags.  Each checkbox in the group is named
 * local_tsg_tags[tagid].
 *
 * @param TSG\MoodleLMSBundle\Entity\Tag[] $tags
 * @param $mform
 * @throws coding_exception
 */
function local_tsg_add_tags_to_form($tags, &$mform)
{
    /*
     *
     */
    $mform->addElement('header', 'local_tsg_tags_hdr',
        get_string('tags', 'local_tsg'));

    $checkboxes = array();
    array_walk($tags,
        function ($t) use ($mform, &$checkboxes)
        {
            $checkboxes[] = $mform->createElement(
                'checkbox', strval($t->getId()), null, $t->getName());
        }
    );
    $mform->addGroup($checkboxes, 'local_tsg_tags');
}