<?php
/**
* Version information
*
* @package    local_tsg
* @copyright 2015 onwards The Sententia Group, LLC {@link http://sententiagroup.com}
*/

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_tsg'; // Full name of the plugin (used for diagnostics).
$plugin->version  = 2015050700;    // The current module version (Date: YYYYMMDDXX).
$plugin->requires = 2013050100;    // Requires this Moodle version.