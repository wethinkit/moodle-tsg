<?php
/**
 * Plugin capabilities
 *
 * @package    local_tsg
 * @copyright  2015 The Sententia Group, LLC
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'moodle/lsc:assignusers' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE
    ),

    'moodle/lsc:create_lsc_course_section' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE
    ),

    'moodle/lsc:edit_lsc_course_section' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE
    ),

    'moodle/lsc:delete_lsc_course_section' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE
    ),

    'moodle/lsc:view_lsc_course_section_roster' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE
    ),

    'moodle/lsc:run_reports' => array(

        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM
    ),

    'moodle/lsc:view_user_directory' => array(

        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM
    ),

    'moodle/lsc:view_all_users' => array(

        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM
    ),

    'moodle/lsc:manage_settings' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM
    ),

    'moodle/lsc:manage_groups' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM
    )
);