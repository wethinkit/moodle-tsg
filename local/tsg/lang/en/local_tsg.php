<?php

$string['pluginname'] = 'TSG Moodle Customizations';

$string['assignments']   = 'Assignments';
$string['groups']        = 'Groups';
$string['profilegroups'] = 'Profile Groups';
$string['reports']       = 'Reports';
$string['sessions']      = 'Sessions';
$string['tags']          = 'Tags';
$string['users']         = 'Users';

$string['error.notnumeric'] = '{$a} must be numeric';

$string['courseinfo.header'] = 'Course Info';
$string['courseinfo.hours.label'] = 'Hours';
$string['courseinfo.credits.label'] = 'Credits';
$string['courseinfo.costperstudent.label'] = 'Cost (per student)';

$string['userprofile.status'] = 'Status';
$string['userprofile.hiredate'] = 'Hire Date (m/d/yyyy)';
$string['userprofile.terminationdate'] = 'Termination Date (m/d/yyyy)';
$string['userprofile.inactive'] = 'Inactive?';


$string['problem.notloggedin.title'] = 'Not Logged In';

$string['mycourses.problem.notloggedin.detail'] = 'You must be logged in to see My Courses';
$string['mytranscript.problem.notloggedin.detail'] = 'You must be logged in to see My Transcript';

