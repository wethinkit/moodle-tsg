Project Notes
=============

## Creating a new API request

1.  Optional:  add new HAL\Resource class if needed.  Specify route(s) for self link generation
2.  Optional:  add new Controller class and service definition to services.yml.
3.  Add a new route to routing.yml.
4.  Update UrlService with new route as well.
5.  Optional: add Entity repository method to support necessary query if more complex than builtins.
6.  Add controller method.
    a.  annotate with @View
    b.  return a Container (e.g. EntitiesContainer)
    c.  may need to specify MaxDepthExclusionStrategy or RulesExclusionStrategy
    
    
## Rewrite rules for Symfony URLs

*NOTE:*  If rewrite rules are not used, you **must** adjust the context root settings to include 
the front controller filename (see Context Root section below).

The nginx configuration directives below do the following:

1.  Redirect all requests for the front controller (app.php) to a url stripped of the front
controller file name. (e.g. /local/tsg/s/web/app.php/reports becomes /local/tsg/s/web/reports).

2.  try_files attempts to serve the requested url from the file system (e.g. for images) and sends
you to @rewriteapp if there is no matching file.  The assumption is that, in this case, the request
must be intended for the front controller.

3.  location @rewriteapp rewrites the url, putting the front controller file name in.

Similar rules should be setup for Apache using mod_rewrite.


```
rewrite ^/local/tsg/s/web/app_dev\.php/?(.*)$ /local/tsg/s/web/$1 permanent;
        
try_files $uri @rewriteapp;

location @rewriteapp {
    rewrite ^/local/tsg/s/web/(.*)$ /local/tsg/s/web/app_dev.php/$1 last;
}
```

## Context Root

Defined in:

1.  local/tsg/config.php                      :  `define('LOCAL_TSG_CONTEXTROOT', '/local/tsg/s/web');`
2.  local/tsg/yui/src/defaults/js/defaults.js :  `contextRoot: 'local/tsg/s/web/app_dev.php'`

## Local Plugin config.php

This file defines constants used throughout the application.  It is automatically required by two
scripts:

1.  The front controllers :  `local/tsg/s/web/app.php` and `local/tsg/s/web/app_dev.php`.
2.  The local lib file    :  `local/tsg/locallib.php`



## Filtering Records for Training Coordinators


### lsc_user_allowed_groups

This table defines to which user groups a given user (training coordinator) has access.

| Field            | Type         | Null | Description
| ---------------- | ------------ | ---- | ----------------------------------------------------
| id                | int(11)     | no   | auto generated
| user_id           | int(11)     | no   | &nbsp;
| lsc_user_group_id | int(11)     | no   | &nbsp;


### Areas

- User autocomplete fields
- Assignment Search
- Reports


### Controller Methods

#### User autocomplete fields

- `TSG\MoodleLMSBundle\Controller\UserController::findByPatternAction`
- `TSG\MoodleLMSBundle\Controller\UserController::findByPatternUsingAccelerator`

#### Assignment Search

- `TSG\MoodleLMSBundle\Controller\AssignmentManagementController::searchAction`

#### Reports

- `TSG\MoodleLMSBundle\Controller\ReportController::runAction`