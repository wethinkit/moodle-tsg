Installing and Updating TSG's Customized Moodle
===============================================

## Introduction and Definitions

Moodle is a PHP web application.  As such, installation is largely a matter of installing its PHP scripts and web assets (e.g. images, javascript, style sheets) to a publically accessible directory on a web server that supports PHP execution.

Some definitions to make the installation instructions below clear:

- `moodledir`:  the directory into which Moodle is installed (e.g. `/srv/www/htdocs`)
- `moodledatadir`:  directory that Moodle uses to store course assets (e.g. videos, Power Points, SCORM files).  This is a potentially large directory and is usually located somewhere in `/var`.
- `config.php`:  Moodle's main config file.  This file is located at the root of `moodledir` and specifies the following:
    - db connection parameters (e.g. host name, db username, db password)
    - location of `moodledatadir`
    - url of Moodle home page (used to make absolute url's out of relative ones)
    - `password salt` value (used to make passwords more secure)
- `plugin`:  code that conforms to the published API of a well defined Moodle integration point.  An example would be activity plugins within courses (e.g. quizzes).
    
For the most part, the TSG additions to Moodle are packaged as a Moodle `plugin` -- specifically a `local plugin`.  The plugin's name is `tsg` and it is installed in `<moodledir>/local/tsg`.

There are also a few cusomizations which have been made to Moodle core code outside of `<moodledir>/local/tsg` but most custom code is packaged in the `local plugin`. 


## Instructions

There are two different types of installation pacages:

1.  Full
2.  Local plugin only

### Full

This is a full installation of Moodle, including the TSG `local plugin`.  To install:

1.  Backup the `<moodledir>/config.php` file.  This is critical, mostly for the `password salt`
    value.  If this value is lost, all users will be locked out until their passwords can be reset
    manually in the database.
2.  Extract the installation file into `<moodledir>`.
3.  Restore `config.php`.
4.  From `<moodledir>`, run `/home/sjones/symfony.sh`.  This step is temporary and will be replaced
    in the future.
5.  Restart the PHP process(es).  For Apache with mod_php, this means to restart Apache.  For 
    php-fpm setups, this involves restarting php-fpm.
5.  Login to Moodle as *admin*.  You will be prompted to upgrade the TSG `local plugin`.  This will
    execute any necessary database structure changes required by the new version.

### Local plugin only

This is an installation of only the TSG `local plugin` onto an existing Moodle installation.  
Follow the exact same procedure as for a full installation of Moodle.  The only difference is that
the installation package includes only files in `<moodledir>/local/tsg`.