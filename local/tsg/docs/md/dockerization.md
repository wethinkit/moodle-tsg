Steps
=====
1.  Install through creating admin account.
4.  When too many redirects error occurs, purge moodle caches and continue.
	- docker exec dcfs_php_1 php /srv/www/admin/cli/purge_caches.php
5.  Insert root category into lsc_user_group table.
6.  Set front page center to "None".
7.  Change theme to tsg.


To Do
=====
x1.  Integrate date.timezone setting into php init.sh in Docker image.
x2.  Add course catalog / my courses / my transcript widget to front page.
x3.  Try setup with tsg db driver set to allow db sessions.
4.  Troubleshoot groups page javascript error: 
    - groups is undefined in self.displayGroups
    - there are no top level groups so _embedded is empty
    - probably just insert some top level groups into db
    - or adjust code to handle no top level groups
5.  Move db/capability creation to local/tsg/db

