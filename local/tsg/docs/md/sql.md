Title
=====

## Most Frequently Used Tables

| Custom Tables                                           |  Moodle Tables
| ------------------------------------------------------- | -------------- 
| [lsc_course_info](#lsc_course_info)                     | course
| [lsc_user_info](#lsc_user_info)                         | grade_grades
| [lsc_course_section](#lsc_course_section)               | grade_items 
| [lsc_course_section_enroll](#lsc_course_section_enroll) | user 
| lsc_training_area                                       | &nbsp;
| lsc_course_training_area                                | &nbsp;
| lsc_user_training_area                                  | &nbsp;
| lsc_user_group                                          | &nbsp;
| lsc_user_group_group                                    | &nbsp;
| lsc_user_group_user                                     | &nbsp;
| lsc_user_allowed_groups                                 | &nbsp;


<a id="lsc_course_info">
### lsc_course_info 

This table is used to store custom information about courses that Moodle does not include.  There
 is a one to one relationship with the Moodle `course` table:
`lsc_course_info.course_id = course.id`.

| Field            | Type         | Null | Description
| ---------------- | ------------ | ---- | -----------------------
| id               | bigint(20)   | no   | auto generated
| course_id        | bigint(20)   | no   | references course.id
| hours            | decimal(6,2) | yes  | credit hours
| credits          | decimal(6,2) | yes  | ceu's
| cost_per_student | decimal(7,2) | yes  | cost accounting measure

##### SQL for Courses with Info
To list `courses` along with each's corresponding `lsc_course_info`, simply join the two tables.  
For example:

```sql
select c.id, c.fullname, i.hours, i.credits, i.cost_per_student
from course c join lsc_course_info i on i.course_id = c.id
limit 5
```

```
+----+---------------------------------------------------------------------+-------+---------+------------------+
| id | fullname                                                            | hours | credits | cost_per_student |
+----+---------------------------------------------------------------------+-------+---------+------------------+
|  2 | Supervision - Module 2, Achieving Excellence Through Supervision    | 11.00 |   11.00 |             0.00 |
|  6 | Basic First Aid                                                     |  4.00 |    0.00 |             0.00 |
| 10 | Child Sexual Abuse: Investigation and Assessment                    | 18.00 |    0.00 |             0.00 |
| 11 | Clerical and Paraprofessional Support Staff Training                |  6.00 |    0.00 |             0.00 |
| 12 | Coaching/Mentoring Supervisors Initiative                           |  5.00 |    0.00 |             0.00 |
+----+---------------------------------------------------------------------+-------+---------+------------------+
```


<a id="lsc_user_info">
### lsc_user_info

This table is used to store custom information about users that Moodle does not include.  There is a
 one to one relationship the the Moodle `user` table:  `lsc_user_info.user_id = user.id`.

| Field            | Type         | Null | Description
| ---------------- | ------------ | ---- | ---------------------------------------------
| id               | bigint(20)   | no   | auto generated
| user_id          | bigint(20)   | no   | references user.id
| hire_date        | int(11)      | yes  | unix timestamp
| termination_date | int(11)      | yes  | unix timestamp
| isactive         | tinyint(4)   | no   | 1 = active, 0 = inactive, default = 1
| idnum            | varchar(30)  | yes  | link to an external system's id for this user

##### SQL for Users with Info
To list `users` along with each's corresponding `lsc_user_info`, simply join the two tables.  
For example:

```sql
select u.id, u.lastname, u.firstname, u.email, from_unixtime(i.hire_date) as hire_date
from user u join lsc_user_info i on i.user_id = u.id
where hire_date is not null
limit 5
```

```
+-------+-------------------+-----------+----------------------------------+---------------------+
| id    | lastname          | firstname | email                            | hire_date           |
+-------+-------------------+-----------+----------------------------------+---------------------+
|  1008 | Adams             | Carla     | cbamonte@dss.state.la.us         | 1990-09-10 00:00:00 |
| 11637 | Owens             | Rosie     | Rosie.Owens@LA.GOV               | 2012-07-09 00:00:00 |
|   776 | Paxon             | Beverly   | Beverly.Paxon@LA.GOV             | 1997-12-01 00:00:00 |
|  4184 | Allen             | Judyette  | Judyette.Allen@LA.GOV            | 2002-05-13 00:00:00 |
|   987 | Smith (Delete Me) | Crystal   | dc78f9be1d81ee068240f2e4376d3afc | 2008-09-22 00:00:00 |
+-------+-------------------+-----------+----------------------------------+---------------------+
```


<a id="lsc_course_section">
### lsc_course_section

This table describes a particular instance of an instructor led course.  For example, if Safety 101 
is taught once a month, then there would be 12 records in this table in a given year -- one for each
monthly meeting.

| Field            | Type         | Null | Description
| ---------------- | ------------ | ---- | ----------------------------------------------------
| id               | bigint(20)   | no   | auto generated
| course_id        | int(11)      | no   | references course.id
| instructor       | varchar(255) | yes  | instructor name
| startdate        | int(11)      | yes  | unix timestamp
| enddate          | int(11)      | yes  | unix timestamp
| schedule         | varchar(255) | yes  | description of class meeting times (e.g. 8:00 am)
| location         | varchar(255) | yes  | e.g. Room 203
| capacity         | int(11)      | yes  | classroom capacity (informational only, not enforced


##### SQL for Sections in a Course

To see what sections are available for a given instructor led course, join `course` and this table.  
For example:
 
```sql
select c.id, c.shortname, s.id as section_id, from_unixtime(s.startdate), s.schedule
from course c join lsc_course_section s on s.course_id = c.id
where c.id = 4896
```

```
+------+-------------------+------------+----------------------------+-------------------+
| id   | shortname         | section_id | from_unixtime(s.startdate) | schedule          |
+------+-------------------+------------+----------------------------+-------------------+
| 4896 | DI CW EU Train BR |      10997 | 2014-06-09 00:00:00        | 8:30 am - 4:00 pm |
| 4896 | DI CW EU Train BR |      10998 | 2014-06-10 00:00:00        | 8:00 am - 4:00 pm |
| 4896 | DI CW EU Train BR |      10999 | 2014-06-11 00:00:00        | 8:00 am - 4:00 pm |
| 4896 | DI CW EU Train BR |      11000 | 2014-06-12 00:00:00        | 8:00 am  4:00 pm  |
| 4896 | DI CW EU Train BR |      11001 | 2014-06-13 00:00:00        | 8:00 am - 4:00 pm |
+------+-------------------+------------+----------------------------+-------------------+
```


<a id="lsc_course_section_enroll">
### lsc_course_section_enroll

| Field            | Type         | Null | Description
| ---------------- | ------------ | ---- | ----------------------------------------------------
| id               | bigint(20)   | no   | auto generated
| user_id          | bigint(20)   | no   | 