var driverController = require('./webdriver-controller');
var by = driverController.by;

describe("hello", function() {

    var driver = driverController.get("local/tsg/s/web/app.php/scratch/hello/steve");

    var originalTimeout;
    beforeEach(function() {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    });

    it("should prompt to login", function(done) {

        driver.findElement(by.css('#page-login-index')).getTagName()
            .then(function(tagName) {
                expect(tagName).toEqual("body");
                done();
            }, function(err) {
                expect(true).toBe(false);
                done();
            });
    });

    it("should login and display steve", function(done) {
        driver.findElement(by.css('form#login input#username')).sendKeys('admin');
        driver.findElement(by.css('form#login input#password')).sendKeys('Lsc@2011');
        driver.findElement(by.css('input#loginbtn')).click();
        driver.wait(function() {
            return driver.findElement()
        })
    });

    afterEach(function() {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    })
});