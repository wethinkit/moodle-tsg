var driverController = require('./webdriver-controller');
var by = driverController.by;

describe("Report page", function() {

    var originalTimeout = jasmine.getEnv().defaultTimeoutInterval;

    beforeEach(function(done) {
        jasmine.getEnv().defaultTimeoutInterval = 30000;
        driverController.login().then(function() { done(); });
    });

    afterEach(function() {
        jasmine.getEnv().defaultTimeoutInterval = originalTimeout;
    });

    /*
    it("should work", function() {
        expect(true).toBe(true);
    });
    */

    it("should be logged in", function(done) {

        var page = driverController.get('local/tsg/s/web/app_dev.php/report');

        page.getCurrentUrl()
            .then(function(url) {
                console.log("loaded " + url);
            }, function(err) {
                console.log(err);
            });


        page.findElement(by.css('#page-header .logininfo a[title="View profile"]')).getTagName()
            .then(function(element) {
                console.log("view profile link found");
                return element.getInnerHtml();
            })
            .then(function(html) {
                console.log(html);
            })
            .then(function() {
                done();
            }, function(err) {
                console.log(err);
            });
    });
});