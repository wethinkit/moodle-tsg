var webdriver = require('selenium-webdriver');
var Q = require('q');

function DriverController(context)
{
    var self = this;

    self.context = context || "http://moodle-tsg.local";

    self.driver = new webdriver.Builder()
        .withCapabilities(webdriver.Capabilities.chrome())
        .build();

    jasmine.getEnv().addReporter(new ShutdownReporter(self.driver));

    self.get = function(path)
    {
        path = path || '';
        var fullPath = self.context + "/" + path;
        console.log("retrieving " + fullPath);
        self.driver.get(fullPath);
        return self.driver;
    };

    self.isLoggedIn = false;
    self.login = function()
    {
        return Q.Promise(function(resolve, reject) {
            if (self.isLoggedIn) {
                console.log("already logged in");
                resolve(true);
                return;
            }

            var by = self.by;
            var login = self.get('login/index.php');
            login.findElement(by.css('form#login input#username')).sendKeys('admin');
            login.findElement(by.css('form#login input#password')).sendKeys('Lsc@2011');
            login.findElement(by.css('input#loginbtn')).click();

            console.log("waiting for login");
            waitsFor(function() {
                login.isElementPresent(by.css('#page-header .logininfo a[title="View Profile"]'))
                    .then(function() {
                        self.isLoggedIn = true;
                    });
                if (self.isLoggedIn) {
                    console.log("logged in successfully");
                    resolve(true);
                }
                return self.isLoggedIn;
            }, "login timed out", 15000);
        });
    };

    self.by = webdriver.By;
}

function ShutdownReporter(driver)
{
    this.driver = driver;
}
ShutdownReporter.prototype = new jasmine.Reporter();
ShutdownReporter.prototype.reportRunnerResults = function(runner)
{
    console.log("shutting down selenium webdriver");
    this.driver.quit();
}


module.exports = new DriverController();