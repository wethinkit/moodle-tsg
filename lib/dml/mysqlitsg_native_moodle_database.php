<?php

defined('MOODLE_INTERNAL') || die;

require_once(__DIR__.'/mysqli_native_moodle_database.php');

class mysqlitsg_native_moodle_database extends mysqli_native_moodle_database
{
    public function getWrappedConnection()
    {
        return $this->mysqli;
    }

    protected function get_dbtype() {
        return 'mysqlitsg';
    }

    protected function get_dblibrary() {
        return 'native';
    }

    public function get_name()
    {
        return 'TSG Improved MySQL wrapper (native/mysqli)';
    }
}