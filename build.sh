#!/bin/bash

tar -zcf /Volumes/NO\ NAME/local-tsg.tgz --exclude vendor --exclude vagrant --exclude app/config/config.yml --exclude app/cache local/tsg/*

tar -zcf /Volumes/NO\ NAME/theme-tsg.tgz theme/tsg/*

diskutil unmount /Volumes/NO\ NAME
