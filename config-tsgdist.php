<?php  /// Moodle Configuration File 

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'tsg';
// $CFG->dbhost    = 'cs99la84';
$CFG->dbhost	= 'localhost';
$CFG->dbname    = 'moodle_dcfs_test';
$CFG->dbuser    = 'moodle_user';
$CFG->dbpass    = 'learning1';
$CFG->dbpersist =  false;
$CFG->prefix    = '';

$CFG->wwwroot   = 'http://moodle-tsg.dev:8080';
$CFG->dirroot   = '/Users/sjones/Sites/moodle-tsg.2.5.7';
$CFG->dataroot  = '/Users/sjones/var/sites/moodle-tsg/moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

require_once("$CFG->dirroot/lib/setup.php");

$GLOBALS['USER'] = $USER;
require_once($CFG->dirroot . '/local/tsg/config.php');
require_once($CFG->dirroot . '/local/tsg/locallib.php');

ini_set('error_reporting', E_ALL);
ini_set('display_errors','Off');
ini_set('log_errors','On');
ini_set('error_log','/var/log/moodle/php-errors.log');


// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>
